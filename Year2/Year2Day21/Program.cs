﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

static class Constants
{   
    public const string ROOT_MONKEY = "root";
    public const string HUMAN = "humn";
}

namespace Year2Day21
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = "";

            Dictionary<string, Tuple<string, string, string>> original_monkies = new Dictionary<string, Tuple<string, string, string>>();
            Dictionary<string, ulong> original_vals = new Dictionary<string, ulong>();   

            while ((curr_line = reader.ReadLine()) != null) 
            {
                curr_line = curr_line.Replace(":", "");
                string[] split_line = curr_line.Split(" ");
                if (split_line.Count() > 2)
                {
                    original_monkies.Add(split_line[0], new Tuple<string, string, string>(split_line[1], split_line[2], split_line[3]));
                }
                else 
                {
                    original_vals.Add(split_line[0], ulong.Parse(split_line[1]));
                }
            }
            reader.Close();

            Dictionary<string, Tuple<string, string, string>> curr_monkies = new Dictionary<string, Tuple<string, string, string>>(original_monkies);    
            Dictionary<string, ulong> curr_vals = new Dictionary<string, ulong>(original_vals);   

            while (curr_monkies.Count > 0)
            {
                Dictionary<string, Tuple<string, string, string>> next_monkies = new Dictionary<string, Tuple<string, string, string>>();
                
                foreach (KeyValuePair<string, Tuple<string, string, string>> monkey in curr_monkies)
                {
                    if (curr_vals.ContainsKey(monkey.Value.Item1) && curr_vals.ContainsKey(monkey.Value.Item3))
                    {
                        switch (monkey.Value.Item2)
                        {
                            case "+":
                            {
                                curr_vals.Add(monkey.Key, curr_vals[monkey.Value.Item1] + curr_vals[monkey.Value.Item3]);
                                break;
                            }
                            case "-":
                            {
                                curr_vals.Add(monkey.Key, curr_vals[monkey.Value.Item1] - curr_vals[monkey.Value.Item3]);
                                break;
                            }
                            case "*":
                            {
                                curr_vals.Add(monkey.Key, curr_vals[monkey.Value.Item1] * curr_vals[monkey.Value.Item3]);
                                break;
                            }
                            case "/":
                            {
                                curr_vals.Add(monkey.Key, curr_vals[monkey.Value.Item1] / curr_vals[monkey.Value.Item3]);
                                break;
                            }
                        }
                    }
                    else
                    {
                        next_monkies.Add(monkey.Key, monkey.Value);
                    }
                }    
                curr_monkies = next_monkies;        
            }
            Console.WriteLine("Part 1: " + curr_vals[Constants.ROOT_MONKEY]);  

            curr_monkies = new Dictionary<string, Tuple<string, string, string>>(original_monkies);    
            curr_vals = new Dictionary<string, ulong>(original_vals);   
            Tuple<string, string> root_compare = new Tuple<string, string>(curr_monkies[Constants.ROOT_MONKEY].Item1, curr_monkies[Constants.ROOT_MONKEY].Item3);
            curr_monkies.Remove(Constants.ROOT_MONKEY);
            curr_vals.Remove(Constants.HUMAN);

            while (!curr_vals.ContainsKey(Constants.HUMAN))
            {
                Dictionary<string, Tuple<string, string, string>> next_monkies = new Dictionary<string, Tuple<string, string, string>>();
                
                foreach (KeyValuePair<string, Tuple<string, string, string>> monkey in curr_monkies)
                {
                    ulong i = 0;
                    bool i_num = ulong.TryParse(monkey.Value.Item1, out i);
                    if (!i_num && curr_vals.ContainsKey(monkey.Value.Item1))
                    {
                        i_num = true;
                        i = curr_vals[monkey.Value.Item1];
                    }
                    ulong j = 0;
                    bool j_num = ulong.TryParse(monkey.Value.Item3, out j);
                    if (!j_num && curr_vals.ContainsKey(monkey.Value.Item3))
                    {
                        j_num = true;
                        j = curr_vals[monkey.Value.Item3];
                    }
                    ulong k = 0;
                    bool k_num = ulong.TryParse(monkey.Key, out k);
                    if (!k_num && curr_vals.ContainsKey(monkey.Key))
                    {
                        k_num = true;
                        k = curr_vals[monkey.Key];
                    }

                    if (i_num && j_num && !k_num)
                    {
                        switch (monkey.Value.Item2)
                        {
                            case "+":
                            {
                                curr_vals.Add(monkey.Key, i + j);
                                break;
                            }
                            case "-":
                            {
                                curr_vals.Add(monkey.Key, i - j);
                                break;
                            }
                            case "*":
                            {
                                curr_vals.Add(monkey.Key, i * j);
                                break;
                            }
                            case "/":
                            {
                                curr_vals.Add(monkey.Key, i / j);
                                break;
                            }
                        }
                    }
                    else if (i_num && !j_num && k_num)
                    {
                        switch (monkey.Value.Item2)
                        {
                            case "+":
                            {
                                curr_vals.Add(monkey.Value.Item3, k - i);
                                break;
                            }
                            case "-":
                            {
                                curr_vals.Add(monkey.Value.Item3, i - k);
                                break;
                            }
                            case "*":
                            {
                                curr_vals.Add(monkey.Value.Item3, k / i);
                                break;
                            }
                            case "/":
                            {
                                curr_vals.Add(monkey.Value.Item3, i / k);
                                break;
                            }
                        }
                    }
                    else if (!i_num && j_num && k_num)
                    {
                        switch (monkey.Value.Item2)
                        {
                            case "+":
                            {
                                curr_vals.Add(monkey.Value.Item1, k - j);
                                break;
                            }
                            case "-":
                            {
                                curr_vals.Add(monkey.Value.Item1, k + j);
                                break;
                            }
                            case "*":
                            {
                                curr_vals.Add(monkey.Value.Item1, k / j);
                                break;
                            }
                            case "/":
                            {
                                curr_vals.Add(monkey.Value.Item1, k * j);
                                break;
                            }
                        }
                    }
                    else if (i_num)
                    {
                        next_monkies.Add(monkey.Key, new Tuple<string, string, string>(i.ToString(), monkey.Value.Item2, monkey.Value.Item3)); 
                    }
                    else if (j_num)
                    {
                        next_monkies.Add(monkey.Key, new Tuple<string, string, string>(monkey.Value.Item1, monkey.Value.Item2, j.ToString())); 
                    }
                    else if (k_num)
                    {
                        next_monkies.Add(k.ToString(), new Tuple<string, string, string>(monkey.Value.Item1, monkey.Value.Item2, monkey.Value.Item3));  
                    }
                    else
                    {
                        next_monkies.Add(monkey.Key, monkey.Value);
                    }
                }    
                if (curr_vals.ContainsKey(root_compare.Item1) && !curr_vals.ContainsKey(root_compare.Item2))
                {
                    curr_vals[root_compare.Item2] = curr_vals[root_compare.Item1];
                }  
                else if (curr_vals.ContainsKey(root_compare.Item2) && !curr_vals.ContainsKey(root_compare.Item1))
                {
                    curr_vals[root_compare.Item1] = curr_vals[root_compare.Item2];
                }
                curr_monkies = next_monkies;        
            }
            Console.WriteLine("Part 2: " + curr_vals[Constants.HUMAN]);   
        }
    }
}
