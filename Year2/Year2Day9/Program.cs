﻿using System;
using System.IO;
using System.Collections.Generic;


static class Constants
{
    public const int NUM_KNOTS = 10;
}

namespace Advent
{
    class Year2Day9
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            Dictionary<int, HashSet<int>> tail_visited = new Dictionary<int, HashSet<int>>();
            tail_visited.Add(0, new HashSet<int>());
            tail_visited[0].Add(0);

            int[] knot_xs = new int [Constants.NUM_KNOTS];
            int[] knot_ys = new int [Constants.NUM_KNOTS];

            while ((curr_line = reader.ReadLine()) != null) 
            {                
                string[] curr_cmd = curr_line.Split(" ");

                for (int step_num = 0; step_num < int.Parse(curr_cmd[1]); step_num++)
                {
                    switch (curr_cmd[0])
                    {
                        case "R":
                        {
                            knot_xs[0]++;
                            break;
                        }
                        case "L":
                        {
                            knot_xs[0]--;
                            break;
                        }
                        case "U":
                        {
                            knot_ys[0]++;
                            break;
                        }
                        case "D":
                        {
                            knot_ys[0]--;
                            break;
                        }
                    }
                    for (int curr_knot = 0; curr_knot < Constants.NUM_KNOTS - 1; curr_knot++)
                    {
                        bool this_knot_changed = FixNextKnot(knot_xs[curr_knot], knot_ys[curr_knot], ref knot_xs[curr_knot+1], ref knot_ys[curr_knot+1]);
                        if (curr_knot == Constants.NUM_KNOTS - 2 && this_knot_changed)
                        {
                            if (!tail_visited.ContainsKey(knot_xs[curr_knot+1]))
                            {
                                tail_visited.Add(knot_xs[curr_knot+1], new HashSet<int>());
                            }
                            tail_visited[knot_xs[curr_knot+1]].Add(knot_ys[curr_knot+1]);
                        }
                        else if (!this_knot_changed)
                        {
                            break;
                        }
                    }                 
                }
            }
            int total_num = 0;
            foreach (KeyValuePair<int, HashSet<int>> curr_vis in tail_visited)
            {
                total_num += curr_vis.Value.Count;
            }
            reader.Close();
            Console.WriteLine(total_num);                
        }

        public static bool FixNextKnot(int prev_knot_x, int prev_knot_y, ref int knot_x, ref int knot_y) 
        {
            if (prev_knot_x - knot_x >= 2 && prev_knot_y == knot_y)
            {
                knot_x++;
                return true;
            }
            else if (prev_knot_x - knot_x <= -2 && prev_knot_y == knot_y)
            {
                knot_x--;
                return true;
            }
            else if (prev_knot_y - knot_y >= 2 && prev_knot_x == knot_x)
            {
                knot_y++;
                return true;
            }
            else if (prev_knot_y - knot_y <= -2 && prev_knot_x == knot_x)
            {
                knot_y--;
                return true;
            }
            else if ((prev_knot_x - knot_x >= 2 && prev_knot_y - knot_y >= 1) || (prev_knot_x - knot_x >= 1 && prev_knot_y - knot_y >= 2))
            {
                knot_x++;
                knot_y++;
                return true;
            }
            else if ((prev_knot_x - knot_x <= -2 && prev_knot_y - knot_y >= 1) || (prev_knot_x - knot_x <= -1 && prev_knot_y - knot_y >= 2))
            {
                knot_x--;
                knot_y++;
                return true;
            }
            else if ((prev_knot_x - knot_x >= 2 && prev_knot_y - knot_y <= -1) || (prev_knot_x - knot_x >= 1 && prev_knot_y - knot_y <= -2))
            {
                knot_x++;
                knot_y--;
                return true;
            }
            else if ((prev_knot_x - knot_x <= -2 && prev_knot_y - knot_y <= -1) || (prev_knot_x - knot_x <= -1 && prev_knot_y - knot_y <= -2))
            {
                knot_x--;
                knot_y--;
                return true;
            }
            return false;
        }
    }
}