﻿using System;
using System.IO;

namespace Advent
{
    class Year2Day3
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            int elf_no = 0;
            int score_p1 = 0;
            int score_p2 = 0;
            string[] elves = new string[3];

            while ((curr_line = reader.ReadLine()) != null) 
            {
                int score_add = 0;
                elves[elf_no++] = curr_line;

                string comp1 =  curr_line.Substring(0, curr_line.Length/2);
                string comp2 = curr_line.Substring(curr_line.Length/2, curr_line.Length/2);
                char[] comp1_chars = (char[]) comp1.ToCharArray();
                bool[] found = new bool[52];
                for (int i = 0; i < comp1.Length; i++)
                {
                    score_add = (comp1_chars[i] > 'Z' ? (int)comp1_chars[i] - 96 : (int)comp1_chars[i] - 38);
                    if (comp2.IndexOf(comp1_chars[i]) != -1 && !found[score_add-1])
                    {
                        score_p1 += score_add;
                        found[score_add-1] = true;
                    }
                }

                if (elf_no == 3)
                {
                    elf_no = 0;
                    char[] elf1_chars = (char[]) elves[elf_no].ToCharArray();
                    for (int i = 0; i < elves[elf_no].Length; i++)
                    {
                        if(elves[elf_no+1].IndexOf(elf1_chars[i]) != -1 && elves[elf_no+2].IndexOf(elf1_chars[i]) != -1)
                        {
                            score_add = (elf1_chars[i] > 'Z' ? (int)elf1_chars[i] - 96 : (int)elf1_chars[i] - 38);
                            score_p2 += score_add;
                            break;
                        }
                    }
                }     
            }
            Console.WriteLine(score_p1 + " " + score_p2);
            reader.Close();
        }
    }
}