﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Advent
{
    class Year2Day6
    {
        static class Constants
        {   
            public const int MARKER_LEN = 14;
        }
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";     
            while ((curr_line = reader.ReadLine()) != null) 
            {
                int curr_read_char = 0;
                string curr_last_three = "";
                while (curr_read_char < curr_line.Length)
                {
                    if (curr_last_three.Length < Constants.MARKER_LEN - 1)
                    {
                        curr_last_three+= curr_line.ElementAt(curr_read_char);
                        curr_read_char++;
                        continue;
                    }
                    curr_last_three += curr_line.ElementAt(curr_read_char);
                    
                    int freq = 0;
                    for (int i = 0; i < Constants.MARKER_LEN; i++)
                    {
                        freq+=curr_last_three.Where(x => (x == curr_last_three.ElementAt(i))).Count();
                    }
                    if (freq == Constants.MARKER_LEN)
                    {
                        Console.WriteLine(curr_read_char + 1);
                        break;
                    }
                        
                    curr_last_three = curr_last_three.Substring(1, curr_last_three.Length -1);
                    curr_read_char++;
                }
            }
            reader.Close();
        }
    }
}
