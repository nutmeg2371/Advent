﻿using System;
using System.IO;

namespace Advent
{
    class Year2Day4
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            int overlap_count_p1 = 0;
            int overlap_count_p2 = 0;

            while ((curr_line = reader.ReadLine()) != null) 
            {
                string[] elves = curr_line.Split(",");
                int elf1_start = int.Parse(elves[0].Split("-")[0]);
                int elf1_end = int.Parse(elves[0].Split("-")[1]);
                int elf2_start = int.Parse(elves[1].Split("-")[0]);
                int elf2_end = int.Parse(elves[1].Split("-")[1]);

                bool overlap_type1 = (elf1_start <= elf2_start && elf1_end >= elf2_end) || (elf2_start <= elf1_start && elf2_end >= elf1_end);
                bool overlap_type2 = (elf1_start <= elf2_start && elf1_end >= elf2_start) || (elf1_start <= elf2_end && elf1_end >= elf2_end);

                if (overlap_type1)
                {
                    overlap_count_p1++;
                }
                if (overlap_type1 || overlap_type2)
                {
                    overlap_count_p2++;
                }
            }
            Console.WriteLine(overlap_count_p1 + " " + overlap_count_p2);
            reader.Close();
        }
    }
}
