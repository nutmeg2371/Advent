﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Advent
{

    class Year2Day5
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            int num_stacks = 0;
            Stack<string> stack_lines = new Stack<string>();
            
            while ((curr_line = reader.ReadLine()) != null) 
            {
                if (curr_line.Contains("["))
                {
                    stack_lines.Push(curr_line);
                    continue;
                }
                else
                {
                    string bin_nums_rev = curr_line.Trim();
                    bin_nums_rev = ReverseString(bin_nums_rev);
                    bin_nums_rev = bin_nums_rev.Substring(0, bin_nums_rev.IndexOf(" "));
                    bin_nums_rev = ReverseString(bin_nums_rev);
                    num_stacks = int.Parse(bin_nums_rev);
                    break;
                }
            
            }
            reader.Close();
            reader = File.OpenText("input.txt");

            Stack<char>[] stacks = new Stack<char>[num_stacks];

            for (int i = 0; i < num_stacks; i++)
            {
                stacks[i] = new Stack<char>();
            }
            while (stack_lines.Count > 0)
            {
                curr_line = stack_lines.Pop();
                char[] charArray = (char[]) curr_line.ToCharArray();
                for (int stack_no = 1; stack_no <= num_stacks; stack_no++)
                {
                    char curr_char = curr_line.ElementAt((stack_no -1 ) * 4 + 1);
                    if (curr_char != ' ')
                    {
                        stacks[stack_no - 1].Push(curr_char);
                    }
                }

            }
            while ((curr_line = reader.ReadLine()) != null) 
            {
                if (!curr_line.Contains("move"))
                {
                    continue;
                }
                curr_line = curr_line.Replace("move ","");
                curr_line = curr_line.Replace("from ","");
                curr_line = curr_line.Replace("to ","");
                int[] op_params = Array.ConvertAll(curr_line.Split(' '), x => int.Parse(x));
                int to_move = op_params[0];
                List<char> moved_crates = new List<char>();
                while (to_move > 0)
                {
                    moved_crates.Add(stacks[op_params[1] - 1].Pop());
                    to_move--;
                }
                moved_crates.Reverse();
                foreach (char curr_crate in moved_crates)
                {                    
                    stacks[op_params[2] - 1].Push(curr_crate);
                }

            }

            string top_layer = "";
            for (int i = 0; i < num_stacks; i++)
            {
                top_layer += stacks[i].Pop();
            }
            Console.WriteLine(top_layer);
            reader.Close();
        }
        public static string ReverseString (string in_s)
        {
            char[] charArray = (char[]) in_s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
