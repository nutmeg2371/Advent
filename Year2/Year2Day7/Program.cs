﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Advent
{
    class Year2Day7
    {
        static class Constants
        {   
            public const int FS_SIZE = 70000000;
            public const int UPDATE_SIZE = 30000000;
            public const int SMALL_DIR_SIZE = 100000;
        }

        static void Main(string[] args)
        {
            /* bool is_dir, to allow for shared dir and file names in case, string full_path, int nested_size */
            TreeNode<Tuple<bool, string, int>> file_system = new TreeNode<Tuple<bool, string, int>>(new Tuple<bool, string, int>(true, "/", 0));
            TreeNode<Tuple<bool, string, int>> curr_node = file_system;
            
            Dictionary<string, int> dir_sizes = new Dictionary<string,int>();
            dir_sizes.Add(file_system.Data.Item2, 0);
            
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";  

            while ((curr_line = reader.ReadLine()) != null) 
            {
                string[] split_str = curr_line.Split(" ");
                if (split_str[1] == "ls")
                {
                    continue;
                }
                else if (split_str[1] == "cd")
                {
                    if (split_str[2] == "/")
                    {
                        curr_node = file_system;
                    }
                    else if (split_str[2] == "..")
                    {
                        curr_node = curr_node.Parent;
                    }
                    else
                    {
                        foreach (TreeNode<Tuple<bool, string, int>> curr_child in curr_node.Children)
                        {
                            int path_end = curr_child.Data.Item2.LastIndexOf("/");
                            string dir_name = curr_child.Data.Item2.Substring(path_end + 1, curr_child.Data.Item2.Length - path_end -1);
                            if (curr_child.Data.Item1 && dir_name == split_str[2])
                            {
                                curr_node = curr_child;
                                break;
                            }
                        }
                    }
                }
                else
                {
                    string new_path = curr_node.Data.Item2 + "/" + split_str[1];
                    new_path = new_path.Replace("//", "/");

                    if (split_str[0] == "dir")
                    {
                        curr_node.AddChild(new Tuple<bool, string, int>(true, new_path, 0));
                        dir_sizes.Add(new_path, 0);
                    }
                    else
                    {
                        curr_node.AddChild(new Tuple<bool, string, int>(false, new_path, int.Parse(split_str[0])));

                        curr_node.Data = new Tuple<bool, string, int>(curr_node.Data.Item1, curr_node.Data.Item2, curr_node.Data.Item3 + int.Parse(split_str[0]));
                        dir_sizes[curr_node.Data.Item2] +=  int.Parse(split_str[0]);
 
                        TreeNode<Tuple<bool, string, int>> parent_node = curr_node.Parent;
                        while (parent_node != null)
                        {
                            parent_node.Data = new Tuple<bool, string, int>(parent_node.Data.Item1, parent_node.Data.Item2, parent_node.Data.Item3 + int.Parse(split_str[0]));
                            dir_sizes[parent_node.Data.Item2] +=  int.Parse(split_str[0]);
                            parent_node = parent_node.Parent;
                        } 

                    }
                }
            }
            int needed_free_space = Constants.UPDATE_SIZE - (Constants.FS_SIZE - file_system.Data.Item3);
            int smallest_candidate_dir_size = -1;
            int total_small_dir_sizes = 0;
            foreach (KeyValuePair<string, int> candidate_dir in dir_sizes)
            {
                if (candidate_dir.Value > needed_free_space)
                {
                    if (candidate_dir.Value < smallest_candidate_dir_size || smallest_candidate_dir_size == -1)
                    {
                        smallest_candidate_dir_size = candidate_dir.Value;
                    }
                }
                if (candidate_dir.Value <= Constants.SMALL_DIR_SIZE)
                {
                    total_small_dir_sizes += candidate_dir.Value;
                }
            }
            Console.WriteLine(total_small_dir_sizes + ", " + smallest_candidate_dir_size);
            reader.Close();
        }
    }
    
    public class TreeNode<T>
    {
        public T Data { get; set; }
        public TreeNode<T> Parent { get; set; }
        public ICollection<TreeNode<T>> Children { get; set; }

        public TreeNode(T data)
        {
            this.Data = data;
            this.Children = new LinkedList<TreeNode<T>>();
        }

        public TreeNode<T> AddChild(T child)
        {
            TreeNode<T> childNode = new TreeNode<T>(child) { Parent = this };
            this.Children.Add(childNode);
            return childNode;
        }
    }
}
