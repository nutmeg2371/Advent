﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Numerics;
using System.Text.RegularExpressions;

static class Constants
{  
    public const int NUM_DIR_CHECKS = 4;  
    public const int NUM_ROUNDS = 10;  
}

namespace Year2Day23
{
    class Program
    {
        static int min_x = int.MaxValue;
        static int min_y = int.MaxValue;
        static int max_x = int.MinValue;
        static int max_y = int.MinValue;

        static HashSet<Tuple<int, int>> elf_positions = new HashSet<Tuple<int, int>>();
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = ""; 

            int x = 0;
            int y = 0;

            while ((curr_line = reader.ReadLine()) != null) 
            {
                x = 0;
                foreach (char c in curr_line)
                {
                    if (c == '#')
                    {
                        elf_positions.Add(new Tuple<int, int>(x, y));
                    }
                    x++;
                }
                y++;
            }
            reader.Close();

            int curr_round = 0;
            int dir_check_index = 0;

            bool[] dir_checks = new bool[Constants.NUM_DIR_CHECKS];

            while (true)
            {
                Dictionary<Tuple<int, int>,Tuple<int, int>> proposals = new Dictionary<Tuple<int, int>, Tuple<int, int>>();
                foreach (Tuple<int, int> curr_elf_pos in elf_positions)
                {
                    x = curr_elf_pos.Item1;
                    y = curr_elf_pos.Item2;

                    bool elf_north = elf_positions.Contains(new Tuple<int, int>(x, y-1));
                    bool elf_north_west = elf_positions.Contains(new Tuple<int, int>(x-1, y-1));
                    bool elf_north_east = elf_positions.Contains(new Tuple<int, int>(x+1, y-1));

                    bool elf_south = elf_positions.Contains(new Tuple<int, int>(x, y+1));
                    bool elf_south_west = elf_positions.Contains(new Tuple<int, int>(x-1, y+1));
                    bool elf_south_east = elf_positions.Contains(new Tuple<int, int>(x+1, y+1));

                    bool elf_west = elf_positions.Contains(new Tuple<int, int>(x-1, y));
                    bool elf_east = elf_positions.Contains(new Tuple<int, int>(x+1, y));

                    bool can_move_north = (!elf_north && !elf_north_west && !elf_north_east);
                    bool can_move_south = (!elf_south && !elf_south_west && !elf_south_east);
                    bool can_move_east = (!elf_east && ! elf_north_east && !elf_south_east);
                    bool can_move_west = (!elf_west && ! elf_north_west && !elf_south_west);

                    if (can_move_north && can_move_south && can_move_east && can_move_west)
                    {
                        continue;
                    }
                    dir_checks[0] = can_move_north;
                    dir_checks[1] = can_move_south;
                    dir_checks[2] = can_move_west;
                    dir_checks[3] = can_move_east;

                    int curr_checking_index = dir_check_index;
                    bool found_option = false;
                    int num_checks = 0;
                    while (!found_option && num_checks < Constants.NUM_DIR_CHECKS)
                    {
                        found_option = dir_checks[curr_checking_index++];
                        num_checks++;
                        curr_checking_index%= Constants.NUM_DIR_CHECKS;
                    }
                    if (found_option)
                    {
                        curr_checking_index = (curr_checking_index + Constants.NUM_DIR_CHECKS - 1)%Constants.NUM_DIR_CHECKS;
                        int proposal_x = (curr_checking_index == 0 || curr_checking_index == 1 ? x : (curr_checking_index == 2 ? x-1 : x+1 ));
                        int proposal_y = (curr_checking_index == 2 || curr_checking_index == 3 ? y : (curr_checking_index == 0 ? y-1 : y+1 ));
                        proposals.Add(curr_elf_pos, new Tuple<int, int>(proposal_x, proposal_y));
                    }
                }

                if (proposals.Count == 0)
                {
                    Console.WriteLine("Part 2: " + (curr_round + 1));
                    return;
                }

                Dictionary<Tuple<int, int>,Tuple<int, int>> moves = new Dictionary<Tuple<int, int>, Tuple<int, int>>();
                foreach (KeyValuePair<Tuple<int, int>,Tuple<int, int>> curr_proposal in proposals)
                {
                    bool duplicate_found = false;
                    foreach (KeyValuePair<Tuple<int, int>,Tuple<int, int>> other_proposal in proposals)
                    {
                        if (curr_proposal.Key.Item1 == other_proposal.Key.Item1 && curr_proposal.Key.Item2 == other_proposal.Key.Item2)
                        {
                            continue;
                        }
                        if (curr_proposal.Value.Item1 == other_proposal.Value.Item1 && curr_proposal.Value.Item2 == other_proposal.Value.Item2)
                        {
                            duplicate_found = true;
                            break;
                        }
                    }
                    if (!duplicate_found)
                    {
                        moves.Add(curr_proposal.Key, curr_proposal.Value);
                    }
                }

                foreach (KeyValuePair<Tuple<int, int>,Tuple<int, int>> curr_move in moves)
                {   
                    elf_positions.Remove(curr_move.Key);
                    elf_positions.Add(curr_move.Value); 
                }

                dir_check_index = (dir_check_index + 1)% Constants.NUM_DIR_CHECKS;
                curr_round++;

                if (curr_round == Constants.NUM_ROUNDS)
                {
                    int blanks = 0;
                    foreach (Tuple<int, int> curr_elf in elf_positions)
                    {
                        x = curr_elf.Item1;
                        y = curr_elf.Item2;
                        min_x = Math.Min(min_x, x);
                        min_y = Math.Min(min_y, y);
                        max_x = Math.Max(max_x, x);
                        max_y = Math.Max(max_y, y);
                    }
                    for (x = min_x; x <= max_x; x++)
                    {
                        for(y = min_y; y <= max_y; y++)
                        {
                            if (!elf_positions.Contains(new Tuple<int, int>(x,y)))
                            {
                                blanks++;
                            }
                        }
                    }
                    Console.WriteLine("Part 1: " + blanks);
                }
            }

            
        }
    }
}

           