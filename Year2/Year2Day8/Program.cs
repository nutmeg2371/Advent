﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Advent
{
        static class Constants
        {   
          //  public const int GRID_ROWS = 3;
           // public const int GRID_COLS = 3;
        }

    class Year2Day8
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            List<string> grid_lines = new List<string>();
            int num_rows = 0;
            int num_cols = 0;
            
            while ((curr_line = reader.ReadLine()) != null) 
            {
                grid_lines.Add(curr_line);
                num_cols = curr_line.Length;
                num_rows++;
            }

            int[,] tree_grid = new int[num_rows,num_cols];
            bool[,] vis_left = new bool[num_rows,num_cols];
            bool[,] vis_right = new bool[num_rows,num_cols];
            bool[,] vis_top = new bool[num_rows,num_cols];
            bool[,] vis_bot = new bool[num_rows,num_cols];
            
            int curr_row = 0;
            int curr_col = 0;

            foreach (string curr_grid_str in grid_lines)
            {
                foreach (char curr_tree in curr_grid_str)
                {
                    tree_grid[curr_row, curr_col] = curr_tree;
                    curr_row++;
                }
                curr_row = 0;
                curr_col++;
            }

            int max_height_so_far = 0;
            for (curr_row = 0; curr_row < num_rows; curr_row++)
            {
                for (curr_col = 0; curr_col < num_cols; curr_col++)
                {
                    if (tree_grid[curr_row, curr_col] > max_height_so_far)
                    {
                        vis_left[curr_row, curr_col] = true;
                        max_height_so_far = tree_grid[curr_row, curr_col];
                    }
                }
                max_height_so_far = 0;
            }           
            max_height_so_far = 0;
            for (curr_row = 0; curr_row < num_rows; curr_row++)
            {
                for (curr_col = num_cols - 1; curr_col >= 0; curr_col--)
                {
                    if (tree_grid[curr_row, curr_col] > max_height_so_far)
                    {
                        vis_right[curr_row, curr_col] = true;
                        max_height_so_far = tree_grid[curr_row, curr_col];
                    }
                }
                max_height_so_far = 0;
            }         
            max_height_so_far = 0;
            for (curr_col = 0; curr_col < num_cols; curr_col++)
            {
                for (curr_row = 0; curr_row < num_rows; curr_row++)
                {
                    if (tree_grid[curr_row, curr_col] > max_height_so_far)
                    {
                        vis_top[curr_row, curr_col] = true;
                        max_height_so_far = tree_grid[curr_row, curr_col];
                    }
                }
                max_height_so_far = 0;
            }     
            max_height_so_far = 0;
            for (curr_col = 0; curr_col < num_cols; curr_col++)
            {
                for (curr_row = num_rows - 1; curr_row >=  0; curr_row--)
                {
                    if (tree_grid[curr_row, curr_col] > max_height_so_far)
                    {
                        vis_bot[curr_row, curr_col] = true;
                        max_height_so_far = tree_grid[curr_row, curr_col];
                    }
                }
                max_height_so_far = 0;
            }

            int total_vis = 0;
            for (curr_row = 1; curr_row < num_rows - 1; curr_row++)
            {
                for (curr_col = 1; curr_col < num_cols - 1; curr_col++)
                {
                    if (vis_bot[curr_row, curr_col] || vis_top[curr_row, curr_col] || vis_left[curr_row, curr_col] || vis_right[curr_row, curr_col])
                    {
                        total_vis++;
                    }
                }
            }
            total_vis += (2 * num_cols + 2 * num_rows - 4);

            int max_scenic = 0;

            for (curr_row = 1; curr_row < num_rows - 1; curr_row++)
            {
                for (curr_col = 1; curr_col < num_cols - 1; curr_col++)
                {
                    int left_vis_score = 0;
                    int right_vis_score = 0;
                    int top_vis_score = 0;
                    int bot_vis_score = 0;     

                    for (int vis_col = curr_col - 1; vis_col >= 0; vis_col--)
                    {
                        left_vis_score++;
                        if (tree_grid[curr_row, vis_col] >= tree_grid[curr_row, curr_col])
                        {
                            break;
                        }
                    }    
                    for (int vis_col = curr_col + 1; vis_col < num_cols; vis_col++)
                    {
                        right_vis_score++;
                        if (tree_grid[curr_row, vis_col] >= tree_grid[curr_row, curr_col])
                        {
                            break;
                        }
                    }  
                    for (int vis_row = curr_row - 1; vis_row >= 0; vis_row--)
                    {
                        top_vis_score++;
                        if (tree_grid[vis_row, curr_col] >= tree_grid[curr_row, curr_col])
                        {
                            break;
                        }
                    }   
                    for (int vis_row = curr_row + 1; vis_row < num_rows; vis_row++)
                    {
                        bot_vis_score++;
                        if (tree_grid[vis_row, curr_col] >= tree_grid[curr_row, curr_col])
                        {
                            break;
                        }
                    }             
                    int scenic_score = left_vis_score * right_vis_score * top_vis_score * bot_vis_score;
                    if (scenic_score > max_scenic)
                    {
                        max_scenic = scenic_score;
                    }
                }
            }

            reader.Close();
            Console.WriteLine(total_vis + " " + max_scenic);
        }
    }
}