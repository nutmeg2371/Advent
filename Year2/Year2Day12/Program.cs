﻿using System;
using System.IO;
using System.Collections.Generic;


static class Constants
{   
    public const int GRID_ROWS = 41;
    public const int GRID_COLS = 81;
}

namespace Year2Day12
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            char [,] input_grid = new char[Constants.GRID_ROWS, Constants.GRID_COLS];

            int line_num = 0;
            int end_row = -1;
            int end_col = -1;
            int start_row = -1;
            int start_col = -1;

            while ((curr_line = reader.ReadLine()) != null) 
            {                 
                char[] split_line = curr_line.ToCharArray();
                for (int i = 0; i < curr_line.Length; i++)
                {
                    input_grid[line_num, i] = split_line[i];
                    if (split_line[i] =='E')
                    {
                        end_row = line_num;
                        end_col = i;
                        input_grid[line_num, i] = 'z';
                    }
                    else if (split_line[i] =='S')
                    {
                        start_row = line_num;
                        start_col = i;
                        input_grid[line_num, i] = 'a';
                    }
                }
                line_num++;
            } 
            reader.Close();
            GetPathLength(input_grid, end_row, end_col, start_row, start_col);
        }
        static void GetPathLength(char[,] grid, int end_row, int end_col, int start_row, int start_col)
        {

            int [,] path_cost = new int[Constants.GRID_ROWS, Constants.GRID_COLS];
            int max_path_cost = Constants.GRID_COLS * Constants.GRID_ROWS;

            for (int curr_row = 0; curr_row < Constants.GRID_ROWS; curr_row++)
            {
                for (int curr_col = 0; curr_col < Constants.GRID_COLS; curr_col++)
                {
                    path_cost[curr_row, curr_col] = max_path_cost;
                }
            }
            
            path_cost[end_row, end_col] = 0;

            bool change = true;
            while(change)
            {
                change = false;
                for (int curr_row = 0; curr_row < Constants.GRID_ROWS; curr_row++)
                {
                    for (int curr_col = 0; curr_col < Constants.GRID_COLS; curr_col++)
                    {
                        int this_spot_elevation = (int) grid[curr_row, curr_col];
                        
                        if (curr_col > 0 && grid[curr_row, curr_col - 1] <= this_spot_elevation + 1 && path_cost[curr_row, curr_col - 1] + 1 < path_cost[curr_row, curr_col])
                        {
                            path_cost[curr_row, curr_col] = path_cost[curr_row, curr_col - 1] + 1;
                            change = true;
                        }
                        
                        if (curr_col < Constants.GRID_COLS - 1 && grid[curr_row, curr_col + 1] <= this_spot_elevation + 1 && path_cost[curr_row, curr_col + 1] + 1 < path_cost[curr_row, curr_col])
                        {
                            path_cost[curr_row, curr_col] = path_cost[curr_row, curr_col + 1] + 1;
                            change = true;
                        }

                        if (curr_row > 0 && grid[curr_row - 1, curr_col] <= this_spot_elevation + 1 && path_cost[curr_row - 1, curr_col] + 1 < path_cost[curr_row, curr_col])
                        {
                            path_cost[curr_row, curr_col] = path_cost[curr_row - 1, curr_col] + 1;
                            change = true;
                        }

                        if (curr_row < Constants.GRID_ROWS - 1 && grid[curr_row + 1, curr_col] <= this_spot_elevation + 1 && path_cost[curr_row + 1, curr_col] + 1 < path_cost[curr_row, curr_col])
                        {
                            path_cost[curr_row, curr_col] = path_cost[curr_row + 1, curr_col] + 1;
                            change = true;
                        }
                    }
                }

            } 
            int min_a_start = max_path_cost;
            for (int curr_row = 0; curr_row < Constants.GRID_ROWS; curr_row++)
            {
                for (int curr_col = 0; curr_col < Constants.GRID_COLS; curr_col++)
                {
                    if (grid[curr_row, curr_col] == 'a' && path_cost[curr_row, curr_col] < min_a_start)
                    {
                        min_a_start = path_cost[curr_row, curr_col];
                    }
                }
            }
            Console.WriteLine ("Min from given start: " + path_cost[start_row, start_col]);
            Console.WriteLine ("Min from any allowed start: " + min_a_start);
        }
    }
}
