﻿using System;
using System.IO;
using System.Collections.Generic;


static class Constants
{   
    public const int INTEREST_LINE = 2000000;
    public const double MULTIPLIER = 4000000;
    public const int MAX_COORD = 4000000;
    public const int MIN_COORD = 0;
}

namespace Year2Day15
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            List<Tuple<int,int>>[] per_y_covered_x_ranges = new List<Tuple<int,int>>[Constants.MAX_COORD + 1];
            HashSet<int>[] per_y_beacon_xs = new HashSet<int>[Constants.MAX_COORD + 1];

            for (int curr_y_interest_line = 0; curr_y_interest_line <= Constants.MAX_COORD; curr_y_interest_line++)
            {
                per_y_covered_x_ranges[curr_y_interest_line] = new List<Tuple<int, int>>();
                per_y_beacon_xs[curr_y_interest_line] = new HashSet<int>();
            }

            while ((curr_line = reader.ReadLine()) != null) 
            {                 
                curr_line = curr_line.Replace("Sensor at x=","");
                curr_line = curr_line.Replace(", y="," ");
                curr_line = curr_line.Replace(": closest beacon is at x="," ");
                string[] split_str = curr_line.Split();
                int sensor_x = int.Parse(split_str[0]);
                int sensor_y = int.Parse(split_str[1]);
                int beacon_x = int.Parse(split_str[2]);
                int beacon_y = int.Parse(split_str[3]);
                int manhattan_dist = Math.Abs(sensor_x - beacon_x) + Math.Abs(sensor_y - beacon_y);
                for (int curr_y_interest_line = 0; curr_y_interest_line <= Constants.MAX_COORD; curr_y_interest_line++)
                {
                    int y_dist = Math.Abs(sensor_y - curr_y_interest_line);
                    int remaining_x = manhattan_dist - y_dist;
                    if (remaining_x >= 0)
                    {
                        int range_start = sensor_x - remaining_x;
                        int range_end = sensor_x + remaining_x;
                        if (range_start < Constants.MIN_COORD)
                        {
                            range_start = Constants.MIN_COORD;
                        }
                        if (range_end > Constants.MAX_COORD)
                        {
                            range_end = Constants.MAX_COORD;
                        }
                        per_y_covered_x_ranges[curr_y_interest_line].Add(new Tuple<int, int>(range_start, range_end));
                    }
                    if (beacon_y == Constants.INTEREST_LINE)
                    {
                        per_y_beacon_xs[curr_y_interest_line].Add(beacon_x);
                    }
                    if (sensor_y == Constants.INTEREST_LINE)
                    {
                        per_y_beacon_xs[curr_y_interest_line].Add(beacon_x);
                    }
                }
            } 
            reader.Close();
            
            List<Tuple<int,int>>[] per_y_collapsed_x_ranges = new List<Tuple<int,int>>[Constants.MAX_COORD + 1];
            int[] per_y_coverage_size = new int[Constants.MAX_COORD + 1];
            for (int curr_y_interest_line = 0; curr_y_interest_line <= Constants.MAX_COORD; curr_y_interest_line++)
            {
                List<Tuple<int,int>> collapsed_ranges = CollapseRanges(per_y_covered_x_ranges[curr_y_interest_line]);
                per_y_collapsed_x_ranges[curr_y_interest_line] = collapsed_ranges;
                foreach (Tuple<int,int> curr_range in collapsed_ranges)
                {
                    per_y_coverage_size[curr_y_interest_line] += (curr_range.Item2 - curr_range.Item1 + 1);
                    foreach (int curr_x in per_y_beacon_xs[curr_y_interest_line])
                    {
                        if (curr_x >= curr_range.Item1 && curr_x <= curr_range.Item2)
                        {
                            per_y_coverage_size[curr_y_interest_line]--;
                        }
                    }
                }
                if (per_y_coverage_size[curr_y_interest_line] < Constants.MAX_COORD)
                {
                    collapsed_ranges.Sort();
                    HashSet<int> potential_missing_xs = new HashSet<int>();
                    potential_missing_xs.Add(collapsed_ranges[0].Item1 - 1);
                    for (int curr_range_index = 0; curr_range_index < collapsed_ranges.Count - 1; curr_range_index++)
                    {
                        for (int potential_missing_x = collapsed_ranges[curr_range_index].Item2 + 1; potential_missing_x <= collapsed_ranges[curr_range_index + 1].Item1 - 1; potential_missing_x++)
                        {
                            potential_missing_xs.Add(potential_missing_x);
                        }
                    }
                    potential_missing_xs.Add(collapsed_ranges[collapsed_ranges.Count - 1].Item1 - 1);
                    foreach (int potential_missing_x in potential_missing_xs)
                    {
                        if (potential_missing_x >= Constants.MIN_COORD && potential_missing_x <= Constants.MAX_COORD)
                        {
                            double answer = potential_missing_x * Constants.MULTIPLIER + curr_y_interest_line;
                            Console.WriteLine("Part 2: " + answer);
                        }
                    }
                }

            }

            Console.WriteLine("Part 1: " + per_y_coverage_size[Constants.INTEREST_LINE]); //wrong for part 1 if limited by min/max coords
        }
        public static List<Tuple<int,int>> CollapseRanges(List<Tuple<int,int>> in_ranges)
        {
            List<Tuple<int,int>> out_ranges = new List<Tuple<int, int>>(in_ranges);

            bool changed = true;
            while (changed)
            {
                changed = false;
                out_ranges = new List<Tuple<int, int>>(in_ranges);
                for (int curr_first_range_index = 0; curr_first_range_index < in_ranges.Count; curr_first_range_index++)
                {
                    for (int curr_second_range_index = curr_first_range_index + 1; curr_second_range_index < in_ranges.Count; curr_second_range_index++)
                    {
                        if (!((in_ranges[curr_second_range_index].Item1 > in_ranges[curr_first_range_index].Item2) || (in_ranges[curr_first_range_index].Item1 > in_ranges[curr_second_range_index].Item2)))
                        {
                            Tuple<int,int> new_range = new Tuple<int, int>(Math.Min(in_ranges[curr_first_range_index].Item1, in_ranges[curr_second_range_index].Item1), Math.Max(in_ranges[curr_first_range_index].Item2, in_ranges[curr_second_range_index].Item2));
                            out_ranges.Remove(in_ranges[curr_first_range_index]);
                            out_ranges.Remove(in_ranges[curr_second_range_index]);
                            out_ranges.Add(new_range);
                            changed = true;
                            break;
                        }
                    }
                    if (changed)
                    {
                        in_ranges = out_ranges;
                        break;
                    }
                }
            } 
            return out_ranges;   
        }
    }
}
