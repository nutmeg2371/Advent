﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;


namespace Advent
{
    static class Constants
    {
        public const Decimal WORRY_DIV = 1;
        public const int NUM_ROUNDS = 10000 ;
    }
    class Year2Day11
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            Dictionary<int, Monkey> Monkies = new Dictionary<int, Monkey>();
            List<int> all_div_tests = new List<int>();

            int curr_monkey_index = -1;
            while ((curr_line = reader.ReadLine()) != null) 
            {
                curr_line = curr_line.Trim();
                string[] split_str = curr_line.Split(" ");
                if (curr_line.Length == 0)
                {
                    continue;
                }
                if (split_str[0] == "Monkey")
                {
                    split_str[1] = split_str[1].Replace(":","");
                    curr_monkey_index = int.Parse(split_str[1]);
                    if (!Monkies.ContainsKey(curr_monkey_index))
                    {
                        Monkey new_monkey = new Monkey();
                        Monkies.Add(curr_monkey_index, new_monkey);
                    }
                }
                else if (split_str[0] == "Starting")
                {
                    for (int item_ind = 2; item_ind < split_str.Length; item_ind++)
                    {
                        split_str[item_ind] = split_str[item_ind].Replace(",","");
                        Monkies[curr_monkey_index].AddInitialItem(int.Parse(split_str[item_ind]));
                    }
                }
                else if (split_str[0] == "Operation:")
                {
                    char op = split_str[4].ToCharArray()[0];
                    int operand = 0;
                    if (split_str[5] == "old")
                    {
                        op = '^';
                    }
                    else
                    {
                        operand = int.Parse(split_str[5]);
                    }

                    Monkies[curr_monkey_index].my_operator = op;
                    Monkies[curr_monkey_index].my_operand = operand;
                }
                else if (split_str[0] == "Test:")
                {
                    Monkies[curr_monkey_index].my_div_test = int.Parse(split_str[3]);
                    all_div_tests.Add(Monkies[curr_monkey_index].my_div_test);
                }
                else if (split_str[1] == "true:")
                {
                    Monkies[curr_monkey_index].my_true_target = int.Parse(split_str[5]);
                }
                else if (split_str[1] == "false:")
                {
                    Monkies[curr_monkey_index].my_false_target = int.Parse(split_str[5]);
                }
            }
            reader.Close();
            foreach (KeyValuePair<int, Monkey> curr_monkey in Monkies)
            {
                curr_monkey.Value.InitializeQueue(all_div_tests);
            }
            
            for (int round_no = 0; round_no < Constants.NUM_ROUNDS; round_no++)
            {
                Console.WriteLine("Round: " + round_no);
                foreach (KeyValuePair<int, Monkey> curr_monkey in Monkies)
                {
                    Console.WriteLine("  Monkey: " + curr_monkey.Key + " " + curr_monkey.Value.process_count);
                }
                foreach (KeyValuePair<int, Monkey> curr_monkey in Monkies)
                {
                    int target_monkey = -1;
                    MonkeyNumber new_value = null;
                    while(curr_monkey.Value.ProcessItem(ref target_monkey, ref new_value))
                    {
                        Monkies[target_monkey].QueueItem(new_value);
                    }
                }
            }
            
            List<System.Numerics.BigInteger> activities = new List<System.Numerics.BigInteger>();
            foreach (KeyValuePair<int, Monkey> curr_monkey in Monkies)
            {
                activities.Add(curr_monkey.Value.process_count);
            }
            activities.Sort();
            activities.Reverse();
            Console.WriteLine(activities.ElementAt(0) * activities.ElementAt(1));
        }
    }

    public class Monkey
    {
        public List<int> my_initial_items;
        public List<MonkeyNumber> my_current_items;
        public char my_operator { get; set; }
        public int my_operand { get; set; }
        public int my_div_test { get; set; }
        public int my_true_target { get; set; }
        public int my_false_target { get; set; }

        public System.Numerics.BigInteger process_count = 0;
        
        public Monkey()
        {
            my_initial_items = new List<int>();
        }

        public void AddInitialItem (int x)
        {
            my_initial_items.Add(x);
        }

        public void InitializeQueue (List<int> div_tests)
        {
            my_current_items = new List<MonkeyNumber>();
            foreach (int curr_init_queue_item in my_initial_items)
            {
                my_current_items.Add(new MonkeyNumber(curr_init_queue_item, div_tests));
            }
        }

        public void QueueItem (MonkeyNumber to_queue)
        {
            my_current_items.Add(to_queue);
        }
        public bool ProcessItem (ref int target_monkey, ref MonkeyNumber new_value)
        {
            if (my_current_items.Count == 0)
            {
                return false;
            }
            process_count++;
            new_value = my_current_items.ElementAt(0);
            my_current_items.RemoveAt(0);

            new_value.TransformNumber(my_operator, my_operand);
            target_monkey = (new_value.TestNumber(my_div_test) ? my_true_target : my_false_target);
            return true;
        }
    }

    public class MonkeyNumber
    {
        Dictionary<int, int> div_tests_and_remainders;

        public MonkeyNumber (int initial_integer, List<int> div_tests)
        {
            div_tests_and_remainders = new Dictionary<int, int>();
            foreach (int curr_div_test in div_tests)
            {
                div_tests_and_remainders.Add(curr_div_test, initial_integer%curr_div_test);
            }
        }

        public bool TestNumber (int div_test)
        {
            return (div_tests_and_remainders[div_test] == 0);
        }

        public void TransformNumber(int xform_operator, int xform_operand)
        {
            Dictionary<int, int>.KeyCollection all_keys = div_tests_and_remainders.Keys;
            switch (xform_operator)
            {
                case '+':
                {
                    foreach (int curr_key in all_keys)
                    {
                        div_tests_and_remainders[curr_key] = (div_tests_and_remainders[curr_key] + xform_operand)%curr_key;
                    }
                    break;
                }
                case '*':
                {
                    foreach (int curr_key in all_keys)
                    {
                        div_tests_and_remainders[curr_key] = (div_tests_and_remainders[curr_key] * xform_operand)%curr_key;
                    }
                    break;
                }
                case '^':
                {
                    foreach (int curr_key in all_keys)
                    {
                        div_tests_and_remainders[curr_key] = ((div_tests_and_remainders[curr_key] * div_tests_and_remainders[curr_key]) % curr_key);
                    }
                    break;
                }
                default :
                {
                    break;
                }
            }
            //curr_item = curr_item/(System.Numerics.BigInteger)Constants.WORRY_DIV;
            //System.Numerics.BigInteger temp = curr_item/(System.Numerics.BigInteger)my_div_test;
            
        }
        public bool TestNumberDic (int div_test)
        {
            return (div_tests_and_remainders[div_test] == 0);
        }
    }            
}