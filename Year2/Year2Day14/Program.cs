﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

static class Constants
{
  public const int GRID_SIZE = 1311;
  public const int NUM_CHARS = 8;
}

namespace Year2Day14
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            List<List<Tuple<int,int>>> all_path_sets = new List<List<Tuple<int, int>>>();    
            int max_x = -1;
            int max_y = -1;    

            while ((curr_line = reader.ReadLine()) != null) 
            {
                List<Tuple<int,int>> this_path_set = new List<Tuple<int, int>>();
                string[] split_line = curr_line.Split(" -> ");
                foreach (string curr_pos in split_line)
                {
                    int x = int.Parse(curr_pos.Split(",")[0]);
                    int y = int.Parse(curr_pos.Split(",")[1]);
                    this_path_set.Add(new Tuple<int,int>(x, y));
                    if (max_x == -1 || max_x < x)
                    {
                        max_x = x;
                    }
                    if (max_y == -1 || max_y < y)
                    {
                        max_y = y;
                    }
                }
                all_path_sets.Add(this_path_set);
            }
            reader.Close();

            int floor_y = 2 + max_y;
            int size_x = max_x * 2;
            int size_y = floor_y + 1;

            char[,] field = new char[size_x, size_y];

            for (int curr_x = 0; curr_x < size_x; curr_x++)
            {
                for (int curr_y = 0; curr_y < size_y; curr_y++)
                {
                    field[curr_x, curr_y] = '.';
                }
                field[curr_x, floor_y] = '#';
            }

            foreach (List<Tuple<int,int>> path_set in all_path_sets)
            {
                for (int curr_path_index = 0; curr_path_index < path_set.Count - 1; curr_path_index++)
                {
                    int start_x = path_set[curr_path_index].Item1;
                    int start_y = path_set[curr_path_index].Item2;
                    int end_x = path_set[curr_path_index + 1].Item1;
                    int end_y = path_set[curr_path_index + 1].Item2;

                    if (start_x == end_x)
                    {
                        int top_y = (start_y < end_y ? start_y : end_y);
                        int bottom_y = (start_y < end_y ? end_y : start_y);
                        for (int curr_y = top_y; curr_y <= bottom_y; curr_y++)
                        {
                            field[start_x, curr_y] = '#';
                        }
                    }
                    else if (start_y == end_y)
                    {
                        int left_x = (start_x < end_x ? start_x : end_x);
                        int right_x = (start_x < end_x ? end_x : start_x);
                        for (int curr_x = left_x; curr_x <= right_x; curr_x++)
                        {
                            field[curr_x, start_y] = '#';
                        }
                    }
                }
            }

            int num_balls_dropped = 0;
            bool ball_stopped = true;

            while (ball_stopped)
            {
                ball_stopped = false;
                int ball_x = 500;
                int ball_y = 0;

                bool ball_rolling = true;
                while (ball_rolling)
                {
                    ball_rolling = false;

                    if (ball_y >= size_y - 1)
                    {
                        break;
                    }

                    if (field[ball_x, ball_y + 1] == '.')
                    {
                        ball_y++;
                        ball_rolling = true;
                        continue;
                    }
                    else if (field[ball_x -1, ball_y + 1] == '.')
                    {
                        ball_x--;
                        ball_y++;
                        ball_rolling = true;
                        continue;
                    }
                    else if (field[ball_x + 1, ball_y + 1] == '.')
                    {
                        ball_x++;
                        ball_y++;
                        ball_rolling = true;
                        continue;
                    }
                    if (ball_y != 0)
                    {
                        field[ball_x, ball_y] = 'o';
                        num_balls_dropped++;
                        ball_stopped = true;
                    }
                    else
                    {
                        num_balls_dropped++;
                        break;
                    }
                }
            }            
            
            Console.WriteLine(num_balls_dropped);
        }
        
    }
    
}
