﻿using System;
using System.IO;

namespace Advent
{
    class Year2Day1
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            int curr_cal_total = 0;

            int TOP_ELVES = 3;
            int[] cal_totals = new int[TOP_ELVES];
        
            while ((curr_line = reader.ReadLine()) != null) 
            {

                if (curr_line == "")
                {
                    curr_cal_total = 0;
                    continue;
                }
                curr_cal_total += int.Parse(curr_line);

                for (int i = TOP_ELVES - 1; i >= 0; i--)
                {
                    if (curr_cal_total > cal_totals[i])
                    {
                        int old_max = cal_totals[i];
                        cal_totals[i] = curr_cal_total;
                        curr_cal_total = old_max;
                    }
                }
            }
            int overall_total = 0;
            for (int i = TOP_ELVES - 1; i >= 0; i--)
            {
                overall_total += cal_totals[i];
            }
            Console.WriteLine(overall_total);
            reader.Close();
        }
    }
}
