﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Advent
{
    static class Constants
    {
        public const int CRT_ROWS = 6;
        public const int START_STRENGTH_COUNT = 20;
        public const int STRENGTH_COUNT_INTERVAL = 40;
    }
    class Year2Day10
    {
        static int cycle_count = 0;
        static int x = 1;
        static int next_strength_count = Constants.START_STRENGTH_COUNT;
        

        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            
            int total_sig_strength = 0;
            CRT crt = new CRT(Constants.CRT_ROWS);
            
            while ((curr_line = reader.ReadLine()) != null) 
            {
    
                cycle_count++;
                total_sig_strength += CheckSignalCount();
                crt.PrintPixel(x);

                if (curr_line.Contains("addx"))
                {
                    cycle_count++;
                    total_sig_strength += CheckSignalCount();
                    crt.PrintPixel(x);
                        
                    string[] split_str = curr_line.Split(" ");
                    int add_amt = int.Parse(split_str[1]);
                    x+= add_amt;
                    
                }
            }
            reader.Close();
            Console.WriteLine (total_sig_strength);
            crt.Print();
            
        }
        public static int CheckSignalCount ()
        {
            if (cycle_count == next_strength_count)
            {
                next_strength_count += Constants.STRENGTH_COUNT_INTERVAL;
                return x * cycle_count;
            }
            return 0;
        }
    }
    public class CRT
    {
        int crt_line_index = 0;
        string[] crt_lines;
        public CRT (int rows)
        {
            crt_lines = new string[rows];
            crt_lines[crt_line_index] = "";
        }
            
        public void PrintPixel (int x)
        {
            char char_to_draw = ((x-1  == crt_lines[crt_line_index].Length || x == crt_lines[crt_line_index].Length || x+1 == crt_lines[crt_line_index].Length) ? '#' : '.');
            crt_lines[crt_line_index] += char_to_draw;
            if (crt_lines[crt_line_index].Length == 40 && crt_line_index < Constants.CRT_ROWS - 1)
            {
                crt_lines[++crt_line_index] = "";
            }
        }
            
        public void Print ()
        {
            foreach (string curr_line in crt_lines)
            {
                Console.WriteLine(curr_line);
            }
        }
    }
}