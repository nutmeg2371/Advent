﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

static class Constants
{   
    public const int DIR_RIGHT = 0;
    public const int DIR_DOWN = 1;
    public const int DIR_LEFT = 2;
    public const int DIR_UP = 3;
    public const int NUM_DIRS = 4;
}

namespace Year2Day24
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = "";

            int top_opening = 0;
            int bottom_opening = 0;
            int x = 0;
            int y = 0;
            int max_x = 0;
            int max_y = 0;

            HashSet<Tuple<int, int, int>> blizzards = new HashSet<Tuple<int, int, int>>();

            while ((curr_line = reader.ReadLine()) != null) 
            {
                if (curr_line.Contains("##") && top_opening == 0)
                {
                    top_opening = curr_line.IndexOf(".");
                    max_x = Math.Max(max_x, curr_line.Length - 1);
                }  
                else if (curr_line.Contains("##"))
                {
                    bottom_opening = curr_line.IndexOf(".");
                }   
                else
                {
                    x = 0;
                    foreach (char c in curr_line)
                    {
                        if (c == '>')
                        {
                            blizzards.Add(new Tuple<int, int, int>(x, y, Constants.DIR_RIGHT));
                        }
                        else if (c == 'v')
                        {
                            blizzards.Add(new Tuple<int, int, int>(x, y, Constants.DIR_DOWN));
                        }
                        else if (c == '<')
                        {
                            blizzards.Add(new Tuple<int, int, int>(x, y, Constants.DIR_LEFT));
                        }
                        else if (c == '^')
                        {
                            blizzards.Add(new Tuple<int, int, int>(x, y, Constants.DIR_UP));
                        }
                        x++;
                    }

                }  
                y++;                      
            }
            max_y = y - 1;

            Dictionary<int, HashSet<Tuple<int, int, int>>> states = new Dictionary<int, HashSet<Tuple<int, int, int>>>();
            Dictionary<int, HashSet<Tuple<int, int, int, int>>> positions = new Dictionary<int, HashSet<Tuple<int, int, int, int>>>();
            positions[0] = new HashSet<Tuple<int, int, int, int>>();
            positions[0].Add(new Tuple<int, int, int, int>(top_opening, 0, 0, 0));
            states[0] = blizzards;
            int minute = 1;
            bool goal_seen = false;
            bool restart_seen = false;

            bool got_out = false;
            while (!got_out)
            {
                HashSet<Tuple<int, int>> blizzards_this_min = new HashSet<Tuple<int, int>>();

                states[minute] = new HashSet<Tuple<int, int, int>>();
                foreach (Tuple<int, int, int> blizzard in states[minute - 1])
                {
                    int new_x = blizzard.Item1;
                    int new_y = blizzard.Item2;
                    if (blizzard.Item3 == 0)
                    {
                        new_x++;
                        if (new_x == max_x)
                        {
                            new_x = 1;
                        }
                    }
                    else if (blizzard.Item3 == 1)
                    {
                        new_y++;
                        if (new_y == max_y)
                        {
                            new_y = 1;
                        }
                    }
                    else if (blizzard.Item3 == 2)
                    {
                        new_x--;
                        if (new_x == 0)
                        {
                            new_x = max_x - 1;
                        }
                    }
                    else if (blizzard.Item3 == 3)
                    {
                        new_y--;
                        if (new_y == 0)
                        {
                            new_y = max_y - 1;
                        }
                    }
                    Tuple<int, int, int> new_blizzard = new Tuple<int, int, int>(new_x, new_y, blizzard.Item3);
                    blizzards_this_min.Add(new Tuple<int, int>(new_x, new_y));
                    states[minute].Add(new_blizzard);
                }
                positions[minute] = new HashSet<Tuple<int, int, int, int>>();
                foreach (Tuple<int, int, int, int> curr_position in positions[minute - 1])
                {
                    Tuple<int, int, int, int>[] potentials = new Tuple<int, int, int, int>[Constants.NUM_DIRS + 1];
                    potentials[0] = new Tuple<int, int, int, int> (curr_position.Item1, curr_position.Item2 - 1, curr_position.Item3, curr_position.Item4);
                    potentials[1] = new Tuple<int, int, int, int> (curr_position.Item1, curr_position.Item2 + 1, curr_position.Item3, curr_position.Item4);
                    potentials[2] = new Tuple<int, int, int, int> (curr_position.Item1 - 1, curr_position.Item2, curr_position.Item3, curr_position.Item4);
                    potentials[3] = new Tuple<int, int, int, int> (curr_position.Item1 + 1, curr_position.Item2, curr_position.Item3, curr_position.Item4);
                    potentials[4] = new Tuple<int, int, int, int> (curr_position.Item1, curr_position.Item2, curr_position.Item3, curr_position.Item4);

                    foreach (Tuple<int, int, int, int> potential in potentials)
                    {
                        if (blizzards_this_min.Contains(new Tuple<int, int>(potential.Item1, potential.Item2)))
                        {
                            continue;
                        } 
                        if (potential.Item2 < 0 || (potential.Item2 == 0 && potential.Item1 != top_opening))
                        {
                            continue;
                        }
                        if (potential.Item2 > max_y || (potential.Item2 == max_y && potential.Item1 != bottom_opening))
                        {
                            continue;
                        }
                        if (potential.Item1 <= 0 || potential.Item1 >= max_x)
                        {
                            continue;
                        }
                        if (potential.Item1 == bottom_opening && potential.Item2 == max_y && potential.Item3 == 0)
                        {
                            if (!goal_seen)
                            {
                                Console.WriteLine("First Goal hit: " + minute);
                                goal_seen = true;
                            }
                            positions[minute].Add(new Tuple<int, int, int, int>(potential.Item1, potential.Item2, potential.Item3, 1));
                        }
                        else if (potential.Item1 == bottom_opening && potential.Item2 == max_y && potential.Item3 > 0)
                        {
                            got_out = true;
                            Console.WriteLine();
                            Console.WriteLine("Second End Goal hit: " + minute);
                            break; 
                        }
                        else if (potential.Item1 == top_opening && potential.Item2 == 0 && potential.Item4 > 0)
                        {
                            if (!restart_seen)
                            {
                                Console.WriteLine();
                                Console.WriteLine("Restart hit: " + minute);
                                restart_seen = true;
                            }
                            positions[minute].Add(new Tuple<int, int, int, int>(potential.Item1, potential.Item2, 1, potential.Item4));
                        }
                        else
                        {
                            positions[minute].Add(potential);
                        }
                    }
                    if (got_out)
                    {
                        break;
                    }
                }
                if (got_out)
                {
                    break;
                }
                //Console.Write(" " + minute + " (" + positions[minute].Count + ")");
                minute++;
            }
        }
    }
}