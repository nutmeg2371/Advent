﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

static class Constants
{
  public const int GRID_SIZE = 1311;
  public const int NUM_CHARS = 8;
}

namespace Year2Day13
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            List<Packet> even_packets = new List<Packet>();
            List<Packet> odd_packets = new List<Packet>();
            bool even_packet = true;

            while ((curr_line = reader.ReadLine()) != null) 
            {
                if (curr_line == "")
                {
                    continue;
                }
                if (even_packet)
                {
                    even_packets.Add(new Packet(curr_line));
                }
                else
                {
                    odd_packets.Add(new Packet(curr_line));
                }
                even_packet = !even_packet;
            }
            reader.Close();

            int part1_sum = 0;
            for (int curr_packet_index = 0; curr_packet_index < even_packets.Count; curr_packet_index++)
            {
                if (even_packets[curr_packet_index].CompareTo(odd_packets[curr_packet_index]) != 1)
                {
                    part1_sum += (curr_packet_index + 1) ;
                }
            }
            Console.WriteLine("Part 1 Sum: " + part1_sum);
            
            List<Packet> all_packets = new List<Packet>();
            all_packets.AddRange(even_packets);
            all_packets.AddRange(odd_packets);
            all_packets.Add(new Packet("[[2]]"));
            all_packets.Add(new Packet("[[6]]"));
            all_packets.Sort();

            int part2_sum = 1;
            for (int curr_packet_index = 0; curr_packet_index < all_packets.Count; curr_packet_index++)
            {
                if (all_packets[curr_packet_index].packet_contents == "[[2]]" || all_packets[curr_packet_index].packet_contents == "[[6]]")
                {
                    part2_sum *= (curr_packet_index + 1);
                }
            }
            Console.WriteLine("Part 2 Sum: " + part2_sum);
        }

        
        
        
    }
    public class Packet : IEquatable<Packet> , IComparable<Packet>
    {
        public string packet_contents { get; set; }

        public Packet (string contents)
        {
            packet_contents = contents;
        }
        public int SortByNameAscending(string packet1, string packet2)
        {
            return packet1.CompareTo(packet2);
        }

        // Default comparer for Part type.
        public int CompareTo(Packet comparePart)
        {
            // A null value means that this object is greater.
            if (comparePart == null)
                return 1;

            else
                return ComparePacketStrings(this.packet_contents, comparePart.packet_contents);
        }
        public bool Equals(Packet other)
        {
            if (other == null) return false;
            return (ComparePacketStrings(this.packet_contents, other.packet_contents) == 0);
        }

        public static int ComparePacketStrings (string packet1, string packet2)
        {
            if (!packet1.Contains("[") && !packet2.Contains("["))
            {
                return (int.Parse(packet1).CompareTo(int.Parse(packet2)));
            }
            else if (!packet1.Contains("["))
            {
                return ComparePacketStrings("[" + packet1 + "]", packet2);
            }
            else if (!packet2.Contains("["))
            {
                return ComparePacketStrings(packet1, "[" + packet2 + "]");
            }

            packet1 = packet1.Substring(1, packet1.Length-2);
            packet2 = packet2.Substring(1, packet2.Length-2);

            string[] packet1_split = SplitPacket(packet1);
            string[] packet2_split = SplitPacket(packet2);

            int curr_packet_index = 0;
            int this_comp = 0;
            while(true)
            {
                if (curr_packet_index < packet1_split.Count() && curr_packet_index < packet2_split.Count())
                {
                    this_comp = ComparePacketStrings(packet1_split[curr_packet_index], packet2_split[curr_packet_index]);
                    if (this_comp != 0)
                    {
                        return this_comp;   
                    }
                }
                else if (curr_packet_index < packet1_split.Count())
                {
                    return 1;
                }
                else if (curr_packet_index < packet2_split.Count())
                {
                    return -1;
                }
                else
                {
                    break;
                }
                curr_packet_index++;
            }
            return this_comp;
        }
        public static string[] SplitPacket (string packet)
        {
            List<string> packet_tokens = new List<string>();

            string curr_token = "";
            int brackets = 0;

            foreach (char c in packet)
            {
                if (c == ',' && brackets == 0)
                {
                    packet_tokens.Add(curr_token);
                    curr_token = "";
                }
                else if (c == '[')
                {
                    curr_token+= c;
                    brackets++;
                }
                else if (c == ']')
                {
                    curr_token+= c;
                    brackets--;
                }
                else
                {
                    curr_token+=c;
                }
            }
            if (curr_token.Length > 0)
            {
                packet_tokens.Add(curr_token);
            }
            return packet_tokens.ToArray();
        }
    }
}
