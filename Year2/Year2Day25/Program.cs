﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Year2Day25
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = "";

            double input_total = 0;

            while ((curr_line = reader.ReadLine()) != null) 
            {
                curr_line = curr_line.Trim();
                char[] curr_line_array = curr_line.ToCharArray();
                Array.Reverse(curr_line_array);
                curr_line = string.Join(",", curr_line_array);
                curr_line = curr_line.Replace("-", "-1");
                curr_line = curr_line.Replace("=", "-2");
                string[] curr_line_str_array = curr_line.Split(",");
                int[] snafu = Array.ConvertAll(curr_line_str_array, x => int.Parse(x.ToString()));
                input_total+= SnafuToDecimal(snafu);     
            }
            Console.WriteLine(DecimalToSnafu(input_total));
            reader.Close();
        }
        public static double SnafuToDecimal(int[] snafu)
        {
            double answer = 0;
            int power = 0;
            foreach (int i in snafu)
            {
                answer += (i * Math.Pow(5, power++));
            }
            return answer;
        }
        public static string DecimalToSnafu(double in_dec)
        {
            int highest_power = 0;
            while (in_dec/Math.Pow(5, highest_power++) >= 1);
            highest_power -= 2;

            int[] snafu = new int[highest_power + 1];
            for (int p = highest_power; p >= 0; p--)
            {
                int mult = (int)(in_dec/Math.Pow(5, p));
                in_dec = in_dec - Math.Pow(5, p) * mult;
                snafu[p] = mult;
            }

            double extra = 0;
            for (int p = 0; p <= highest_power; p++)
            {
                if (snafu[p] > 2)
                {
                    snafu[p+1]++;
                    extra = Math.Pow(5, p + 1);
                    snafu[p] -= 5;
                }
            }

            Array.Reverse(snafu);
            string answer = string.Join("",snafu);
            answer = answer.Replace("-2", "=");
            answer = answer.Replace("-1", "-");
            return answer;
        }
    }
}

           