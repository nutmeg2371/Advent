﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Numerics;
using System.Text.RegularExpressions;

static class Constants
{   
    public const int NUMDIRS = 4;
}

namespace Year2Day22
{
    class Program
    {
        static char[,] map = null;

        static int[,] cube_map = null;
        static int map_width = 0;
        static int map_height = 0;
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = ""; 

            List<string> map_lines = new List<string>();
            int[] movement_counts = null;
            int[] turn_dirs = null;
            int turn_index = 0;

            while ((curr_line = reader.ReadLine()) != null) 
            {
                if (curr_line == "")
                {
                    continue;
                }
                if (curr_line.Contains(".") || curr_line.Contains("#"))
                {
                    map_lines.Add(curr_line);
                    map_width = Math.Max(map_width, curr_line.Length);
                }  
                else
                {
                    map_height = map_lines.Count;
                    movement_counts = Array.ConvertAll(curr_line.Split(new Char [] {'R' , 'L' }), x => int.Parse(x));
                    turn_dirs = Array.ConvertAll((Regex.Replace(curr_line, @"[\d-]", string.Empty)).ToCharArray(), x=> (x == 'R' ? 1 : -1));
                }
            }

            map = new char[map_width + 2, map_height + 2];
            int x = 1;
            int y = 1;
            foreach (string curr_map_line in map_lines)
            {
                foreach (char c in curr_map_line)
                {
                    if (c == '.' || c == '#')
                    {
                        map[x, y] = c;
                    }
                    x++;
                }
                x = 1;
                y++;
            }
            int start_x = map_lines[0].IndexOf('.') + 1;

            x = start_x;
            y = 1;
            int facing = 0;
            
            turn_index = 0;
            foreach (int curr_mvmt in movement_counts)
            {
                MoveInMap(ref x, ref y, ref facing, curr_mvmt, (turn_index < turn_dirs.Count()? turn_dirs[turn_index++] : 0), false);
            }

            int part1 = 1000 * y + 4 * x + facing;
            Console.WriteLine(part1);

            RefoldInput();

            FigureOutCubeSides();

            x = 1;
            y = 1;
            for (x = 0; x < map_width; x++)
            {
                if (map[x, 1] == '.' || map[x, 1] == '#')
                {
                    start_x = x;
                    break;
                }
            }

            x = start_x;
            y = 1;
            facing = 0;
            
            turn_index = 0;
            foreach (int curr_mvmt in movement_counts)
            {
                MoveInMap(ref x, ref y, ref facing, curr_mvmt, (turn_index < turn_dirs.Count()? turn_dirs[turn_index++] : 0), true);
            }

            UnfoldOutput(ref x, ref y, ref facing);

            int part2 = 1000 * y + 4 * x + facing;
            Console.WriteLine(part2);

            reader.Close();
        }

        public static void MoveInMap (ref int x, ref int y, ref int facing, int num_steps, int turn, bool part2)
        {
            for (int step_num = 0; step_num < num_steps; step_num++)
            {
                int delta = ((facing == 0 || facing == 1) ? 1: -1);
                if (facing == 0 || facing == 2)
                { 
                    char next_char = map[x + delta, y];
                    if (next_char == '.')
                    {
                        x += delta;
                    }
                    else if (next_char == '#')
                    {
                        break;
                    }
                    else if (next_char == '\0')
                    {
                        if (part2)
                        {
                            if (!Part2Wrapping(ref x, ref y, ref facing))
                            {
                                break;
                            }
                        }
                        else 
                        {
                            Part1Wrapping(ref x, ref y, delta, facing);
                        }
                    }
                }    
                else
                { 
                    char next_char = map[x, y + delta];
                    if (next_char == '.')
                    {
                        y += delta;
                    }
                    else if (next_char == '#')
                    {
                        break;
                    }
                    else if (next_char == '\0')
                    {
                        if (part2)
                        {
                            if (!Part2Wrapping(ref x, ref y, ref facing))
                            {
                                break;
                            }
                        }
                        else 
                        {
                            Part1Wrapping(ref x, ref y, delta, facing);
                        }
                    }
                }               
            }
            facing += turn;
            facing %= Constants.NUMDIRS;
            if (facing < 0)
            {
                facing = Constants.NUMDIRS + facing;
            }
        }
        public static void Part1Wrapping (ref int x, ref int y, int delta, int facing)
        {
           if (facing == 0 || facing == 2)
           {
               int wrap_x = (facing == 0 ? 0 : map_width + 1);
                while (map[wrap_x + delta, y] == '\0')
                {
                    wrap_x += delta;
                }
                if (map[wrap_x + delta, y] == '#')
                {
                    return;
                }
                else
                {
                    x = wrap_x += delta;
                    return;
                }
           }
           else
           {
                int wrap_y = (facing == 1 ? 0 : map_height + 1);
                while (map[x, wrap_y + delta] == '\0')
                {
                    wrap_y += delta;
                }
                if (map[x, wrap_y + delta] == '#')
                {
                    return;
                }
                else
                {
                    y = wrap_y + delta;
                }
           }
        }
        public static void FigureOutCubeSides()
        {
            cube_map = new int[map_width + 2, map_height + 2];
            int cube_size = map_width/4;
            for (int y = 1; y <= cube_size; y++)
            {
                for (int x = 2 * cube_size + 1; x <= 3 * cube_size; x++)
                {
                    cube_map[x, y] = 1;
                }
            }
            for (int y = cube_size + 1; y <= 2 * cube_size; y++)
            {
                for (int x = 1; x <= cube_size; x++)
                {
                    cube_map[x, y] = 2;
                }
                for (int x = cube_size + 1; x <= 2 * cube_size; x++)
                {
                    cube_map[x, y] = 3;
                }
                for (int x = 2 * cube_size + 1; x <= 3 * cube_size; x++)
                {
                    cube_map[x, y] = 4;
                }
            }
            for (int y = 2 * cube_size + 1; y <= 3 * cube_size; y++)
            {
                for (int x = 2 * cube_size + 1; x <= 3 * cube_size; x++)
                {
                    cube_map[x, y] = 5;
                }
                for (int x = 3 * cube_size + 1; x <= 4 * cube_size; x++)
                {
                    cube_map[x, y] = 6;
                }
            }
        }

        public static bool Part2Wrapping (ref int x, ref int y, ref int facing)
        {
            int wrap_x = 0;
            int wrap_y = 0;
            int wrap_facing = 0;
            int dist_into_cube = 0;

            int cube_size = map_width/4;
            switch (cube_map[x, y])
            {
                case 1:
                {
                    if (facing == 3)
                    {
                        dist_into_cube = x - (2 * cube_size + 1);
                        wrap_x = cube_size - dist_into_cube;
                        wrap_y = cube_size + 1;
                        wrap_facing = 1;
                        break;
                    }
                    else if (facing == 2)
                    {
                        dist_into_cube = y - 1; 
                        wrap_x = cube_size + 1 + dist_into_cube;
                        wrap_y = cube_size + 1;
                        wrap_facing = 1;
                        break;
                    }
                    else if (facing == 0)
                    {
                        dist_into_cube = y - 1; 
                        wrap_x = 4 * cube_size;
                        wrap_y = 3 * cube_size - dist_into_cube;
                        wrap_facing = 2;
                        break;
                    }
                    break;//should not get here
                }
                case 2:
                {
                    if (facing == 3)
                    {
                        dist_into_cube = x - 1; 
                        wrap_x = 3 * cube_size - dist_into_cube;
                        wrap_y = 1;
                        wrap_facing = 1;
                        break;
                    }
                    else if (facing == 1)
                    {
                        dist_into_cube = x - 1; 
                        wrap_x = 3 * cube_size - dist_into_cube;
                        wrap_y = 3 * cube_size;
                        wrap_facing = 3;
                        break;
                    }
                    else if (facing == 2)
                    {
                        dist_into_cube = y - (cube_size + 1); 
                        wrap_y = 3 * cube_size;
                        wrap_x = 4 * cube_size - dist_into_cube;
                        wrap_facing = 3;
                        break;
                    }
                    break;//should not get here
                }
                case 3:
                {
                    if (facing == 3)
                    {
                        dist_into_cube = x - (cube_size + 1);
                        wrap_x = 2 * cube_size + 1;
                        wrap_y = 1 + dist_into_cube;
                        wrap_facing = 0;
                        break;
                    }
                    else if (facing == 1)
                    {
                        dist_into_cube = x - (cube_size + 1);
                        wrap_x = 2 * cube_size + 1;
                        wrap_y = 3 * cube_size - dist_into_cube;
                        wrap_facing = 0;
                        break;
                    }
                    break;//should not get here
                }
                case 4:
                {
                    if (facing == 0)
                    {
                        dist_into_cube = y - (cube_size + 1); 
                        wrap_x = 4 * cube_size - dist_into_cube;
                        wrap_y = 2 * cube_size + 1;
                        wrap_facing = 1;
                        break;
                    }
                    break;//should not get here
                }
                case 5:
                {
                    if (facing == 1)
                    {
                        dist_into_cube = x - (2 * cube_size + 1);
                        wrap_x = cube_size - dist_into_cube;
                        wrap_y = 2 * cube_size;
                        wrap_facing = 3;
                        break;
                    }
                    else if (facing == 2)
                    {
                        dist_into_cube = y - (2 * cube_size + 1);
                        wrap_x = 2 * cube_size + - dist_into_cube;
                        wrap_y = 2 * cube_size;
                        wrap_facing = 3;
                        break;
                    }
                    break;//should not get here
                }
                case 6:
                {
                    if (facing == 3)
                    {
                        dist_into_cube = x - (3 * cube_size + 1);
                        wrap_x = 3 * cube_size;
                        wrap_y = 2 * cube_size - dist_into_cube;
                        wrap_facing = 2;
                        break;
                    }
                    else if (facing == 1)
                    {
                        dist_into_cube = x - (3 * cube_size + 1);
                        wrap_x = 1;
                        wrap_y = 2 * cube_size - dist_into_cube;
                        wrap_facing = 0;
                        break;
                    }
                    else if (facing == 0)
                    {
                        dist_into_cube = y - (2 * cube_size + 1); 
                        wrap_x = 3 * cube_size;
                        wrap_y = cube_size - dist_into_cube;
                        wrap_facing = 2;
                        break;
                    }
                    break;//should not get here
                }
            }
            if (map[wrap_x, wrap_y] != '#')
            {
                x = wrap_x;
                y = wrap_y;
                facing = wrap_facing;
                return true;
            }
            return false;
        }

        public static void UnfoldOutput (ref int x, ref int y, ref int facing)
        {
            int cube_size = map_width/4; 

            if (cube_map[x, y] == 2) 
            {
                int dist_in_x = x - 1;
                int dist_in_y = y - 1;
                y = 4 * cube_size - dist_in_x;
                x = cube_size + 1 + dist_in_y;
                x-= cube_size;
                facing--;
                if (facing < 0)
                {
                    facing = Constants.NUMDIRS + facing;
                }
            }
            else if (cube_map[x, y] == 3)
            {
                int dist_in_x = x - (cube_size + 1);
                int dist_in_y = y - (cube_size + 1);
                y = 3 * cube_size - dist_in_x;
                x = cube_size + 1 + dist_in_y;
                x-= cube_size;
                facing--;
                if (facing < 0)
                {
                    facing = Constants.NUMDIRS + facing;
                }
            }
            else if (cube_map[x, y] == 6)
            {
                int dist_in_x = x - (3 * cube_size + 1);
                int dist_in_y = y - (2 * cube_size + 1);
                x = 4 * cube_size - dist_in_x;
                y = cube_size - dist_in_y;
                x-= cube_size;
                facing-= 2;
                if (facing < 0)
                {
                    facing = Constants.NUMDIRS + facing;
                }
            }
        }

        public static void RefoldInput()
        {
            map_width = Math.Max(map_width, map_height);
            map_height = Math.Max(map_width, map_height);

            char[,] new_map = new char[map_width + 2, map_height + 2];

            int cube_size = map_width/4;
            int new_x = 0;
            int new_y = 0;

            for (int y = 1; y <= map_height; y++)
            {
                for (int x = 4 * cube_size; x >= cube_size + 1; x--)
                {
                    new_map[x,y] = map[x-cube_size, y];
                }
            }

            map = (char[,]) new_map.Clone();

            new_y = 3 * cube_size;
            for (int y = 1; y <= cube_size; y++)
            {
                new_x = 4 * cube_size;
                for (int x = 3 * cube_size + 1; x <= 4 * cube_size; x++)
                {
                    map[new_x--, new_y] = map[x,y];
                    map[x,y] = '\0';
                }
                new_y--;
            }

            new_x = 2 * cube_size;
            for (int y = 2 * cube_size + 1; y <= 3 * cube_size; y++)
            {
                new_y = cube_size + 1;
                for (int x = cube_size + 1; x <= 2 * cube_size; x++)
                {
                    map[new_x, new_y++] = map[x,y];
                    map[x,y] = '\0';
                }
                new_x--;
            }

            new_x = cube_size;
            for (int y = 3 * cube_size + 1; y <= 4 * cube_size; y++)
            {
                new_y = cube_size + 1;
                for (int x = cube_size + 1; x <= 2 * cube_size; x++)
                {
                    map[new_x, new_y++] = map[x,y];
                    map[x,y] = '\0';
                }
                new_x--;
            }
        }
    }
}
