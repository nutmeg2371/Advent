﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

static class Constants
{   
    public const int NUM_ROUNDS = 10; 
    public const int MULTIPLIER = 811589153; 
}
namespace Year2Day20
{
    class Program
    {
        static double[] original_numbers; 
        static int[] current_positions; 
        static double[] current_array;

        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = "";

            List<int> all_input_nums = new List<int>();

            while ((curr_line = reader.ReadLine()) != null && curr_line != "") 
            {
                all_input_nums.Add(int.Parse(curr_line));
            }
            reader.Close();

            original_numbers = new double[all_input_nums.Count];
            current_positions = new int[all_input_nums.Count];

            int  zero_pos = -1;

            for (int i = 0; i < all_input_nums.Count; i++)
            {
                original_numbers[i] = (double) all_input_nums[i] * Constants.MULTIPLIER;
                current_positions[i] = i;
                if (original_numbers[i] == 0)
                {
                    zero_pos = i;
                }
            }
            
            current_array = (double[]) original_numbers.Clone();
            
            //Console.WriteLine(string.Join(",", current_array));

            int round_num = 0;

            for (int i = 0; i < all_input_nums.Count; i++)
            {
                int prev_pos = current_positions[i];
                int next_pos = GetNextPos(prev_pos, original_numbers[i]);

                bool to_right = (next_pos > current_positions[i]);
                bool to_left = (next_pos <current_positions[i]);
                if (to_right)
                {
                    for (int j = current_positions[i]; j < next_pos; j++)
                    {   
                        current_array[j] = current_array[j+1];
                    }
                    int[] updated_positions = (int[]) current_positions.Clone();
                    for (int k = 0; k < original_numbers.Count(); k++)
                    {
                        if (current_positions[k] > prev_pos && current_positions[k] <= next_pos)
                        {
                            updated_positions[k]--;
                        }
                    }
                    current_positions = updated_positions;
                }
                else if (to_left)
                {
                    for (int j = current_positions[i]; j > next_pos; j--)
                    {   
                        current_array[j] = current_array[j-1];
                    }
                    int[] updated_positions = (int[]) current_positions.Clone();
                    for (int k = 0; k < original_numbers.Count(); k++)
                    {
                        if (current_positions[k] < prev_pos && current_positions[k] >= next_pos)
                        {
                            updated_positions[k]++;
                        }
                    }
                    current_positions = updated_positions;
                }
                current_array[next_pos] = original_numbers[i];
                current_positions[i] = next_pos;
               // Console.WriteLine(string.Join(",", current_array));

                if (i == original_numbers.Count() - 1)
                {
                    round_num++;
                    Console.WriteLine(round_num);
                    i = -1;
                }
                if (round_num == Constants.NUM_ROUNDS)
                {
                    break;
                }
            }

            zero_pos = current_positions[zero_pos];
            double groove_1 = current_array[(1000 + zero_pos)%original_numbers.Count()];
            double groove_2 = current_array[(2000 + zero_pos)%original_numbers.Count()];
            double groove_3 = current_array[(3000 + zero_pos)%original_numbers.Count()];
            Console.WriteLine(groove_1 + groove_2 + groove_3);
            
        }
        public static int GetNextPos (int prev_pos, double num_to_move)
        {
            if (num_to_move == 0)
            {
                return prev_pos;
            }
            int positions_to_move = (int)(num_to_move % (original_numbers.Count() - 1));
            int next_pos = prev_pos + positions_to_move;
            if (next_pos <= 0)
            {
                next_pos = original_numbers.Count() + next_pos - 1;
            }
            else if (next_pos >= original_numbers.Count() - 1)
            {
                next_pos = next_pos - original_numbers.Count() + 1;
            }

            return next_pos;

        }
    }
}
