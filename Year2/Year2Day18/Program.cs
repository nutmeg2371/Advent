﻿using System;
using System.IO;
using System.Collections.Generic;

static class Constants
{   
    public const int NUM_COORDS = 3; 
}
namespace Year2Day18
{

    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            int max_coord = 0;
            int min_coord = int.MaxValue;

            HashSet<Tuple<int,int,int>> coords_set = new HashSet<Tuple<int, int, int>>();
            
            while ((curr_line = reader.ReadLine()) != null) 
            {
                int[] curr_coords = Array.ConvertAll(curr_line.Split(","), s => int.Parse(s));
                coords_set.Add(new Tuple<int, int, int>(curr_coords[0], curr_coords[1], curr_coords[2]));
                max_coord = Math.Max(max_coord, curr_coords[0]);
                max_coord = Math.Max(max_coord, curr_coords[1]);
                max_coord = Math.Max(max_coord, curr_coords[2]);
                min_coord = Math.Min(min_coord, curr_coords[0]);
                min_coord = Math.Min(min_coord, curr_coords[1]);
                min_coord = Math.Min(min_coord, curr_coords[2]);
            }
            reader.Close();

            int all_surface_area = FindAllSurfaceArea(coords_set);           
            Console.WriteLine("Part 1:" + all_surface_area);

            Tuple<int,int,int> corner_node = new Tuple<int, int, int>(min_coord - 1, min_coord - 1, min_coord -1);
            HashSet<Tuple<int,int,int>> outer_air = Flood(coords_set, corner_node, min_coord, max_coord);
            HashSet<Tuple<int,int,int>> interior_air = new HashSet<Tuple<int, int, int>>();
            
            for (int x = min_coord - 1; x <= max_coord + 1; x++)
            {
                for (int y = min_coord - 1; y <= max_coord + 1; y++)
                {
                    for (int z = min_coord - 1; z <= max_coord + 1; z++)
                    {
                        Tuple<int, int, int> test_node = new Tuple<int, int, int>(x, y, z);
                        if (!outer_air.Contains(test_node) && !coords_set.Contains(test_node))
                        {
                            interior_air.Add (test_node);
                        }
                    }
                }
            }

            foreach (Tuple<int, int, int> interior_node in interior_air)
            {
                coords_set.Add(interior_node);
            }

            int exterior_surface_area = FindAllSurfaceArea(coords_set);           
            Console.WriteLine("Part 2:" + exterior_surface_area);
        }

        public static HashSet<Tuple<int,int,int>> Flood (HashSet<Tuple<int,int,int>> coords_set, Tuple<int, int, int> start_node, int min_coord, int max_coord)
        {
            HashSet<Tuple<int,int,int>> flooded = new HashSet<Tuple<int, int, int>>();

            List<Tuple<int, int, int>> process_queue = new List<Tuple<int, int, int>>();
            process_queue.Add(start_node);

            while (process_queue.Count > 0)
            {
                Tuple<int, int, int> curr_node = process_queue[0];
                process_queue.RemoveAt(0);

                if (!flooded.Contains(curr_node) && !coords_set.Contains(curr_node))
                {
                    flooded.Add(curr_node);
                    List<Tuple<int, int, int>> neighbors = new List<Tuple<int, int, int>>();                    
                    neighbors.Add(new Tuple<int, int, int>(curr_node.Item1 - 1, curr_node.Item2, curr_node.Item3));
                    neighbors.Add(new Tuple<int, int, int>(curr_node.Item1 + 1, curr_node.Item2, curr_node.Item3));
                    neighbors.Add(new Tuple<int, int, int>(curr_node.Item1, curr_node.Item2 - 1, curr_node.Item3));
                    neighbors.Add(new Tuple<int, int, int>(curr_node.Item1, curr_node.Item2 + 1, curr_node.Item3));
                    neighbors.Add(new Tuple<int, int, int>(curr_node.Item1, curr_node.Item2, curr_node.Item3 - 1));
                    neighbors.Add(new Tuple<int, int, int>(curr_node.Item1, curr_node.Item2, curr_node.Item3 + 1));
                    foreach (Tuple<int, int, int> neighbor in neighbors)
                    {
                        if (neighbor.Item1 >= min_coord - 1 && neighbor.Item1 <= max_coord + 1 && neighbor.Item2 >= min_coord - 1 && neighbor.Item2 <= max_coord + 1 && neighbor.Item3 >= min_coord - 1 && neighbor.Item3 <= max_coord + 1)
                        {
                            process_queue.Add(neighbor);
                        }
                    }
                }
            }
            return flooded;
        }

        public static int FindAllSurfaceArea (HashSet<Tuple<int, int, int>> coords_set)
        { 
            int surface_area = 0;
            foreach (Tuple<int, int, int> curr_coord in coords_set)
            {
                surface_area += (!coords_set.Contains(new Tuple<int, int, int>(curr_coord.Item1 + 1, curr_coord.Item2, curr_coord.Item3)) ? 1 : 0);
                surface_area += (!coords_set.Contains(new Tuple<int, int, int>(curr_coord.Item1 - 1, curr_coord.Item2, curr_coord.Item3)) ? 1 : 0);
                surface_area += (!coords_set.Contains(new Tuple<int, int, int>(curr_coord.Item1, curr_coord.Item2 + 1, curr_coord.Item3)) ? 1 : 0);
                surface_area += (!coords_set.Contains(new Tuple<int, int, int>(curr_coord.Item1, curr_coord.Item2 - 1, curr_coord.Item3)) ? 1 : 0);
                surface_area += (!coords_set.Contains(new Tuple<int, int, int>(curr_coord.Item1, curr_coord.Item2, curr_coord.Item3 + 1)) ? 1 : 0);
                surface_area += (!coords_set.Contains(new Tuple<int, int, int>(curr_coord.Item1, curr_coord.Item2, curr_coord.Item3 - 1)) ? 1 : 0);
            }
            return surface_area;
        }   
    }
}
