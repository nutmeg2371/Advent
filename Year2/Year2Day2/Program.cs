﻿using System;
using System.IO;

namespace Advent
{
    class Year2Day2
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            int score = 0;

            bool part_2 = true;
            while ((curr_line = reader.ReadLine()) != null) 
            {
                char[] plays_chars = (char[]) curr_line.ToCharArray();
                
                if (part_2)
                {
                    switch (plays_chars[2])
                    {
                        case 'X':
                        {
                            plays_chars[2] = (char)((int)plays_chars[0] - 1 + 23);
                            if (plays_chars[2] == 'W')
                            {
                                plays_chars[2] = 'Z';
                            }
                            break;
                        }
                        case 'Y':
                        {
                            plays_chars[2] = (char) ((int)plays_chars[0] + 23);
                            break;
                        }
                        case 'Z':
                        {
                            plays_chars[2] = (char)((int)plays_chars[0] + 1 + 23);
                            if (plays_chars[2] == '[')
                            {
                                plays_chars[2] = 'X';
                            }
                            break;
                        }
                    }
                }

                score += (plays_chars[2] - 87);

                int distance = plays_chars[2] - plays_chars[0];

                switch(distance)
                {
                    case 23:
                    {
                        score += 3;
                        break;
                    }
                    case 24:
                    {
                        score += 6;
                        break;
                    }
                    case 21:
                    {
                        score += 6;
                        break;
                    }
                }
            }
            Console.WriteLine(score);
            reader.Close();
        }
    }
}
