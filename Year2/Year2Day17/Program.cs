﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Year2Day17
{
    static class Constants
    {   
        public const int TOWER_WIDTH = 7;
        public const ulong TOWER_HEIGHT = 100000000;
        public const int ROCK_WIDTH = 4;
        public const int ROCK_HEIGHT = 4;
        public const int ROCK_X_START = 2;
        public const int ROCK_Y_GAP_START = 4;
        public const double TARGET_TOTAL_BLOCKS = 1000000000000;
        public const int MAX_ITERATIONS = 100000;
        public const int MATCH_COUNT_THRESHOLD = 7;
    }

    class Program
    {
        static char[,] tower = new char [Constants.TOWER_WIDTH, Constants.TOWER_HEIGHT];
        static Tuple<double, ulong>[,] history;

        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");

            // rock num, x, y
            int[,] rock1 = new int[,] { {1, 0, 0, 0}, {1, 0, 0, 0}, {1, 0, 0, 0}, {1, 0, 0, 0}};
            int[,] rock2 = new int[,] { {0, 1, 0, 0}, {1, 1, 1, 0}, {0, 1, 0, 0}, {0, 0, 0, 0}};
            int[,] rock3 = new int[,] { {1, 0, 0, 0}, {1, 0, 0, 0}, {1, 1, 1, 0}, {0, 0, 0, 0}};
            int[,] rock4 = new int[,] { {1, 1, 1, 1}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
            int[,] rock5 = new int[,] { {1, 1, 0, 0}, {1, 1, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}};
            List<int[,]> rocks = new List<int[,]>();
            rocks.Add(rock1);
            rocks.Add(rock2);
            rocks.Add(rock3);
            rocks.Add(rock4);
            rocks.Add(rock5);
            
            char[] jet_movements = reader.ReadLine().ToCharArray(); 
            reader.Close();

            ulong curr_pile_height = 0;
            for (int curr_x = 0; curr_x < Constants.TOWER_WIDTH; curr_x++)
            {
                for (ulong curr_y = 0; curr_y < Constants.TOWER_HEIGHT; curr_y++)
                {
                    tower[curr_x, curr_y] = '.';
                }
                tower[curr_x, (ulong) curr_pile_height] = '#';
            }

            int curr_rock_index = 0;
            double num_fallen = 0;
            int jet_index = 0;
            //double tower_dumped = 0;

            /* Rock number, Jet Action number, Iteraction for this pattern -> tower height before */
            history = new Tuple<double, ulong>[jet_movements.Count(), Constants.MAX_ITERATIONS];

            double height_skipped_by_pattern = 0;
            bool pattern_found = false;

            while (num_fallen < Constants.TARGET_TOTAL_BLOCKS)
            {                
                int[,] curr_rock = rocks.ElementAt(curr_rock_index++);
                curr_rock_index %=rocks.Count();

                int rock_x = Constants.ROCK_X_START;
                ulong rock_y = curr_pile_height + Constants.ROCK_Y_GAP_START;

                bool still_falling = true;
                while (still_falling)
                {
                    MoveRock(ref rock_x, rock_y, curr_rock, jet_movements[jet_index++]);
                    jet_index %= jet_movements.Count();
                    if (CheckHitBottom(rock_x, ref rock_y, curr_rock))
                    {
                        SetDownRock(rock_x, rock_y, curr_rock, ref curr_pile_height);
                        num_fallen++;
                        still_falling = false;
                    }
                }
                if (curr_rock_index == 0 && !pattern_found)
                {
                    pattern_found = CheckHistory(jet_index, ref num_fallen, curr_pile_height, ref height_skipped_by_pattern);
                }
            }

            Console.WriteLine(curr_pile_height + height_skipped_by_pattern);
        }

        public static bool CheckHistory (int jet_num, ref double blocks_fallen, ulong curr_pile_height, ref double pile_height_skipped)
        {
            int next_iteration = 0;
            for (next_iteration = 0; next_iteration < Constants.MAX_ITERATIONS; next_iteration++)
            {
                if (history[jet_num, next_iteration] == null)
                {
                    break;
                }
            }
            history[jet_num, next_iteration] = new Tuple<double, ulong>(blocks_fallen, curr_pile_height);
            
            if (next_iteration < Constants.MATCH_COUNT_THRESHOLD)
            {
                return false;
            }

            int iteration_gap = 1;
            int[] interations_to_check = new int [Constants.MATCH_COUNT_THRESHOLD + 1];
            interations_to_check[0] = next_iteration;
            while (true)
            {
                for (int curr_iteration_to_check = 1; curr_iteration_to_check <= Constants.MATCH_COUNT_THRESHOLD; curr_iteration_to_check++)
                {
                    interations_to_check[curr_iteration_to_check] = interations_to_check[curr_iteration_to_check - 1] - iteration_gap;
                }
                if (interations_to_check[Constants.MATCH_COUNT_THRESHOLD] < 0)
                {
                    break;
                }

                bool match_found = true;
                double last_block_diff = history[jet_num, interations_to_check[0]].Item1 - history[jet_num, interations_to_check[1]].Item1;
                ulong last_height_diff = history[jet_num, interations_to_check[0]].Item2 - history[jet_num, interations_to_check[1]].Item2;

                for (int curr_iteration_to_check = 1; curr_iteration_to_check < Constants.MATCH_COUNT_THRESHOLD; curr_iteration_to_check++)
                {
                    double curr_block_diff = history[jet_num, interations_to_check[curr_iteration_to_check]].Item1 - history[jet_num, interations_to_check[curr_iteration_to_check + 1]].Item1;
                    ulong curr_height_diff = history[jet_num, interations_to_check[curr_iteration_to_check]].Item2 - history[jet_num, interations_to_check[curr_iteration_to_check + 1]].Item2;
                    
                    if (curr_block_diff != last_block_diff && curr_height_diff != last_height_diff)
                    {
                        match_found = false;
                        break;
                    }
                }
                if (match_found)
                {
                    double blocks_left = Constants.TARGET_TOTAL_BLOCKS - blocks_fallen;
                    double iterations = Math.Floor(blocks_left/last_block_diff);
                    blocks_fallen += iterations * last_block_diff;
                    pile_height_skipped = iterations * last_height_diff;
                    return true;
                }
                iteration_gap++;
            }
            return false;         
        }

        public static void SetDownRock(int x, ulong y, int[,] curr_rock, ref ulong curr_pile_height)
        {
            for (int i = 0; i < Constants.ROCK_WIDTH; i++)
            {
                for (int j = 0; j < Constants.ROCK_HEIGHT; j++)
                {
                    if (curr_rock[i, j] == 1)
                    {
                        tower[i + x, (ulong) j + y] = '#';
                        if ((ulong) j + y > curr_pile_height)
                        {
                            curr_pile_height = (ulong) j + y;
                        }
                    }
                }
            }
        }

        public static bool CheckHitBottom(int x, ref ulong y, int[,] curr_rock)
        {
            y--;
            for (int i = 0; i < Constants.ROCK_WIDTH; i++)
            {
                for (int j = 0; j < Constants.ROCK_HEIGHT; j++)
                {
                    if (curr_rock[i, j] == 1 && tower[i + x, (ulong) j + y] == '#')
                    {
                        y++;
                        return true;
                    }
                }
            }
            return false;
        }
        public static bool MoveRock(ref int x, ulong y, int[,] curr_rock, char movement)
        {
            int new_x = (movement == '<' ? x -1 : x + 1);
            
            for (int i = 0; i < Constants.ROCK_WIDTH; i++)
            {
                for (int j = 0; j < Constants.ROCK_HEIGHT; j++)
                {
                    if (curr_rock[i, j] == 1)
                    {
                        if ((i + new_x >= Constants.TOWER_WIDTH || i + new_x < 0) || (tower[i + new_x, (ulong) j + y] == '#'))
                        {
                            return false;
                        }
                        
                    }
                }
            }
            x = new_x;
            return true;
        }
    }
}
