﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;


namespace Year2Day16
{
    static class Constants
    {   
        public const int TIME_LIMIT_P1 = 30;
        public const int TIME_LIMIT_P2 = 26;
        public const String START_NODE = "AA";
    }

    class Program
    {     
        static Dictionary<string, int> flow_rates = new Dictionary<string, int>();  
        static Dictionary<string,HashSet<string>> neighbors = new Dictionary<string, HashSet<string>>();
        static List<string> all_nodes = new List<string>();
        static Dictionary<Tuple<string,string>,int> dijkstra = new Dictionary<Tuple<string, string>, int>();
        static int max_total_flow = 0;

        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            
            while ((curr_line = reader.ReadLine()) != null) 
            {
                curr_line = curr_line.Replace("Valve ","");
                curr_line = curr_line.Replace(" has flow rate="," ");
                curr_line = curr_line.Replace("; tunnels lead to valves "," ");
                curr_line = curr_line.Replace("; tunnel leads to valve "," ");
                curr_line = curr_line.Replace(",","");

                string[] split_str = curr_line.Split();

                flow_rates.Add(split_str[0], int.Parse(split_str[1]));
                neighbors.Add(split_str[0], new HashSet<string>()); 
                for (int edge_index = 2; edge_index < split_str.Length; edge_index++)
                {
                    neighbors[split_str[0]].Add(split_str[edge_index]);
                }
                all_nodes.Add(split_str[0]);
            }
            reader.Close();

            RunDijkstra();
            List<string> non_zero_keys = flow_rates.Where(x => x.Value != 0).Select(pair => pair.Key).ToList<string>();
            Part1(Constants.START_NODE, non_zero_keys, new Tuple<int, int>(Constants.TIME_LIMIT_P1, max_total_flow));
            Console.WriteLine ("Part 1: " + max_total_flow);

            max_total_flow= 0;
            Part2(Constants.START_NODE, Constants.START_NODE, non_zero_keys, non_zero_keys, new Tuple<int, int, int, int>(Constants.TIME_LIMIT_P2, Constants.TIME_LIMIT_P2, max_total_flow, max_total_flow));
            Console.WriteLine ("Part 2: " + max_total_flow);
        }

        public static void RunDijkstra ()
        {
            foreach (string source_node in all_nodes)
            {
                Dictionary<string, int> dist = new Dictionary<string, int>();
                List<string> processing_queue = new List<string>();
                foreach (string curr_node in all_nodes)
                {
                    dist[curr_node] = all_nodes.Count() + 1;
                    processing_queue.Add(curr_node);
                }
                dist[source_node] = 0;

                while (processing_queue.Count > 0)
                {
                    string curr_dest_node = "";
                    int min_dist = all_nodes.Count() + 1;
                    foreach (string curr_candidate_node in processing_queue)
                    {
                        if (dist[curr_candidate_node] < min_dist)
                        {
                            min_dist = dist[curr_candidate_node];
                            curr_dest_node = curr_candidate_node;
                        }
                    }
                    processing_queue.Remove(curr_dest_node);

                    foreach (string dests_neighbor in neighbors[curr_dest_node])
                    {
                        if (min_dist + 1 < dist[dests_neighbor])
                        {
                            dist[dests_neighbor] = min_dist + 1;
                        }
                    }
                }
                foreach (KeyValuePair<string, int> curr_entry in dist)
                {
                    dijkstra[new Tuple<string, string>(source_node, curr_entry.Key)] = curr_entry.Value;
                }
            }
        }
            
         public static Tuple<int,int> GoToNextNode(string source_node, string dest_node, Tuple<int,int> prev_tme_flow)
        {
            int time_left = prev_tme_flow.Item1 - dijkstra[new Tuple<string, string>(source_node, dest_node)] - 1;
            return new Tuple<int, int>(time_left, prev_tme_flow.Item2 + (time_left * flow_rates[dest_node]));
        }
            
        public static Tuple<int,int,int,int> BothGoToNextNode(string my_source_node, string my_dest_node, string elephant_source_node, string elephant_dest_node, Tuple<int,int,int,int> prev_tme_flow)
        {
            int my_time_left = prev_tme_flow.Item1 - dijkstra[new Tuple<string, string>(my_source_node, my_dest_node)] - 1;
            int elephant_time_left = prev_tme_flow.Item2 - dijkstra[new Tuple<string, string>(elephant_source_node, elephant_dest_node)] - 1;
            return new Tuple<int, int, int, int>(my_time_left, elephant_time_left, prev_tme_flow.Item3 + (my_time_left * flow_rates[my_dest_node]),  prev_tme_flow.Item4 + (elephant_time_left * flow_rates[elephant_dest_node]));
        }

        public static void Part1(string source_node, List<string> dest_nodes, Tuple<int,int> prev_time_flow)
        {
            foreach (string curr_dest in dest_nodes)
            {
                Tuple<int,int> result = GoToNextNode(source_node, curr_dest, prev_time_flow);
                if (result.Item1 >= 0)
                {
                    if (result.Item2 > max_total_flow)
                    {
                        max_total_flow = result.Item2;
                        Console.WriteLine("New max: " + max_total_flow);
                    }

                    if (dest_nodes.Count > 1)
                    {
                        Part1(curr_dest, dest_nodes.Where(j => j != curr_dest).ToList<string>(), result);
                    }
                }
            }
        }

        public static void Part2(string my_source_node, string elephant_source_node, List<string> my_dest_nodes, List<string> elephant_dest_nodes, Tuple<int,int,int,int> prev_time_flow)
        {
            HashSet<Tuple<string, string>> dest_pairs = new HashSet<Tuple<string, string>>();
            foreach (string curr_my_dest in my_dest_nodes)
            {
                foreach (string curr_elephant_dest in elephant_dest_nodes)
                {
                    if (curr_my_dest != curr_elephant_dest)
                    {
                        dest_pairs.Add(new Tuple<string, string>(curr_my_dest, curr_elephant_dest));
                    }
                }
            }

            foreach (Tuple<string, string> curr_dest_pair in dest_pairs)
            {
                Tuple<int, int, int, int> result = BothGoToNextNode(my_source_node, curr_dest_pair.Item1, elephant_source_node, curr_dest_pair.Item2, prev_time_flow);

                if (result.Item1 >= 0 && result.Item2 >= 0)
                {
                    int new_flow = result.Item3 + result.Item4;
                    if (new_flow > max_total_flow)
                    {
                        max_total_flow = new_flow;
                        Console.WriteLine("New max: " + max_total_flow);
                    }
                    
                    List<string> new_dest_nodes = my_dest_nodes.Where(j => (j != curr_dest_pair.Item1 && j != curr_dest_pair.Item2)).ToList<string>();

                    if (new_dest_nodes.Count >= 2)
                    {
                        Part2(curr_dest_pair.Item1, curr_dest_pair.Item2, new_dest_nodes, new_dest_nodes, result);
                    }
                    else if (new_dest_nodes.Count == 1)
                    {
                        Part1(curr_dest_pair.Item1, new_dest_nodes, new Tuple<int, int>(result.Item1, new_flow));
                        Part1(curr_dest_pair.Item2, new_dest_nodes, new Tuple<int, int>(result.Item2, new_flow));
                    }                    
                }
                else if (result.Item1 >= 0)
                {
                    int new_flow = result.Item3 + prev_time_flow.Item4;
                    if (new_flow > max_total_flow)
                    {
                        max_total_flow = new_flow;
                        Console.WriteLine("New max: " + max_total_flow);
                    }
                    List<string> my_new_dest_nodes = my_dest_nodes.Where(j => (j != curr_dest_pair.Item1)).ToList<string>();

                    if (my_dest_nodes.Count >= 1)
                    {
                        Part1(curr_dest_pair.Item1, my_new_dest_nodes, new Tuple<int, int>(result.Item1, new_flow));
                    }                    
                }                
                else if (result.Item2 >= 0)
                {
                    int new_flow = result.Item4 + prev_time_flow.Item3;
                    if (new_flow > max_total_flow)
                    {
                        max_total_flow = new_flow;
                        Console.WriteLine("New max: " + max_total_flow);
                    }
                    List<string> elephant_new_dest_nodes = elephant_dest_nodes.Where(j => (j != curr_dest_pair.Item2)).ToList<string>();
                    if (my_dest_nodes.Count >= 1)
                    {
                        Part1(curr_dest_pair.Item2, elephant_new_dest_nodes, new Tuple<int, int>(result.Item2, new_flow));
                    }  
                }
            }
        }
    }
}
