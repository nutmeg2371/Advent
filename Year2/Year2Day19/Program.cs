﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

static class Constants
{   
    public const int TIME_LIMIT_P1 = 24;
    public const int TIME_LIMIT_P2 = 32;
    public const int MAX_BP_PART2 = 32;
    public const int NUM_ROBOT_TYPES = 4;
    public const int NUM_RESOURCE_TYPES = 4;
}

namespace Year2Day19
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = "";
            List<Blueprint> blueprints = new List<Blueprint>();
            Dictionary<int, int> per_blueprint_geodes_p1 = new Dictionary<int, int>();
            Dictionary<int, int> per_blueprint_geodes_p2 = new Dictionary<int, int>();

            while ((curr_line = reader.ReadLine()) != null) 
            {
                if (curr_line == "")
                {
                    continue;
                }
                curr_line = curr_line.Replace("Blueprint ", "");
                curr_line = curr_line.Replace(": Each ore robot costs", "");
                curr_line = curr_line.Replace("ore. Each clay robot costs ", "");
                curr_line = curr_line.Replace("ore. Each obsidian robot costs ", "");
                curr_line = curr_line.Replace(" ore and", "");
                curr_line = curr_line.Replace(" clay. Each geode robot costs", "");
                curr_line = curr_line.Replace(" obsidian.", "");

                int[] blueprint_params = Array.ConvertAll(curr_line.Split(" "), x => int.Parse(x));
                Blueprint curr_blueprint = new Blueprint(blueprint_params[0], blueprint_params[1], blueprint_params[2], blueprint_params[3], blueprint_params[4], blueprint_params[5], blueprint_params[6]);
                blueprints.Add(curr_blueprint);
            }
            reader.Close();


            int blueprint_mults = 1;
            int num_done = 0;
            foreach (Blueprint curr_blueprint in blueprints)
            {
                per_blueprint_geodes_p2[curr_blueprint.blueprint_num] = curr_blueprint.RunBlueprint(Constants.TIME_LIMIT_P2);
                blueprint_mults *= per_blueprint_geodes_p2[curr_blueprint.blueprint_num] * curr_blueprint.blueprint_num;
                num_done++;
                if (num_done == 3)
                break;
            }    
            num_done = 0;
            foreach (Blueprint curr_blueprint in blueprints)
            {
                Console.WriteLine(curr_blueprint.blueprint_num + " " + per_blueprint_geodes_p1[curr_blueprint.blueprint_num]);
                num_done++;
                if (num_done == 3)
                break;
            }   
            Console.WriteLine(blueprint_mults);

            int blueprint_totals = 0;
            foreach (Blueprint curr_blueprint in blueprints)
            {
                per_blueprint_geodes_p1[curr_blueprint.blueprint_num] = curr_blueprint.RunBlueprint(Constants.TIME_LIMIT_P1);
                blueprint_totals += per_blueprint_geodes_p1[curr_blueprint.blueprint_num] * curr_blueprint.blueprint_num;
            }   
            
            foreach (Blueprint curr_blueprint in blueprints)
            {
                Console.WriteLine(curr_blueprint.blueprint_num + " " + per_blueprint_geodes_p1[curr_blueprint.blueprint_num]);
            }          
                
            Console.WriteLine(blueprint_totals);
        }
    }
    public class Blueprint
    {
        public int blueprint_num = 0;
        public int ore_robot_cost = 0;
        public int clay_robot_cost = 0;
        public int obs_robot_ore_cost = 0;
        public int obs_robot_clay_cost = 0;
        public int geode_robot_ore_cost = 0;
        public int geode_robot_obs_cost = 0;

        private int max_ore_cost = 0;
        int max_geodes = 0;

        int time_limit = 0;
        
        // resources, machines, previous resources
        Dictionary<int, HashSet<Tuple<int[], int[], int[]>>> poss_states = new Dictionary<int, HashSet<Tuple<int[], int[], int[]>>>();
        HashSet<string> seen_states = new HashSet<string>();

        public Blueprint (int in_num, int in_ore_cost, int in_clay_cost, int in_obs_ore_cost, int in_obs_clay_cost, int in_geode_ore_cost, int in_geode_obs_cost)
        {
            blueprint_num = in_num;
            ore_robot_cost = in_ore_cost;
            clay_robot_cost = in_clay_cost;
            obs_robot_ore_cost = in_obs_ore_cost;
            obs_robot_clay_cost = in_obs_clay_cost;
            geode_robot_ore_cost = in_geode_ore_cost;
            geode_robot_obs_cost = in_geode_obs_cost;
            max_ore_cost = Math.Max(ore_robot_cost, Math.Max(clay_robot_cost, Math.Max(obs_robot_ore_cost, geode_robot_ore_cost)));
        }

        public int RunBlueprint(int min_left)
        {
            time_limit = min_left;
            poss_states[0] = new HashSet<Tuple<int[], int[], int[]>>();
            poss_states[0].Add(new Tuple<int[], int[], int[]>(new int [] { 0, 0, 0, 0 }, new int [] { 1, 0, 0, 0}, new int [] { 0, 0, 0, 0}));
            for (int curr_time = 1; curr_time <= min_left; curr_time++)
            {
                poss_states[curr_time] = new HashSet<Tuple<int[], int[], int[]>>();
                
                foreach (Tuple<int[], int[], int[]> prev_state in poss_states[curr_time - 1])
                {
                    List<Tuple<int[], int[]>> possible_robot_creations = GenerateRobotCreationPossibilties(prev_state.Item1, prev_state.Item3);

                    // if you have money to buy every type of machine, don't just sit there
                    if (!(prev_state.Item1[0] >= max_ore_cost && prev_state.Item1[1] >= obs_robot_clay_cost && prev_state.Item1[2] >= geode_robot_obs_cost))
                    {
                        // always have the option of waiting and collecting coin
                        int[] after_mining_resources = new int[] {prev_state.Item1[0] + prev_state.Item2[0], prev_state.Item1[1] + prev_state.Item2[1], prev_state.Item1[2] + prev_state.Item2[2], prev_state.Item1[3] + prev_state.Item2[3]};
                        AddNewState(new Tuple<int[], int[], int[]>(after_mining_resources, prev_state.Item2, prev_state.Item1), curr_time);;
                    }

                    foreach (Tuple<int[], int[]> curr_robot_creation in possible_robot_creations)
                    {
                        // add the new robots to what we currently have
                        int[] after_construction_robots = curr_robot_creation.Item2.Zip(prev_state.Item2, (x, y) => x + y).ToArray();

                        // add the mined resources to what was left after we paid to make robots **Resources left plus previous Robots of each type
                        int[] final_resources = curr_robot_creation.Item1.Zip(prev_state.Item2, (x, y) => x + y).ToArray();
   
                        AddNewState(new Tuple<int[], int[], int[]>(final_resources, after_construction_robots, curr_robot_creation.Item1), curr_time);
                    }
                }
                Console.WriteLine(blueprint_num + ": " + (curr_time) + ", " + poss_states[curr_time].Count + " max geodes = " + max_geodes);
            }
            return max_geodes;
        }

        public void AddNewState (Tuple<int[], int[], int[]> state_to_add, int curr_time)
        {      
            // if i have already seen this state earlier, then it's useless
            string cache_str = string.Join(",", state_to_add.Item1) + ":" + string.Join(",", state_to_add.Item2);
            if (seen_states.Contains(cache_str))
            {
                return;
            }
            seen_states.Add(cache_str);

            // no point in creating so many bots you can make multiple of the next one
           if (state_to_add.Item2[2] > geode_robot_obs_cost)
            {
                return;
            }
            if (state_to_add.Item2[1] > obs_robot_clay_cost)
            {
                return;
            }
            if (state_to_add.Item2[0] > ore_robot_cost + clay_robot_cost + obs_robot_ore_cost + geode_robot_ore_cost)
            {
                return;
            }

            // if you took the geodes you have now, built a machine every minute until the second to last, could you beat the best yet?
            int poss_geodes = state_to_add.Item1[3];
            int time_left = time_limit - curr_time;
            poss_geodes += (state_to_add.Item2[3] * time_left);
            poss_geodes += (time_left * (time_left - 1) / 2);

            if (poss_geodes < max_geodes)
            {
                return;
            }

            // Find all states with the same machine set up, and only keep duplicates with uncomparable resources
            /*var states_w_same_robits = poss_states[curr_time].Where(x => CompareResources(x.Item2, state_to_add.Item2) == 0).ToHashSet<Tuple<int[], int[], int[]>>();
            if (states_w_same_robits.Count == 0)
            {
                poss_states[curr_time].Add(state_to_add);
            }

            foreach (Tuple<int[], int[], int[]> curr_comp_state in states_w_same_robits)
            {
                int comp_result = CompareResources(curr_comp_state.Item1, state_to_add.Item1);
                switch (comp_result)
                {
                    case -1:
                    {
                        poss_states[curr_time].Remove(curr_comp_state);
                        poss_states[curr_time].Add(state_to_add);
                        break;
                    }
                    case 0:
                    {
                        break;
                    }
                    case 1:
                    {
                        break;
                    }
                    case 2:
                    {
                        poss_states[curr_time].Add(state_to_add);
                        break;
                    }
                }
            }

            // Find all states with same number of resources and only keep duplicates with more /uncomparable machines (already added above tho)
            var states_w_same_resources = poss_states[curr_time].Where(x => CompareResources(x.Item1, state_to_add.Item1) == 0).ToHashSet<Tuple<int[], int[], int[]>>();
            states_w_same_resources.Remove(state_to_add);

            foreach (Tuple<int[], int[], int[]> curr_comp_state in states_w_same_resources)
            {
                int comp_result = CompareResources(curr_comp_state.Item2, state_to_add.Item2);
                switch (comp_result)
                {
                    case -1:
                    {
                        poss_states[curr_time].Remove(curr_comp_state);
                        break;
                    }
                    case 0:
                    {
                        break;
                    }
                    case 1:
                    {
                        poss_states[curr_time].Remove(state_to_add);
                        break;
                    }
                    case 2:
                    {
                        break;
                    }
                }
            }*/
            poss_states[curr_time].Add(state_to_add);

            max_geodes = Math.Max(max_geodes, state_to_add.Item1[3]);
        } 

        /* -1 x <= y , 0 =, 1 >=, 2 uncomparable */
        public int CompareResources (int[] x, int[] y)
        {
            bool saw_less = false;
            bool saw_greater = false;
            bool saw_equal = false;
            for (int i = 0; i < x.Count(); i++)
            {
                int result = x[i].CompareTo(y[i]);
                saw_less = (result == -1) || saw_less;
                saw_greater = (result == 1) || saw_greater;
                saw_equal = (result == 0) || saw_equal;

                if (saw_greater && saw_less)
                {
                    return 2;
                }
            }
            if (saw_equal && !saw_less && !saw_greater)
            {
                return 0;
            }
            if (saw_greater)
            {
                return 1;
            }
            if (saw_less)
            {
                return -1;
            }
            return -5; // shouldn't get here
        }

        public List<Tuple<int[], int[]>> GenerateRobotCreationPossibilties (int[] resources, int[] prev_resources)
        {
            List<Tuple<int[], int[]>> possibilities = new List<Tuple<int[], int[]>>();

            bool[] prev_possibilities = new bool[Constants.NUM_ROBOT_TYPES];
            prev_possibilities[0] = prev_resources[0] >= ore_robot_cost;
            prev_possibilities[1] = prev_resources[0] >= clay_robot_cost;
            prev_possibilities[2] = prev_resources[0] >= obs_robot_ore_cost && prev_resources[1] >= obs_robot_clay_cost;
            prev_possibilities[3] = prev_resources[0] >= geode_robot_ore_cost && prev_resources[2] >= geode_robot_obs_cost;

            bool[] prev_sole_possibilities = new bool[Constants.NUM_ROBOT_TYPES];
            prev_sole_possibilities[0] = prev_sole_possibilities[0] && !prev_sole_possibilities[1] && !prev_sole_possibilities[2] && !prev_sole_possibilities[3];
            prev_sole_possibilities[1] = prev_sole_possibilities[1] && !prev_sole_possibilities[0] && !prev_sole_possibilities[2] && !prev_sole_possibilities[3];
            prev_sole_possibilities[2] = prev_sole_possibilities[2] && !prev_sole_possibilities[0] && !prev_sole_possibilities[1] && !prev_sole_possibilities[3]; 
            prev_sole_possibilities[3] = prev_sole_possibilities[3] && !prev_sole_possibilities[0] && !prev_sole_possibilities[1] && !prev_sole_possibilities[2];

            for (int robot_type = 0; robot_type < Constants.NUM_ROBOT_TYPES; robot_type++)
            {
                if (prev_sole_possibilities[robot_type])
                {
                    continue;
                }
                int [] potential_robots = new int[Constants.NUM_ROBOT_TYPES];
                potential_robots[robot_type] = 1;

                int[] potential_resources = (int[])resources.Clone();
                potential_resources[0] -= (ore_robot_cost * potential_robots[0]);
                potential_resources[0] -= (clay_robot_cost *  potential_robots[1]);
                potential_resources[0] -= (obs_robot_ore_cost *  potential_robots[2]);
                potential_resources[0] -= (geode_robot_ore_cost *  potential_robots[3]);
                potential_resources[1] -= (obs_robot_clay_cost *  potential_robots[2]);
                potential_resources[2] -= (geode_robot_obs_cost *  potential_robots[3]);


                if (potential_resources[0] >= 0 && potential_resources[1] >= 0 && potential_resources[2] >= 0)
                            {
                    possibilities.Add(new Tuple<int[], int[]>(potential_resources, potential_robots));
                }
            }
            return possibilities;
        }
    }
}
