﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Versioning;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Xml;

namespace Advent
{
    class Year3Day8
    {
        static Dictionary<string, Tuple<string, string>> nodes = new Dictionary<string, Tuple<string, string>>();
        static string instructions = "";
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            instructions = reader.ReadLine();
            curr_line = reader.ReadLine();

            List<string> node_ends_with_a = new List<string>();
            List<string> node_ends_with_z = new List<string>();

            int node_num = 0;
            while ((curr_line = reader.ReadLine()) != null) 
            {
                List<string> line_info = ((curr_line.Replace("(", "").Replace("=", "").Replace(")", "").Replace(",", "").Trim().Split(null)).OfType<string>().ToList()).Where(x => x.Length > 0).ToList<string>();
                nodes.Add(line_info[0], new Tuple<string, string>(line_info[1],line_info[2]));
                if (line_info[0][2] == 'A')
                {
                    node_ends_with_a.Add(line_info[0]);
                }
                if (line_info[0][2] == 'Z')
                {
                    node_ends_with_z.Add(line_info[0]);
                }
                node_num++;
            }
            reader.Close();  

            Dictionary<Tuple<string, int>, Tuple<string, int, long>> z_maps = new Dictionary<Tuple<string, int>, Tuple<string, int, long>>();

            foreach (string curr_z_node in node_ends_with_z)
            {
                for (int curr_dir_num = 0; curr_dir_num < instructions.Length; curr_dir_num++)
                {
                    z_maps.Add(new Tuple<string, int> (curr_z_node, curr_dir_num), RuntToNextZ(curr_z_node, curr_dir_num));
                }
            }

            List<long> mults = new List<long>();
            Dictionary<string, Tuple<string, int, long>> a_maps = new Dictionary<string, Tuple<string, int, long>>();

            foreach (string curr_a_node in node_ends_with_a)
            {
                a_maps.Add(curr_a_node, RuntToNextZ(curr_a_node, 0));
                mults.Add(a_maps[curr_a_node].Item3);
            }
            Console.WriteLine(LCM(mults));
        }           
        static Tuple <string, int, long> RuntToNextZ(string in_node, int in_dir_num)
        {
            string curr_node = in_node;
            int dir_num = in_dir_num;
            long step_count = 0;
            bool done = false;
            while(!done)
            {  
                if (nodes[curr_node].Item1 == nodes[curr_node].Item2 && nodes[curr_node].Item1 == curr_node)
                {
                    return new Tuple<string, int, long>(in_node, -1, -1);
                }
                if (instructions[dir_num] == 'L')
                {
                    curr_node = nodes[curr_node].Item1;
                }
                else
                {
                    curr_node = nodes[curr_node].Item2;
                }
                dir_num = (dir_num+1)%(instructions.Length);
                step_count++;
                if (curr_node[2] == 'Z')
                {
                    done = true;
                }
            }
            return new Tuple<string, int, long>(curr_node, dir_num, step_count);
        } 
        static long LCM(List<long> ins)
        {
            long answer = ins[0];
            for (int i = 1; i < ins.Count; i++)
            {
                answer = (ins[i] * answer) / GCD(ins[i], answer);

            }
            return answer;
        }
        static long GCD(long x, long y)
        {
            if (y == 0)
                return x;
            return GCD(y, x % y);
        }
    }
}