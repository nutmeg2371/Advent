﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;

namespace Advent
{
    class Year3Day2
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            List<string> test_colors = new List<string>{"red", "green", "blue"};
            int[] test_counts = {12,13,14};
            int sum = 0;
            int power = 0;

            while ((curr_line = reader.ReadLine()) != null) 
            {
                string game_num = curr_line.Substring("Game ".Length, curr_line.IndexOf(":") - "Game ".Length);
                string[] grabs = (curr_line.Substring(curr_line.IndexOf(":") + 2, curr_line.Length - curr_line.IndexOf(":") - 2)).Split(";");
                int[] min_colors_count = {0, 0, 0};
                bool possible_this_game = true;
                foreach (string grab in grabs)
                {
                    string[] grab_colors_str = grab.Replace(",","").Split(" ");
                    for (int i = 0; i < grab_colors_str.Length; i++)
                    {
                        if (test_colors.Contains(grab_colors_str[i]))
                        {
                            min_colors_count[test_colors.IndexOf(grab_colors_str[i])] = Math.Max(int.Parse(grab_colors_str[i-1]),min_colors_count[test_colors.IndexOf(grab_colors_str[i])]);
                            if (int.Parse(grab_colors_str[i-1]) > test_counts[test_colors.IndexOf(grab_colors_str[i])])
                            {
                                possible_this_game = false;
                            }
                            
                        }
                    }
                }
                power += min_colors_count.Aggregate(1, (a, b) => a * b);
                if (possible_this_game)
                {
                    sum += int.Parse(game_num);
                }
            }
            Console.WriteLine(sum);
            Console.WriteLine(power);
            reader.Close();
        }
    }
}
