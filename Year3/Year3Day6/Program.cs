﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Versioning;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace Advent
{
    class Year3Day6
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            curr_line = reader.ReadLine();
            List<long> race_lengths = ((curr_line.Replace("Time","").Replace(":","").Trim().Split(null)).OfType<string>().ToList()).Where(x => x.Length > 0).ToList<string>().Select(x => long.Parse(x)).ToList<long>();
            curr_line = reader.ReadLine();
            List<long> race_distances = ((curr_line.Replace("Distance","").Replace(":","").Trim().Split(null)).OfType<string>().ToList()).Where(x => x.Length > 0).ToList<string>().Select(x => long.Parse(x)).ToList<long>();
            reader.Close();

            long race_length_part2 = long.Parse(string.Join("", race_lengths.ToList<long>().Select(x => x.ToString()).ToList<string>()));
            long race_distance_part2 = long.Parse(string.Join("", race_distances.ToList<long>().Select(x => x.ToString()).ToList<string>()));
            race_lengths.Add(race_length_part2);
            race_distances.Add(race_distance_part2);

            long ways_to_win = 1;

            for (int game_i = 0; game_i < race_lengths.Count; game_i++)
            {
                long ways_to_win_this_race = 0;
                for (int button_hold = 0; button_hold < race_lengths[game_i]; button_hold++)
                {
                    long travel_time = race_lengths[game_i] - button_hold;
                    long travel_speed = button_hold;
                    long travel_distance = travel_time * travel_speed;
                    if (travel_distance > race_distances[game_i])
                    {
                        ways_to_win_this_race++;
                    } 
                }
                if (game_i != race_lengths.Count - 1)
                {
                    ways_to_win *= ways_to_win_this_race;
                }
                if (game_i == race_lengths.Count - 1)
                {
                    Console.WriteLine(ways_to_win_this_race);
                }
            }   
            Console.WriteLine(ways_to_win);
        }
    }
}
