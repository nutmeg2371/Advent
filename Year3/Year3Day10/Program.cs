﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Intrinsics.Arm;
using System.Runtime.Versioning;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Xml;

namespace Advent
{
    class Year3Day10
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            List<List<char>> fieldmap = new List<List<char>>();
            
            int start_x = 0;
            int start_y = 0; 

            while ((curr_line = reader.ReadLine()) != null) 
            {
                List<char> curr_symbols = curr_line.ToCharArray().ToList<char>();
                curr_symbols.Insert(0,'.');
                curr_symbols.Add('.');
                fieldmap.Add(curr_symbols);   
                if (curr_symbols.Contains('S'))
                {
                    start_x = curr_symbols.IndexOf('S');
                    start_y = fieldmap.Count - 1;
                }             
            }
            reader.Close();     
            fieldmap.Add(Enumerable.Repeat('.',fieldmap[0].Count).ToList());
            fieldmap.Insert(0, Enumerable.Repeat('.',fieldmap[0].Count).ToList());
            start_y++;

            List<List<int>> distmap = new List<List<int>>();
            foreach (List<char> curr_map_line in fieldmap)
            {
                distmap.Add(Enumerable.Repeat(-1,curr_map_line.Count).ToList());
            }   
            
            distmap[start_y][start_x] = 0;

            bool change = true;
            while (change)
            {
                change = false;
                for (int x = 1; x < fieldmap[0].Count - 1; x++)
                {
                    for (int y = 1; y < fieldmap.Count - 1; y++)
                    {
                        if (distmap[y][x] != -1)
                        {
                            if (fieldmap[y-1][x] == '|' && (fieldmap[y][x] == 'L' || fieldmap[y][x] == 'J' || fieldmap[y][x] == '|' || fieldmap[y][x] == 'S') && (distmap[y-1][x] == -1 || distmap[y-1][x] > distmap[y][x] + 1))
                            {
                                distmap[y-1][x] = distmap[y][x] + 1;
                                change = true;
                            }
                            if (fieldmap[y+1][x] == '|' && (fieldmap[y][x] == 'F' || fieldmap[y][x] == '7' || fieldmap[y][x] == '|' || fieldmap[y][x] == 'S') && (distmap[y+1][x] == -1 || distmap[y+1][x] > distmap[y][x] + 1))
                            {
                                distmap[y+1][x] = distmap[y][x] + 1;
                                change = true;
                            }
                            if (fieldmap[y][x+1] == '-' && (fieldmap[y][x] == 'L' || fieldmap[y][x] == 'F' || fieldmap[y][x] == '-' || fieldmap[y][x] == 'S') && (distmap[y][x+1] == -1 || distmap[y][x+1] > distmap[y][x] + 1))
                            {
                                distmap[y][x+1] = distmap[y][x] + 1;
                                change = true;
                            }
                            if (fieldmap[y][x-1] == '-' && (fieldmap[y][x] == 'J' || fieldmap[y][x] == '7' || fieldmap[y][x] == '-' || fieldmap[y][x] == 'S') && (distmap[y][x-1] == -1 || distmap[y][x-1] > distmap[y][x] + 1))
                            {
                                distmap[y][x-1] = distmap[y][x] + 1;
                                change = true;
                            }
                            if (fieldmap[y][x+1] == '7' && (fieldmap[y][x] == 'L' || fieldmap[y][x] == 'F' || fieldmap[y][x] == '-' || fieldmap[y][x] == 'S') && (distmap[y][x+1] == -1 || distmap[y][x+1] > distmap[y][x] + 1))
                            {
                                distmap[y][x+1] = distmap[y][x] + 1;
                                change = true;
                            }
                            if (fieldmap[y][x+1] == '7' && (fieldmap[y][x] == 'L' || fieldmap[y][x] == 'F' || fieldmap[y][x] == '-' || fieldmap[y][x] == 'S') && (distmap[y+1][x+1] == -1 || distmap[y+1][x+1] > distmap[y][x] + 2))
                            {
                                distmap[y+1][x+1] = distmap[y][x] + 2;
                                change = true;
                            }
                            if (fieldmap[y-1][x] == '7' && (fieldmap[y][x] == 'L' || fieldmap[y][x] == 'J' || fieldmap[y][x] == '|' || fieldmap[y][x] == 'S') && (distmap[y-1][x] == -1 || distmap[y-1][x] > distmap[y][x] + 1))
                            {
                                distmap[y-1][x] = distmap[y][x] + 1;
                                change = true;
                            }
                            if (fieldmap[y-1][x] == '7' && (fieldmap[y][x] == 'L' || fieldmap[y][x] == 'J' || fieldmap[y][x] == '|' || fieldmap[y][x] == 'S') && (distmap[y-1][x-1] == -1 || distmap[y-1][x-1] > distmap[y][x] + 2))
                            {
                                distmap[y-1][x-1] = distmap[y][x] + 2;
                                change = true;
                            }
                            if (fieldmap[y][x-1] == 'F' && (fieldmap[y][x] == 'J' || fieldmap[y][x] == '7' || fieldmap[y][x] == '-' || fieldmap[y][x] == 'S') && (distmap[y][x-1] == -1 || distmap[y][x-1] > distmap[y][x] + 1))
                            {
                                distmap[y][x-1] = distmap[y][x] + 1;
                                change = true;
                            }
                            if (fieldmap[y][x-1] == 'F' && (fieldmap[y][x] == 'J' || fieldmap[y][x] == '7' || fieldmap[y][x] == '-' || fieldmap[y][x] == 'S') && (distmap[y+1][x-1] == -1 || distmap[y+1][x-1] > distmap[y][x] + 2))
                            {
                                distmap[y+1][x-1] = distmap[y][x] + 2;
                                change = true;
                            }
                            if (fieldmap[y-1][x] == 'F' && (fieldmap[y][x] == 'J' || fieldmap[y][x] == 'L' || fieldmap[y][x] == '|' || fieldmap[y][x] == 'S') && (distmap[y-1][x] == -1 || distmap[y-1][x] > distmap[y][x] + 1))
                            {
                                distmap[y-1][x] = distmap[y][x] + 1;
                                change = true;
                            }
                            if (fieldmap[y-1][x] == 'F' && (fieldmap[y][x] == 'J' || fieldmap[y][x] == 'L' || fieldmap[y][x] == '|' || fieldmap[y][x] == 'S') && (distmap[y-1][x+1] == -1 || distmap[y-1][x+1] > distmap[y][x] + 2))
                            {
                                distmap[y-1][x+1] = distmap[y][x] + 2;
                                change = true;
                            }
                            if (fieldmap[y][x+1] == 'J' && (fieldmap[y][x] == 'F' || fieldmap[y][x] == 'L' || fieldmap[y][x] == '-' || fieldmap[y][x] == 'S') && (distmap[y][x+1] == -1 || distmap[y][x+1] > distmap[y][x] + 1))
                            {
                                distmap[y][x+1] = distmap[y][x] + 1;
                                change = true;
                            }
                            if (fieldmap[y][x+1] == 'J' && (fieldmap[y][x] == 'F' || fieldmap[y][x] == 'L' || fieldmap[y][x] == '-' || fieldmap[y][x] == 'S') && (distmap[y-1][x+1] == -1 || distmap[y-1][x+1] > distmap[y][x] + 2))
                            {
                                distmap[y-1][x+1] = distmap[y][x] + 2;
                                change = true;
                            }
                            if (fieldmap[y+1][x] == 'J'&& (fieldmap[y][x] == 'F' || fieldmap[y][x] == '7' || fieldmap[y][x] == '|' || fieldmap[y][x] == 'S') && (distmap[y+1][x] == -1 || distmap[y+1][x] > distmap[y][x] + 1))
                            {
                                distmap[y+1][x] = distmap[y][x] + 1;
                                change = true;
                            }
                            if (fieldmap[y+1][x] == 'J' && (fieldmap[y][x] == 'F' || fieldmap[y][x] == '7' || fieldmap[y][x] == '|' || fieldmap[y][x] == 'S') && (distmap[y+1][x-1] == -1 || distmap[y+1][x-1] > distmap[y][x] + 2))
                            {
                                distmap[y+1][x-1] = distmap[y][x] + 2;
                                change = true;
                            }
                            if (fieldmap[y][x-1] == 'L' && (fieldmap[y][x] == 'J' || fieldmap[y][x] == '7' || fieldmap[y][x] == '-' || fieldmap[y][x] == 'S') && (distmap[y][x-1] == -1 || distmap[y][x-1] > distmap[y][x] + 1))
                            {
                                distmap[y][x-1] = distmap[y][x] + 1;
                                change = true;
                            }
                            if (fieldmap[y][x-1] == 'L' && (fieldmap[y][x] == 'J' || fieldmap[y][x] == '7' || fieldmap[y][x] == '-' || fieldmap[y][x] == 'S') && (distmap[y-1][x-1] == -1 || distmap[y-1][x-1] > distmap[y][x] + 2))
                            {
                                distmap[y-1][x-1] = distmap[y][x] + 2;
                                change = true;
                            }
                            if (fieldmap[y+1][x] == 'L' && (fieldmap[y][x] == 'F' || fieldmap[y][x] == '7' || fieldmap[y][x] == '|' || fieldmap[y][x] == 'S') && (distmap[y+1][x] == -1 || distmap[y+1][x] > distmap[y][x] + 1))
                            {
                                distmap[y+1][x] = distmap[y][x] + 1;
                                change = true;
                            }
                            if (fieldmap[y+1][x] == 'L' && (fieldmap[y][x] == 'F' || fieldmap[y][x] == '7' || fieldmap[y][x] == '|' || fieldmap[y][x] == 'S') && (distmap[y+1][x+1] == -1 || distmap[y+1][x+1] > distmap[y][x] + 2))
                            {
                                distmap[y+1][x+1] = distmap[y][x] + 2;
                                change = true;
                            }
                        }
                    }
                }  
            }     
            int max_dist = -1;
            for (int x = 0; x < fieldmap[0].Count; x++)
            {
                for (int y = 0; y < fieldmap.Count; y++)
                {
                    max_dist = Math.Max(max_dist, distmap[y][x]);
                }
            }
            Console.WriteLine(max_dist);  

            // delete all the junk pipes & replace S
            for (int y = 0; y < fieldmap.Count; y++)
            {
                for (int x = 0; x < fieldmap[0].Count; x++)
                {
            
                    if (distmap[y][x] == -1)
                    {
                        fieldmap[y][x] = '.';
                    }                    
                }
            }
            bool horiz_left = (fieldmap[start_y][start_x - 1] == 'F' || fieldmap[start_y][start_x - 1] == 'L' || fieldmap[start_y][start_x - 1] == '-');
            bool horiz_right = (fieldmap[start_y][start_x + 1] == 'J' || fieldmap[start_y][start_x + 1] == '7' || fieldmap[start_y][start_x + 1] == '-');
            bool vert_up = (fieldmap[start_y - 1][start_x] == 'F' || fieldmap[start_y - 1][start_x] == '7' || fieldmap[start_y - 1][start_x] == '|');
            bool vert_down = (fieldmap[start_y + 1][start_x] == 'J' || fieldmap[start_y + 1][start_x] == 'L' || fieldmap[start_y + 1][start_x] == '|');
            if (horiz_left && horiz_right)
            {
                fieldmap[start_y][start_x] = '-';
            }
            else if (horiz_left && vert_up)
            {
                fieldmap[start_y][start_x] = 'J';
            }
            else if (horiz_left && vert_down)
            {
                fieldmap[start_y][start_x] = '7';
            }
            else if (horiz_right && vert_up)
            {
                fieldmap[start_y][start_x] = 'L';
            }
            else if (horiz_right && vert_down)
            {
                fieldmap[start_y][start_x] = 'F';
            }
            else if (vert_up && vert_down)
            {
                fieldmap[start_y][start_x] = '|';
            }            

            // extend pipes
            List<List<char>> extendedfieldmap = new List<List<char>>();
            foreach(List<char> curr_map_line in fieldmap)
            {
                List<char> extended_map_line = new List<char>();
                foreach (char c in curr_map_line)
                {
                    switch (c)
                    {
                        case 'F':
                        {
                            extended_map_line.Add('.');
                            extended_map_line.Add('F');
                            extended_map_line.Add('-');
                            break;
                        }
                        case 'L':
                        {
                            extended_map_line.Add('.');
                            extended_map_line.Add('L');
                            extended_map_line.Add('-');
                            break;
                        }
                        case 'J':
                        {
                            extended_map_line.Add('-');
                            extended_map_line.Add('J');
                            extended_map_line.Add('.');
                            break;
                        }
                        case '7':
                        {
                            extended_map_line.Add('-');
                            extended_map_line.Add('7');
                            extended_map_line.Add('.');
                            break;
                        }
                        case '-':
                        {
                            extended_map_line.Add('-');
                            extended_map_line.Add('-');
                            extended_map_line.Add('-');
                            break;
                        }
                        case '|':
                        {
                            extended_map_line.Add('.');
                            extended_map_line.Add('|');
                            extended_map_line.Add('.');
                            break;
                        }
                        case '.':
                        {
                            extended_map_line.Add('.');
                            extended_map_line.Add('.');
                            extended_map_line.Add('.');
                            break;
                        }
                        case 'S':
                        {
                            extended_map_line.Add('S');
                            extended_map_line.Add('S');
                            extended_map_line.Add('S');
                            break;
                        }
                    }
                }
                extendedfieldmap.Add(new List<char>(extended_map_line));                
                extendedfieldmap.Add(new List<char>(extended_map_line));
                extendedfieldmap.Add(new List<char>(extended_map_line));
                for (int c_ind = 0; c_ind < extended_map_line.Count; c_ind++)
                {
                    switch (extended_map_line[c_ind])
                    {
                        case 'F':
                        {
                            extendedfieldmap[extendedfieldmap.Count-3][c_ind] = '.';
                            extendedfieldmap[extendedfieldmap.Count-1][c_ind] = '|';
                            break;
                        }
                        case 'L':
                        {
                            extendedfieldmap[extendedfieldmap.Count-3][c_ind] = '|';
                            extendedfieldmap[extendedfieldmap.Count-1][c_ind] = '.';
                            break;
                        }
                        case 'J':
                        {
                            extendedfieldmap[extendedfieldmap.Count-3][c_ind] = '|';
                            extendedfieldmap[extendedfieldmap.Count-1][c_ind] = '.';
                            break;
                        }
                        case '7':
                        {
                            extendedfieldmap[extendedfieldmap.Count-3][c_ind] = '.';
                            extendedfieldmap[extendedfieldmap.Count-1][c_ind] = '|';
                            break;
                        }
                        case '-':
                        {
                            extendedfieldmap[extendedfieldmap.Count-3][c_ind] = '.';
                            extendedfieldmap[extendedfieldmap.Count-1][c_ind] = '.';
                            break;
                        }
                        case '|':
                        {
                            extendedfieldmap[extendedfieldmap.Count-3][c_ind] = '|';
                            extendedfieldmap[extendedfieldmap.Count-1][c_ind] = '|';
                            break;
                        }
                    }
                }                
            }           
    
            extendedfieldmap = Flood(extendedfieldmap, new Tuple<int, int>(0, 0), '0');    

            // unextend
            List<List<char>> unextendedfieldmap = new List<List<char>>();

            for (int y = 1; y < extendedfieldmap.Count; y+=3)
            {
                List<char> unextended_line = new List<char>();
                for (int x = 1; x < extendedfieldmap[0].Count; x+=3)
                {
                    unextended_line.Add(extendedfieldmap[y][x]);
                }
                unextendedfieldmap.Add(unextended_line);
            }
            int included_tiles = 0;
            
            for (int y = 0; y < unextendedfieldmap.Count; y++)
            {
                for (int x = 0; x < unextendedfieldmap[0].Count; x++)
                {
                    if (unextendedfieldmap[y][x] == '.')
                    {
                        included_tiles++;
                    }                           
                }                  
            }           

            Console.WriteLine(included_tiles); 
        }

        public static List<List<char>> Flood (List<List<char>> fieldmap, Tuple<int, int> start_node, char flood_char)
        {
            List<Tuple<int, int>> process_queue = new List<Tuple<int, int>>();

            process_queue.Add(start_node);

            while (process_queue.Count > 0)
            {
                Tuple<int, int> curr_node = process_queue[0];
                process_queue.RemoveAt(0);

                if (fieldmap[curr_node.Item1][curr_node.Item2] == '.')
                {
                    fieldmap[curr_node.Item1][curr_node.Item2] = flood_char;
                    List<Tuple<int, int>> neighbors = new List<Tuple<int, int>>();                    
                    neighbors.Add(new Tuple<int, int>(curr_node.Item1 - 1, curr_node.Item2));
                    neighbors.Add(new Tuple<int, int>(curr_node.Item1 + 1, curr_node.Item2));
                    neighbors.Add(new Tuple<int, int>(curr_node.Item1, curr_node.Item2 - 1));
                    neighbors.Add(new Tuple<int, int>(curr_node.Item1, curr_node.Item2 + 1));
                    neighbors.Add(new Tuple<int, int>(curr_node.Item1 - 1, curr_node.Item2 - 1));
                    neighbors.Add(new Tuple<int, int>(curr_node.Item1 - 1, curr_node.Item2 + 1));
                    neighbors.Add(new Tuple<int, int>(curr_node.Item1 + 1, curr_node.Item2 - 1));
                    neighbors.Add(new Tuple<int, int>(curr_node.Item1 + 1, curr_node.Item2 + 1));
                    foreach (Tuple<int, int> neighbor in neighbors)
                    {
                        if (neighbor.Item1 < 0 || neighbor.Item1 >= fieldmap.Count || neighbor.Item2 < 0 || neighbor.Item2 >= fieldmap[0].Count)
                        {
                            continue;
                        }
                        if (fieldmap[neighbor.Item1][neighbor.Item2] == '.')
                        {
                            process_queue.Add(neighbor);
                        }
                    }
                }
            }
            return fieldmap;
        }
    }
}