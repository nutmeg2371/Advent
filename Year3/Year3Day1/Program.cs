﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;

namespace Advent
{
    class Year3Day1
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            int sum1 = 0;
            int sum2 = 0;
            string[] digit_strs = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
            HashSet<string> digit_set = new HashSet<string>();
            HashSet<string> rev_digit_set = new HashSet<string>();

            foreach (string dig_str in digit_strs)
            {
                digit_set.Add(dig_str);
                rev_digit_set.Add(RevString(dig_str));
            }
        
            while ((curr_line = reader.ReadLine()) != null) 
            {
                Tuple<int, string, int , int> tens_tuple = FindEndDigits(curr_line, digit_set);
                Tuple<int, string, int, int> ones_tuple = FindEndDigits(RevString(curr_line), rev_digit_set);
                sum1 += 10*tens_tuple.Item1;
                sum1 += ones_tuple.Item1;
                
                if (tens_tuple.Item3 < tens_tuple.Item4)
                {
                    sum2 += 10*tens_tuple.Item1;
                }
                else
                {
                    sum2 += 10*(digit_strs.ToList().IndexOf(tens_tuple.Item2) + 1);
                }
                if (ones_tuple.Item3 < ones_tuple.Item4)
                {
                    sum2 += ones_tuple.Item1;
                }
                else
                {
                    sum2 += (digit_strs.ToList().IndexOf(RevString(ones_tuple.Item2)) + 1);
                }                

            }
            Console.WriteLine(sum1);
            Console.WriteLine(sum2);
            reader.Close();
        }

        static Tuple<int, string, int, int> FindEndDigits(string curr_line, HashSet<string> digit_set)
        {
            int dig_num = -1;
            string dig_str = "";
            int num_ind = curr_line.Length;
            int str_ind = curr_line.Length;
            for (int i = 0; i < curr_line.Length; i++)
            {
                char c = curr_line[i];
                if (Char.IsDigit(c))
                {
                    dig_num = int.Parse(c.ToString());
                    num_ind = i;
                    break;
                }
                else if (dig_str == "")
                {
                    for (int len = 1; len <= 5; len++)
                    {
                        if (curr_line.Length - i >= len && digit_set.Contains(curr_line.Substring(i, len)))
                        {
                            str_ind = i;
                            dig_str = curr_line.Substring(i, len);
                            break;
                        }
                    }
                    if (dig_num > -1)
                    {
                        break;
                    }
                }
            }
            return new Tuple<int, string, int, int>(dig_num,dig_str, num_ind, str_ind);
        }
        static string RevString(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
