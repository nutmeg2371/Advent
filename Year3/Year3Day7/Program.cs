﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Versioning;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Xml;

namespace Advent
{
    class Year3Day7
    {

        public const int HIGH_CARD = 1;
        public const int SINGLE_PAIR = 2;
        public const int TWO_PAIRS = 3;
        public const int THREE_OF_A_KIND = 4;
        public const int FULL_HOUSE = 5;
        public const int FOUR_OF_A_KIND = 6;
        public const int FIVE_OF_A_KIND = 7;
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            List<string> hands = new List<string>();
            List<int> bids = new List<int>();

            while ((curr_line = reader.ReadLine()) != null) 
            {
                List<string> line_info = ((curr_line.Trim().Split(null)).OfType<string>().ToList()).Where(x => x.Length > 0).ToList<string>();
                hands.Add(line_info[0]);
                bids.Add(int.Parse(line_info[1]));
            }
            reader.Close();  

            for (int h_ind2 = 0; h_ind2 < hands.Count - 1; h_ind2++)
            {
                for (int h_ind1 = 0; h_ind1 < hands.Count - 1; h_ind1++)
                {
                    if (CompareHands(hands[h_ind1], hands[h_ind1 + 1], 1) == 1)
                    {
                        string temp_str = hands[h_ind1];
                        int temp_int = bids[h_ind1];
                        hands[h_ind1] = hands[h_ind1 + 1];
                        bids[h_ind1] = bids[h_ind1 + 1];
                        hands[h_ind1 + 1] = temp_str;
                        bids[h_ind1 + 1] = temp_int;
                    }
                }
            }

            long mult = 1;
            long winnings = 0;
            using (StreamWriter writer = new StreamWriter("output1.txt"))
            {
                for (int i =0; i < bids.Count; i++)
                {
                    writer.WriteLine(hands[i] + " " + bids[i]);
                    winnings += bids[i] * mult;
                    mult++;
                }
            }
            Console.WriteLine(winnings);

            for (int h_ind2 = 0; h_ind2 < hands.Count - 1; h_ind2++)
            {
                for (int h_ind1 = 0; h_ind1 < hands.Count - 1; h_ind1++)
                {
                    if (CompareHands(hands[h_ind1], hands[h_ind1 + 1], 2) == 1)
                    {
                        string temp_str = hands[h_ind1];
                        int temp_int = bids[h_ind1];
                        hands[h_ind1] = hands[h_ind1 + 1];
                        bids[h_ind1] = bids[h_ind1 + 1];
                        hands[h_ind1 + 1] = temp_str;
                        bids[h_ind1 + 1] = temp_int;
                    }
                }
            }

            mult = 1;
            winnings = 0;
            using (StreamWriter writer = new StreamWriter("output2.txt"))
            {
                for (int i =0; i < bids.Count; i++)
                {
                    writer.WriteLine(hands[i] + " " + bids[i]);
                    winnings += bids[i] * mult;
                    mult++;
                }
            }
            Console.WriteLine(winnings);
        }

        static int CompareHands (string  h1, string h2, int part)
        {
            int h1_type = 0;
            int h2_type = 0;
            if (part == 1)
            {
                h1_type = GetHandTypeP1(h1);
                h2_type = GetHandTypeP1(h2);
            }
            else
            {
                h1_type = GetHandTypeP2(h1);
                h2_type = GetHandTypeP2(h2);
            }
            if (h1_type > h2_type)
            {
                return 1;
            }
            if (h1_type < h2_type)
            {
                return -1;
            }
            for (int i = 0; i < h1.Length; i++)
            {
                int card_compare = 0;
                if (part == 1)
                {
                    card_compare = CompareCardsP1(h1[i], h2[i]);
                }
                else
                {
                    card_compare = CompareCardsP2(h1[i], h2[i]);
                }
                if (card_compare != 0)
                {
                    return card_compare;
                }
            }
            return 0;
        }

        static int CompareCardsP1 (char c1, char c2)
        {
            string card_order = "AKQJT98765432";
            if (card_order.IndexOf(c1) < card_order.IndexOf(c2))
            {
                return 1;
            }
            if (card_order.IndexOf(c1) > card_order.IndexOf(c2))
            {
                return -1;
            }
            return 0;
        }

        static int GetHandTypeP1 (string h1)
        {
            List<char> chars = new List<char>();
            List<int> counts = new List<int>();
            foreach (char c in h1)
            {
                if (chars.Contains(c))
                {
                    counts[chars.IndexOf(c)]++;
                }
                else
                {
                    chars.Add(c);
                    counts.Add(1);
                }
            }
            if (chars.Count == 1)
            {
                return FIVE_OF_A_KIND;
            }
            if (chars.Count == 2)
            {
                if (counts.Contains(4))
                {
                    return FOUR_OF_A_KIND;
                }
                else
                {
                    return FULL_HOUSE;
                }
            }
            if (chars.Count == 3)
            {
                if (counts.Contains(3))
                {
                    return THREE_OF_A_KIND;
                }
                else
                {
                    return TWO_PAIRS;
                }
            }
            if (chars.Count == 4)
            {
                return SINGLE_PAIR;
            }
            return HIGH_CARD;
        }
        

        static int CompareCardsP2 (char c1, char c2)
        {
            string card_order = "AKQT98765432J";
            if (card_order.IndexOf(c1) < card_order.IndexOf(c2))
            {
                return 1;
            }
            if (card_order.IndexOf(c1) > card_order.IndexOf(c2))
            {
                return -1;
            }
            return 0;
        }

        static int GetHandTypeP2 (string h1)
        {
            List<char> chars = new List<char>();
            List<int> counts = new List<int>();
            foreach (char c in h1)
            {
                if (chars.Contains(c))
                {
                    counts[chars.IndexOf(c)]++;
                }
                else
                {
                    chars.Add(c);
                    counts.Add(1);
                }
            }
            if (chars.Count == 1)
            {
                return FIVE_OF_A_KIND;
            }
            if (chars.Count == 2)
            {
                if (counts.Contains(4))
                {
                    if (chars.Contains('J'))
                    {
                        return FIVE_OF_A_KIND;
                    }
                    return FOUR_OF_A_KIND;
                }
                else
                {
                    if (chars.Contains('J'))
                    {
                        return FIVE_OF_A_KIND;
                    }
                    return FULL_HOUSE;
                }
            }
            if (chars.Count == 3)
            {
                if (counts.Contains(3))
                {
                    if (chars.Contains('J'))
                    {
                        return FOUR_OF_A_KIND;
                    }
                    return THREE_OF_A_KIND;
                }
                else
                {
                    if (chars.Contains('J') && counts[chars.IndexOf('J')] == 1)
                    {
                        return FULL_HOUSE;
                    }
                    if (chars.Contains('J') && counts[chars.IndexOf('J')] == 2)
                    {
                        return FOUR_OF_A_KIND;
                    }                    
                    return TWO_PAIRS;
                }
            }
            if (chars.Count == 4)
            {
                if (chars.Contains('J'))
                {
                    return THREE_OF_A_KIND;
                }
                return SINGLE_PAIR;
            }
            if (chars.Contains('J'))
            {
                return SINGLE_PAIR;
            }
            return HIGH_CARD;
        }
    }
}
