﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace Advent
{
    class Year3Day3
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            List<string> lines = new List<string>();
            int sum = 0;
            int gear_sum = 0;
            Dictionary<Tuple<int, int>,List<int>> gear_map = new Dictionary<Tuple<int, int>, List<int>>();

            while ((curr_line = reader.ReadLine()) != null) 
            {
                lines.Add("." + curr_line + ".");
            }
            reader.Close();
            string dot_str = new string('.', lines[0].Length);

            lines.Insert(0, dot_str);
            lines.Add(dot_str);

            for (int line_ind = 1; line_ind < lines.Count - 1; line_ind++)
            {
                string this_line = lines[line_ind];
                int start_ind = int.MaxValue;
                int end_ind = -1;
                for (int i = 0; i < this_line.Length; i++)
                {
                    char c = this_line[i];
                    if(Char.IsDigit(c))
                    {
                        if (start_ind == int.MaxValue)
                        {
                            start_ind = i;
                        } 
                        end_ind = i;
                    }
                    else
                    {
                        if (end_ind != -1)
                        {
                            bool touching = false;
                            bool touching_gear = false;
                            int this_num = int.Parse(this_line.Substring(start_ind, end_ind - start_ind + 1));
                            List<Tuple<int, int>> comp_chars = new List<Tuple<int, int>>();
                            List<Tuple<int, int>> gears_this_num = new List<Tuple<int, int>>();

                            comp_chars.Add(new Tuple<int, int>(line_ind - 1, start_ind - 1));
                            comp_chars.Add(new Tuple<int, int>(line_ind, start_ind - 1));
                            comp_chars.Add(new Tuple<int, int>(line_ind + 1, start_ind - 1));
                            comp_chars.Add(new Tuple<int, int>(line_ind - 1, end_ind + 1));
                            comp_chars.Add(new Tuple<int, int>(line_ind, end_ind + 1));
                            comp_chars.Add(new Tuple<int, int>(line_ind + 1, end_ind + 1));
                            
                            for (int check_ind = start_ind; check_ind <= end_ind; check_ind++)
                            {
                                comp_chars.Add(new Tuple<int, int>(line_ind - 1, check_ind));
                                comp_chars.Add(new Tuple<int, int>(line_ind + 1, check_ind));
                            }
                            foreach (Tuple<int, int> comp_char_ind in comp_chars)
                            {
                                char comp_char = lines[comp_char_ind.Item1][comp_char_ind.Item2];
                                if (!Char.IsDigit(comp_char) && comp_char != '.')
                                {
                                    touching = true;
                                }
                                if (comp_char == '*')
                                {
                                    touching_gear = true;
                                    gears_this_num.Add(comp_char_ind);
                                }
                            }
                            if (touching)
                            {
                                sum += this_num;
                            }
                            if (touching_gear)
                            {
                                foreach (Tuple <int, int> curr_gear in gears_this_num)
                                {
                                    if (!gear_map.ContainsKey(curr_gear))
                                    {
                                        gear_map[curr_gear] = new List<int>();
                                    }
                                    gear_map[curr_gear].Add(this_num);
                                }
                            }
                            start_ind = int.MaxValue;
                            end_ind = -1;
                        }
                    }
                }              
            }
            foreach (KeyValuePair<Tuple<int, int>, List<int>> gear_map_entry in gear_map)
            {
                if (gear_map_entry.Value.Count == 2)
                {
                    gear_sum += (gear_map_entry.Value[0] * gear_map_entry.Value[1]);
                }
            }
            Console.WriteLine(sum);
            Console.WriteLine(gear_sum);
        }
    }
}
