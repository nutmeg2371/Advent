﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Intrinsics.Arm;
using System.Runtime.Versioning;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Xml;

namespace Advent
{
    class Year3Day11
    {
        public const int P1_MULT = 2;
        public const int P2_MULT = 1000000;

        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            HashSet<int> galaxy_xs = new HashSet<int>();
            HashSet<int> non_galaxy_ys = new HashSet<int>();

            int y_count = 0;
            int x_count = 0;
            int galaxy_num = 1;
            Dictionary<int, Tuple<int, int>> coords = new Dictionary<int, Tuple<int, int>>();
            while ((curr_line = reader.ReadLine()) != null) 
            {
                List<string> curr_map_line = curr_line.ToCharArray().Select(x => x.ToString()).ToList<string>();
                int curr_x = 0;

                if (!curr_map_line.Contains("#"))
                {
                   non_galaxy_ys.Add(y_count); 
                }

                for (int i = 0; i < curr_map_line.Count; i++)
                {
                    if (curr_map_line[i] == "#")
                    {
                        galaxy_xs.Add(i);
                        coords.Add(galaxy_num++, new Tuple<int, int>(y_count, curr_x));
                    }
                    curr_x++;
                    x_count = curr_x;
                }
                y_count++;
            }
            reader.Close();  
            
            List<int> non_galaxy_xs = new List<int>();
            for (int i = 0; i < x_count; i++)
            {
                if (!galaxy_xs.Contains(i))
                {
                    non_galaxy_xs.Add(i);
                }
            }        

            int p1_dist = 0; 
            long p2_dist = 0; 
            for (int i = 1; i < galaxy_num; i++)
            {
                Tuple<int, int> gal1_coords = coords[i];
                for (int j = i + 1; j < galaxy_num; j++)
                {
                    Tuple<int, int> gal2_coords = coords[j];
                    int y_distance = Math.Abs(gal1_coords.Item1 - gal2_coords.Item1);
                    int y_hits = 0;
                    int x_hits = 0;
                    foreach (int y in non_galaxy_ys)
                    {
                        if (y >= Math.Min(gal1_coords.Item1, gal2_coords.Item1) && y <= Math.Max(gal1_coords.Item1, gal2_coords.Item1))
                        {
                            y_hits++;
                        }
                    }
                    int x_distance = Math.Abs(gal1_coords.Item2 - gal2_coords.Item2);
                    foreach (int x in non_galaxy_xs)
                    {
                        if (x >= Math.Min(gal1_coords.Item2, gal2_coords.Item2) && x <= Math.Max(gal1_coords.Item2, gal2_coords.Item2))
                        {
                            x_hits++;
                        }
                    }
                    p1_dist += (y_distance + y_hits * (P1_MULT - 1) + x_distance + x_hits * (P1_MULT - 1));
                    p2_dist += (y_distance + y_hits * (P2_MULT - 1) + x_distance + x_hits * (P2_MULT - 1));
                }
            }
            Console.WriteLine(p1_dist);
            Console.WriteLine(p2_dist);
        }            
    }
}