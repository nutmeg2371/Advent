﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Versioning;
using System.Threading;

namespace Advent
{
    class Year3Day5
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            Dictionary<Tuple<string, string>, List<Tuple<long, long, long>>> xfer_maps = new Dictionary<Tuple<string, string>, List<Tuple<long, long, long>>>();

            curr_line = reader.ReadLine();
            List<long> seeds = ((curr_line.Replace("seeds: ","").Trim().Split(null)).OfType<string>().ToList()).Where(x => x.Length > 0).ToList<string>().Select(x => long.Parse(x)).ToList<long>();

            List<long> seeds_part_2 = new List<long>();
            for (int i = 0; i < seeds.Count; i+=2)
            {
                for (int count = 0; count < seeds[i+1]; count++)
                {
                    seeds_part_2.Add(seeds[i] + count);
                }
            }

            while (curr_line != null) 
            {
                if (curr_line.Contains("-to-"))
                {
                    string curr_source = curr_line.Substring(0, curr_line.IndexOf("-to-")).Trim();
                    string curr_target = curr_line.Replace(curr_source, "").Replace("-to-","").Replace("map:","").Trim();
                    Tuple<string, string> curr_mapping = new Tuple<string, string> (curr_source, curr_target);
                    xfer_maps.Add(curr_mapping, new List<Tuple<long, long, long>>());
                    curr_line = reader.ReadLine();
                    
                    while (curr_line != null && curr_line != "") 
                    {
                        List<long> next_range = ((curr_line.Trim().Split(null)).OfType<string>().ToList()).Where(x => x.Length > 0).ToList<string>().Select(x => long.Parse(x)).ToList<long>();
                        xfer_maps[curr_mapping].Add(new Tuple<long, long, long> (next_range[1], next_range[1] + next_range[2] - 1, next_range[0] - next_range[1]));
                        curr_line = reader.ReadLine();
                    }
                }
                curr_line = reader.ReadLine();
            }
            reader.Close();

            Stack<KeyValuePair<string, List<long>>> curr_resource_process = new Stack<KeyValuePair<string, List<long>>>();
            curr_resource_process.Push(new KeyValuePair<string, List<long>>("seed", seeds_part_2));
            curr_resource_process.Push(new KeyValuePair<string, List<long>>("seed", seeds));

            while (curr_resource_process.Count > 0)
            {
                KeyValuePair<string, List<long>> resource_list_to_process = curr_resource_process.Pop();

                if (resource_list_to_process.Key == "location")
                {
                    Console.WriteLine(resource_list_to_process.Value.Min());
                    continue;
                }

                foreach (KeyValuePair<Tuple<string, string>, List<Tuple<long, long, long>>> curr_mapping in xfer_maps)
                {
                    if (curr_mapping.Key.Item1 == resource_list_to_process.Key)
                    {
                        List<long> xfered_resoures = new List<long>();
                        foreach (long curr_source_num in resource_list_to_process.Value)
                        {
                            bool added = false;
                            foreach (Tuple<long, long, long> curr_numerical_mapping in curr_mapping.Value)
                            {
                                if (curr_source_num >= curr_numerical_mapping.Item1 && curr_source_num <= curr_numerical_mapping.Item2)
                                {
                                    xfered_resoures.Add(curr_source_num + curr_numerical_mapping.Item3);
                                    added = true;
                                    break;
                                }
                            }
                            if (!added)
                            {
                                xfered_resoures.Add(curr_source_num);
                            }
                        }
                        curr_resource_process.Push(new KeyValuePair<string, List<long>>(curr_mapping.Key.Item2, xfered_resoures));
                    }
                } 
            }
        }
    }
}
