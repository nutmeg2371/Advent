﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace Advent
{
    class Year3Day4
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            int total = 0;
            Dictionary<int, int> card_winners = new Dictionary<int, int>();
            Dictionary<int, int> card_copies = new Dictionary<int, int>();

            List<string> games = new List<string>();

            while ((curr_line = reader.ReadLine()) != null) 
            {
                games.Add(curr_line);
            }
            reader.Close();
            foreach (string curr_game in games)
            {
                int card_total = 0;
                int card_factor = 1;
                int card_num = int.Parse(curr_game.Substring("Game ".Length, curr_game.IndexOf(":") - "Game ".Length));
                string[] nums = (curr_game.Substring(curr_game.IndexOf(":") + 2, curr_game.Length - curr_game.IndexOf(":") - 2)).Split("|");
                List<string> winning_nums = ((nums[0].Trim().Split(null)).OfType<string>().ToList()).Where(x => x.Length > 0).ToList<string>();
                List<string> my_nums = ((nums[1].Trim().Split(null)).OfType<string>().ToList()).Where(x => x.Length > 0).ToList<string>();
                
                card_copies.Add(card_num, 1);
                            card_winners.Add(card_num, 0);
                foreach (string mynum in my_nums)
                {
                    if (winning_nums.Contains(mynum))
                    {
                        card_total = card_factor;
                        card_factor *= 2;
                        card_winners[card_num]++;
                    }
                }
                total += card_total;
            }
            Console.WriteLine(total);

            int num_cards = 0;

            for (int game_num = 1; game_num <= games.Count; game_num++)
            {
                num_cards += card_copies[game_num];
                for (int i = 1; i <= card_winners[game_num]; i++)
                {
                    card_copies[game_num + i] += card_copies[game_num];
                }
            }
            Console.WriteLine(num_cards);
        }
    }
}
