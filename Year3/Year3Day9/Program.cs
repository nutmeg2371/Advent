﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Intrinsics.Arm;
using System.Runtime.Versioning;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Xml;

namespace Advent
{
    class Year3Day9
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            int totalp1 = 0;
            int totalp2 = 0;

            while ((curr_line = reader.ReadLine()) != null) 
            {
                List<int> curr_history = ((curr_line.Trim().Split(null)).OfType<string>().ToList()).Where(x => x.Length > 0).ToList<string>().Select(x => int.Parse(x)).ToList<int>();
                List<List<int>> sequences = new List<List<int>>();
                sequences.Add(curr_history);

                bool non_zero = true;
                while (non_zero)
                {
                    non_zero = false;
                    List<int> next_sequence = new List<int>();
                    List<int> curr_sequence = sequences[sequences.Count-1];

                    for (int i = 0; i < curr_sequence.Count - 1; i++)
                    {
                        next_sequence.Add(curr_sequence[i+1] - curr_sequence[i]);
                        if (curr_sequence[i+1] - curr_sequence[i] != 0)
                        {
                            non_zero = true;
                        }
                    }    
                    sequences.Add(next_sequence);                
                }

                List<List<int>> sequencesp1 = new List<List<int>>();
                List<List<int>> sequencesp2 = new List<List<int>>();
                foreach (List<int> curr_seq in sequences)
                {
                    sequencesp1.Add(new List<int>(curr_seq));
                    sequencesp2.Add(new List<int>(curr_seq));
                }
                sequencesp1[sequencesp1.Count - 1].Add(0);
                sequencesp2[sequencesp2.Count - 1].Insert(0, 0);

                for (int j = sequencesp1.Count - 2; j >= 0; j--)
                {
                    sequencesp1[j].Add(sequencesp1[j][sequencesp1[j].Count-1] + sequencesp1[j+1][sequencesp1[j+1].Count - 1]);
                    sequencesp2[j].Insert(0, sequencesp2[j][0] - sequencesp2[j+1][0]);
                }
                totalp1 += sequencesp1[0][sequencesp1[0].Count - 1];
                totalp2 += sequencesp2[0][0];
            }
            reader.Close(); 
            Console.WriteLine(totalp1); 
            Console.WriteLine(totalp2); 
        }           
        
    }
}