﻿using System;
using System.IO;
using System.Collections.Generic;

static class Constants
{
    public const int BOARD_SIZE = 1001;
}

namespace Advent
{
    class Year1Day5
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            int[,,] curr_board = new int[Constants.BOARD_SIZE,Constants.BOARD_SIZE,2];
            int num_danger_points_part1 = 0;
            int num_danger_points_part2 = 0;
            while ((curr_line = reader.ReadLine()) != null) 
            {
                string[] curr_endpoints = curr_line.Split(" -> ");
                string[] curr_start_coords  = curr_endpoints[0].Split(",");
                string[] curr_end_coords  = curr_endpoints[1].Split(",");
                int start_x = int.Parse(curr_start_coords[0]);
                int start_y = int.Parse(curr_start_coords[1]);
                int end_x = int.Parse(curr_end_coords[0]);
                int end_y = int.Parse(curr_end_coords[1]);

                int delta_x = -1;
                if (start_x < end_x)
                {
                    delta_x = 1;
                }
                else if (start_x == end_x)
                {
                    delta_x = 0;
                }
                int delta_y = -1;
                if (start_y < end_y)
                {
                    delta_y = 1;
                }
                else if (start_y == end_y)
                {
                    delta_y = 0;
                }

                int curr_x = start_x;
                int curr_y = start_y;
                while (curr_x != end_x + delta_x || curr_y != end_y + delta_y)
                {
                    if (delta_x == 0 || delta_y == 0)
                    {
                        curr_board[curr_x, curr_y,0]++;
                        if (curr_board[curr_x, curr_y,0] == 2)
                        {
                            num_danger_points_part1++;
                        }
                    }
                    curr_board[curr_x, curr_y,1]++;
                    if (curr_board[curr_x, curr_y,1] == 2)
                    {
                        num_danger_points_part2++;
                    }
                    curr_x+=delta_x;
                    curr_y+=delta_y;
                }
            }
            reader.Close();
            Console.WriteLine (num_danger_points_part1);
            Console.WriteLine (num_danger_points_part2);
        }
    }
}