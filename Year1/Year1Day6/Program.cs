﻿using System;
using System.IO;
using System.Collections.Generic;

static class Constants
{
    public const int NUM_TIMER_VALS = 9;
    public const int FIRST_DAY = 80;
    public const int SECOND_DAY = 256;
}

namespace Advent
{
    class Year1Day6
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = reader.ReadLine();
            string[] curr_day_strs = curr_line.Split(",");
            Int64 [] count_of_ints = new Int64[Constants.NUM_TIMER_VALS];
            Int64 total_fish = curr_day_strs.GetLength(0);

            foreach (string curr_fish in curr_day_strs)
            {
                count_of_ints[int.Parse(curr_fish)]++;
            }
            for (int day_num = 1; day_num <= Constants.SECOND_DAY; day_num++) 
            {
                Int64 new_babies = count_of_ints[0];
                count_of_ints[0] = 0;

                for (int curr_count = 1; curr_count <= 8; curr_count++)
                {
                    count_of_ints[curr_count-1] = count_of_ints[curr_count];
                    count_of_ints[curr_count] = 0;
                }
                count_of_ints[6] += new_babies;
                count_of_ints[8] += new_babies;
                total_fish += new_babies;
                if (day_num == Constants.FIRST_DAY)
                {
                    Console.WriteLine (total_fish);
                }
            }
            reader.Close();
            Console.WriteLine (total_fish);
        }
    }
}