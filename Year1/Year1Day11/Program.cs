﻿using System;
using System.IO;
using System.Collections.Generic;



static class Constants
{
  public const int GRID_SIZE = 10;
  public const int NUM_STEPS = 100;
}

namespace Advent
{
    class Year1Day11
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            int [,,] energy_levels = new int[2, Constants.GRID_SIZE+2, Constants.GRID_SIZE+2];

            int line_num = 1;
            while ((curr_line = reader.ReadLine()) != null) 
            {
                int[] line_array = Array.ConvertAll(curr_line.ToCharArray(), x => int.Parse(x.ToString()));
                for (int curr_col = 1; curr_col <= Constants.GRID_SIZE; curr_col++) 
                {
                    energy_levels[0,line_num,curr_col] = line_array[curr_col-1];
                }
                line_num++;
            }
            reader.Close();

            int num_flashes = 0;
            int step_num = 1;
            while(true)
            {
                for (int curr_y = 1; curr_y <= Constants.GRID_SIZE; curr_y++) 
                {
                    for (int curr_x = 1; curr_x <= Constants.GRID_SIZE; curr_x++) 
                    {
                        energy_levels[0, curr_y, curr_x]++;
                    }
                }
                bool change = true;
                while (change)
                {
                    change = false;
                    for (int curr_y = 1; curr_y <= Constants.GRID_SIZE; curr_y++) 
                    {
                        for (int curr_x = 1; curr_x <= Constants.GRID_SIZE; curr_x++) 
                        {
                            if (energy_levels[0, curr_y, curr_x] > 9 && energy_levels[1, curr_y, curr_x] != 1)
                            {
                                num_flashes++;
                                energy_levels[1,curr_y, curr_x] = 1;
                                energy_levels[0,curr_y, curr_x-1]++;
                                energy_levels[0,curr_y, curr_x+1]++;
                                energy_levels[0,curr_y-1, curr_x]++;
                                energy_levels[0,curr_y+1, curr_x]++;
                                energy_levels[0,curr_y-1, curr_x-1]++;
                                energy_levels[0,curr_y+1, curr_x-1]++;
                                energy_levels[0,curr_y-1, curr_x+1]++;
                                energy_levels[0,curr_y+1, curr_x+1]++;
                                change = true;

                            }
                            
                        }
                    }
                }
                int count_flashes = 0;
                for (int curr_y = 1; curr_y <= Constants.GRID_SIZE; curr_y++) 
                {
                    for (int curr_x = 1; curr_x <= Constants.GRID_SIZE; curr_x++) 
                    {
                        if (energy_levels[1,curr_y, curr_x] == 1)
                        {
                            energy_levels[0,curr_y, curr_x] = 0;
                            energy_levels[1,curr_y, curr_x] = 0;
                            count_flashes++;
                        }
                    }
                }
                if (step_num == Constants.NUM_STEPS)
                {
                    Console.WriteLine (num_flashes);
                }
                if (count_flashes == Constants.GRID_SIZE * Constants.GRID_SIZE)
                {
                    Console.WriteLine(step_num);
                    break;
                }
                step_num++;
            }
        }
    }
}