﻿using System;
using System.IO;
using System.Collections.Generic;

static class Constants
{
  public const int GRID_SIZE = 1311;
  public const int NUM_CHARS = 8;
}

namespace Year1Day13
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            
            int[,] paper = new int[Constants.GRID_SIZE,Constants.GRID_SIZE];

            while ((curr_line = reader.ReadLine()) != null && curr_line != "") 
            {
                string[] coords_this_line = curr_line.Split(",");
                paper[int.Parse(coords_this_line[0]),int.Parse(coords_this_line[1])] = 1;
            }

            int num_spots = 0;
            int curr_grid_size_x = Constants.GRID_SIZE;
            int curr_grid_size_y = Constants.GRID_SIZE;
            bool first_fold = true;
            while ((curr_line = reader.ReadLine()) != null) 
            {
                string[] folds_this_line = curr_line.Split("=");
                int fold_num = int.Parse(folds_this_line[1]);
                if (folds_this_line[0].Contains("x"))
                {
                    curr_grid_size_x = fold_num;
                    for (int delta = 1; delta <= fold_num; delta++)
                    {
                        for (int curr_y = 0; curr_y < Constants.GRID_SIZE; curr_y++)
                        {
                            paper[fold_num-delta,curr_y] += paper[fold_num+delta,curr_y];
                            if (paper[fold_num-delta,curr_y] > 0)
                            {
                                num_spots++;
                            }
                        }
                    }
                }
                else
                {
                    curr_grid_size_y = fold_num;
                    for (int delta = 1; delta <= fold_num; delta++)
                    {
                        for (int curr_x = 0; curr_x < Constants.GRID_SIZE; curr_x++)
                        {
                            paper[curr_x,fold_num-delta] += paper[curr_x,fold_num+delta];
                            if (paper[curr_x,fold_num-delta] > 0)
                            {
                                num_spots++;
                            }
                        }
                    }                    
                }
                if (first_fold)
                {
                    Console.WriteLine(num_spots);
                    first_fold = false;
                }
            }
            reader.Close();

            int char_num = 0;
            for (int curr_y = 0; curr_y < curr_grid_size_y; curr_y++)
            {
                for (int curr_x = 0; curr_x < curr_grid_size_x; curr_x++)
                {
                    if (paper[curr_x,curr_y] > 0)
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(".");
                    }
                    if (++char_num % (curr_grid_size_x/Constants.NUM_CHARS) == 0)
                    {
                        Console.Write("   ");
                    }
                }                
                Console.WriteLine();
            }
        }
    }
}
