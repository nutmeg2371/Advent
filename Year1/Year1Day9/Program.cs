﻿using System;
using System.IO;
using System.Collections.Generic;


static class Constants
{
    public const int MAX_ROWS = 100;
    public const int MAX_COLS = 100;
}

namespace Advent
{
    class Year1Day9
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            int[,,] floor_map = new int [Constants.MAX_ROWS,Constants.MAX_COLS,2];

            int row_num = 0;
            while ((curr_line = reader.ReadLine()) != null) 
            {                
                int col_num = 0;
                foreach (char curr_char in curr_line)
                {
                    floor_map[row_num, col_num++,0] = int.Parse(curr_char.ToString());
                }
                row_num++;
            }

            int total_risk = 0;
            for (int curr_row = 0; curr_row < Constants.MAX_ROWS; curr_row++)
            {
                for (int curr_col = 0; curr_col < Constants.MAX_COLS; curr_col++)
                {
                    bool low_point_top = curr_row < 1 || (floor_map[curr_row,curr_col,0] < floor_map[curr_row - 1,curr_col,0]);
                    bool low_point_bottom = curr_row > Constants.MAX_ROWS - 2 || (floor_map[curr_row,curr_col,0] < floor_map[curr_row + 1,curr_col,0]);
                    bool low_point_left = curr_col < 1 || (floor_map[curr_row,curr_col,0] < floor_map[curr_row,curr_col - 1,0]);
                    bool low_point_right = curr_col > Constants.MAX_COLS - 2 || (floor_map[curr_row,curr_col,0] < floor_map[curr_row,curr_col + 1,0]);
                    if (low_point_top && low_point_bottom && low_point_left && low_point_right)
                    {
                        total_risk += (floor_map[curr_row,curr_col,0] + 1);
                        floor_map[curr_row,curr_col,1] = 1;
                    }
                }  
            }
            Console.WriteLine(total_risk);
            reader.Close();

            // for each low point
            Stack<Tuple<int,int>> points_to_process = new Stack<Tuple<int,int>>();
            List<int> basin_sizes = new List<int>();
            for (int curr_row = 0; curr_row < Constants.MAX_ROWS; curr_row++)
            {
                for (int curr_col = 0; curr_col < Constants.MAX_COLS; curr_col++)
                {
                    if (floor_map[curr_row,curr_col,1] != 1)
                    {
                        continue;
                    }
                    int curr_basin_size = 1;
                    floor_map[curr_row,curr_col,1] = 2;
                    points_to_process.Push(new Tuple<int,int>(curr_row,curr_col));
                    while (points_to_process.Count > 0)
                    {
                        Tuple<int,int> curr_point = points_to_process.Pop();
                        //up
                        if (curr_point.Item1 > 0 && floor_map[curr_point.Item1-1,curr_point.Item2,0] > floor_map[curr_point.Item1,curr_point.Item2,0] && floor_map[curr_point.Item1-1,curr_point.Item2,0] != 9 && floor_map[curr_point.Item1-1,curr_point.Item2,1] != 2)
                        {
                            points_to_process.Push(new Tuple<int,int>(curr_point.Item1-1,curr_point.Item2));
                            floor_map[curr_point.Item1-1,curr_point.Item2,1] = 2;
                            curr_basin_size++;
                        }
                        // down
                        if (curr_point.Item1 < Constants.MAX_ROWS - 1 && floor_map[curr_point.Item1+1,curr_point.Item2,0] > floor_map[curr_point.Item1,curr_point.Item2,0] && floor_map[curr_point.Item1+1,curr_point.Item2,0] != 9 && floor_map[curr_point.Item1+1,curr_point.Item2,1] != 2)
                        {
                            points_to_process.Push(new Tuple<int,int>(curr_point.Item1+1,curr_point.Item2));
                            floor_map[curr_point.Item1+1,curr_point.Item2,1] = 2;
                            curr_basin_size++;
                        }
                        // left
                        if (curr_point.Item2 > 0 && floor_map[curr_point.Item1,curr_point.Item2-1,0] > floor_map[curr_point.Item1,curr_point.Item2,0] && floor_map[curr_point.Item1,curr_point.Item2-1,0] != 9 && floor_map[curr_point.Item1,curr_point.Item2-1,1] != 2)
                        {
                            points_to_process.Push(new Tuple<int,int>(curr_point.Item1,curr_point.Item2-1));
                            floor_map[curr_point.Item1,curr_point.Item2-1,1] = 2;
                            curr_basin_size++;
                        }
                        // right
                        if (curr_point.Item2 < Constants.MAX_COLS - 1 && floor_map[curr_point.Item1,curr_point.Item2+1,0] > floor_map[curr_point.Item1,curr_point.Item2,0] && floor_map[curr_point.Item1,curr_point.Item2+1,0] != 9 && floor_map[curr_point.Item1,curr_point.Item2+1,1] != 2)
                        {
                            points_to_process.Push(new Tuple<int,int>(curr_point.Item1,curr_point.Item2+1));
                            floor_map[curr_point.Item1,curr_point.Item2+1,1] = 2;
                            curr_basin_size++;
                        }
                    }
                    if (curr_basin_size > 0)
                    {
                        basin_sizes.Add(curr_basin_size);
                    }
                }
            }  
            basin_sizes.Sort();
            basin_sizes.Reverse();
            Console.WriteLine(basin_sizes[0] * basin_sizes[1] * basin_sizes[2]);                
        }
    }
}