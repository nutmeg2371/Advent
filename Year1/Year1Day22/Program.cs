﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Numerics;

static class Constants
{   
    public const int INIT_ARRAY_SIZE = 101;
    public const int ARRAY_SHIFT = 50;
}

namespace Year1Day22
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = "";

            
            List<int[]> commands_array = new List<int[]>();
            while ((curr_line = reader.ReadLine()) != null) 
            {
                curr_line = curr_line.Replace("on ", "1..").Replace("off ", "0..").Replace("x=", "").Replace(",y=", "..").Replace(",z=", "..");
                int[] cuboid_def = Array.ConvertAll(curr_line.Split(".."), x => int.Parse(x));
                commands_array.Add(cuboid_def);                                
            }
            reader.Close();
            

            List<int[]> lit_cuboids = new List<int[]>();
            foreach (int[] curr_cmd in commands_array)
            {
                int[] curr_cuboid = new int[] {curr_cmd[1], curr_cmd[2], curr_cmd[3], curr_cmd[4], curr_cmd[5], curr_cmd[6]};
                TurnCuboidOnOff(curr_cmd[0] == 1, curr_cuboid, ref lit_cuboids);
            }

            BigInteger num_lit = lit_cuboids.Aggregate((BigInteger)0, (acc, x) => acc + (BigInteger)(x[1] - x[0] + 1) * (x[3] - x[2] + 1) * (x[5] - x[4] + 1));
            Console.WriteLine(num_lit);
        }

        static void TurnCuboidOnOff(bool turn_on, int[] curr_cuboid, ref List<int[]> on_cuboids)
        {
            // see if it intersects with other cubes and break apart if so
            List<int[]> remaining_other_cuboids = new List<int[]>();

            foreach (int[] other_cuboid in on_cuboids)
            {
                int[] intersecting_cuboid = new int[6];
                if (GetCuboidIntersection(curr_cuboid, other_cuboid, ref intersecting_cuboid))
                {
                    remaining_other_cuboids.AddRange(SubtractCuboid(other_cuboid, intersecting_cuboid));
                }
                else
                {
                    remaining_other_cuboids.Add(other_cuboid);
                }
            }
            on_cuboids = new List<int[]>(remaining_other_cuboids);
            if (turn_on)
            {
                on_cuboids.Add(curr_cuboid);
            }
        }

        static bool GetCuboidIntersection (int[] cuboid1, int[] cuboid2, ref int[] intersecting_cuboid)
        {
            intersecting_cuboid[0] = Math.Max(cuboid1[0], cuboid2[0]);
            intersecting_cuboid[1] = Math.Min(cuboid1[1], cuboid2[1]);
            intersecting_cuboid[2] = Math.Max(cuboid1[2], cuboid2[2]);
            intersecting_cuboid[3] = Math.Min(cuboid1[3], cuboid2[3]);
            intersecting_cuboid[4] = Math.Max(cuboid1[4], cuboid2[4]);
            intersecting_cuboid[5] = Math.Min(cuboid1[5], cuboid2[5]);

            return (intersecting_cuboid[0] <= intersecting_cuboid[1] && intersecting_cuboid[2] <= intersecting_cuboid[3] && intersecting_cuboid[4] <= intersecting_cuboid[5]);
        }

        static List<int[]> SubtractCuboid(int[] cuboid1, int[] cuboid2)
        {
            List<int[]> remaining_cuboids = new List<int[]>();

            // split on z dimension, then y, the x
            for (int dimension_i = 4; dimension_i >= 0; dimension_i -= 2)
            {
                if (cuboid1[dimension_i] < cuboid2[dimension_i])
                {
                    int[] lower_z_cuboid = (int[]) cuboid1.Clone();
                    lower_z_cuboid[dimension_i + 1] = cuboid2[dimension_i] - 1;
                    remaining_cuboids.Add(lower_z_cuboid);
                    cuboid1[dimension_i] = cuboid2[dimension_i];
                }
                if (cuboid1[dimension_i + 1] > cuboid2[dimension_i + 1])
                {
                    int[] upper_z_cuboid = (int[]) cuboid1.Clone();
                    upper_z_cuboid[dimension_i] = cuboid2[dimension_i + 1] + 1;
                    remaining_cuboids.Add(upper_z_cuboid);
                    cuboid1[dimension_i + 1] = cuboid2[dimension_i + 1];
                }
            }  
            return remaining_cuboids;            
        }
    }
}
