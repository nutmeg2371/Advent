﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Year1Day25
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = "";

            List<char[]> lines = new List<char[]>();

            while ((curr_line = reader.ReadLine()) != null) 
            {
                lines.Add(curr_line.ToCharArray());   
            }
            reader.Close();

            int num_rows = lines.Count();
            int num_cols = lines[0].Length;

            int[,] curr_board = new int[num_rows, num_cols];

            for (int curr_line_num = 0; curr_line_num < num_rows; curr_line_num++)
            {
                for (int curr_pos = 0; curr_pos < num_cols; curr_pos++)
                {
                    switch (lines[curr_line_num][curr_pos])
                    {
                        case '>':
                        {
                            curr_board[curr_line_num, curr_pos] = 1;
                            break;
                        }
                        case 'v':
                        {
                            curr_board[curr_line_num, curr_pos] = 2;
                            break;
                        }
                        case '.':
                        {
                            curr_board[curr_line_num, curr_pos] = 0;
                            break;
                        }
                    }
                }
            }

            bool change = true;
            int step_num = 0;
            while (change)
            {
                change = false;
                int[,] next_board = new int[num_rows, num_cols];
                next_board = (int[,])curr_board.Clone();

                for (int curr_line_num = 0; curr_line_num < num_rows; curr_line_num++)
                {
                    for (int curr_pos = 0; curr_pos < num_cols; curr_pos++)
                    {
                        if (curr_board[curr_line_num,curr_pos] == 1 && curr_board[curr_line_num,(curr_pos + 1) % num_cols] == 0)
                        {
                            next_board[curr_line_num,(curr_pos + 1) % num_cols] = 1;
                            next_board[curr_line_num,curr_pos] = 0;
                            change = true;
                        }
                    }
                }
                if (change)
                {
                    curr_board = (int[,])next_board.Clone();
                }
                for (int curr_line_num = 0; curr_line_num < num_rows; curr_line_num++)
                {
                    for (int curr_pos = 0; curr_pos < num_cols; curr_pos++)
                    {
                        if (curr_board[curr_line_num,curr_pos] == 2 && curr_board[(curr_line_num + 1) % num_rows,curr_pos] == 0)
                        {
                            next_board[(curr_line_num + 1) % num_rows,curr_pos] = 2;
                            next_board[curr_line_num,curr_pos] = 0;
                            change = true;
                        }
                    }
                }
                step_num++;
                if (change)
                {
                    curr_board = next_board;
                    Console.WriteLine(step_num);
                }
            }
            Console.WriteLine(step_num);
        }
    }
}
