﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

static class Constants
{   
    public const int GRID_SIZE = 1000;
}

namespace Year1Day19
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = "";

            Dictionary<int,List<int[]>> input_map = new Dictionary<int,List<int[]>>();

            int curr_scanner_num = 0;
            while ((curr_line = reader.ReadLine()) != null) 
            {
                if (curr_line == "")
                {
                    continue;
                }
                if (curr_line.Contains("scanner"))
                {
                    curr_scanner_num = int.Parse(curr_line.Substring("--- scanner ".Length, curr_line.Length - "--- scanner ".Length - " ---".Length));
                    input_map[curr_scanner_num] = new List<int[]>();
                    continue;
                }
                int[] beacon_coords = Array.ConvertAll(curr_line.Split(","), s => int.Parse(s));
                input_map[curr_scanner_num].Add(beacon_coords);
            }
            reader.Close();

            Dictionary<int, List<List<int[]>>> perturbed_map = new Dictionary<int, List<List<int[]>>>();

            // for each coord set, all all perterbations
            HashSet<int> scanner_perturbs_to_find = new HashSet<int>();
            HashSet<int> all_scanner_perturbs_found = new HashSet<int>();
            Dictionary<int, List<int>> remaining_perturbs = new Dictionary<int, List<int>>();
            foreach (KeyValuePair<int, List<int[]>> curr_input_scanner in input_map)
            {
                int curr_beacon_num = 0;
                int curr_perturbation = 0;
                perturbed_map[curr_input_scanner.Key] = new List<List<int[]>>();
                scanner_perturbs_to_find.Add(curr_input_scanner.Key);
                remaining_perturbs.Add(curr_input_scanner.Key, Enumerable.Range(0, 24).ToList());

                for (curr_perturbation = 0; curr_perturbation < 24; curr_perturbation++)
                {
                    perturbed_map[curr_input_scanner.Key].Add(new List<int[]>());
                }
                foreach (int[] curr_beacon_coords in curr_input_scanner.Value)
                {
                    List<int[]> this_beacon_perturbed = PerturbInSpace(curr_beacon_coords);
                    curr_perturbation = 0;
                    foreach (int[] perturbed_coords in this_beacon_perturbed)
                    {
                        perturbed_map[curr_input_scanner.Key][curr_perturbation++].Add(perturbed_coords);
                    }
                    curr_beacon_num++;
                }
            }

            // call scanner 0's orientation correct
            scanner_perturbs_to_find.Remove(0);
            all_scanner_perturbs_found.Add(0);
            remaining_perturbs[0].Clear();
            remaining_perturbs[0].Add(0);
            Dictionary<int, int[]> shifted_positions = new Dictionary<int, int[]>();
            shifted_positions[0] = new int[]{0,0,0};

            // find "correct" perturbation of each scanner (store deltas along the way)
            HashSet<int> last_scanner_perturbs_found = new HashSet<int>(all_scanner_perturbs_found);
            while(scanner_perturbs_to_find.Count > 0)
            {
                HashSet<int> next_scanner_perturbs_found = new HashSet<int>();
                foreach (int scanner_i in last_scanner_perturbs_found)
                {
                    foreach (int scanner_j in scanner_perturbs_to_find)
                    {
                        foreach (int perturb_j in remaining_perturbs[scanner_j])
                        {
                            shifted_positions[scanner_j] = new int[3];
                            bool overlap = CheckNumOverlap(perturbed_map[scanner_i][remaining_perturbs[scanner_i][0]], perturbed_map[scanner_j][perturb_j], shifted_positions[scanner_j]);
                            if (overlap)
                            {
                                scanner_perturbs_to_find.Remove(scanner_j);
                                next_scanner_perturbs_found.Add(scanner_j);
                                all_scanner_perturbs_found.Add(scanner_j);
                                remaining_perturbs[scanner_j].Clear();
                                remaining_perturbs[scanner_j].Add(perturb_j);

                                // deltas are wrt matching scanner, change to wrt scanner 0
                                shifted_positions[scanner_j][0] = shifted_positions[scanner_i][0] - shifted_positions[scanner_j][0];
                                shifted_positions[scanner_j][1] = shifted_positions[scanner_i][1] - shifted_positions[scanner_j][1];
                                shifted_positions[scanner_j][2] = shifted_positions[scanner_i][2] - shifted_positions[scanner_j][2];
                                break;
                            }
                        }
                    }
                }
                last_scanner_perturbs_found = new HashSet<int>(next_scanner_perturbs_found);
            }

            // walk through each scanner and shift all of its beacons to scanner 0's coordinates
            HashSet<Tuple<int,int,int>> distinct_beacons = new HashSet<Tuple<int,int,int>>();
            for (int scanner_i = 0; scanner_i < all_scanner_perturbs_found.Count; scanner_i++)
            {
                foreach (int[] curr_beacon in perturbed_map[scanner_i][remaining_perturbs[scanner_i][0]])
                {
                    Tuple<int,int,int> new_beacon = new Tuple<int,int,int>(curr_beacon[0] + shifted_positions[scanner_i][0], curr_beacon[1] + shifted_positions[scanner_i][1], curr_beacon[2] + shifted_positions[scanner_i][2]);
                    distinct_beacons.Add(new_beacon);
                }
            }
            Console.WriteLine(distinct_beacons.Count);

            int max_distance = int.MinValue;
            for (int scanner_i = 0; scanner_i < shifted_positions.Count; scanner_i++)
            {
                for (int scanner_j = 0; scanner_j < shifted_positions.Count; scanner_j++)
                {
                    if (scanner_i != scanner_j)
                    {
                        int dist = Manhattan(shifted_positions[scanner_i], shifted_positions[scanner_j]);
                        if (dist > max_distance)
                        {
                            max_distance = dist;
                        }
                    }
                }
            }
            Console.WriteLine(max_distance);
        }

        static int Manhattan (int[] p1, int[] p2)
        {
            return Math.Abs(p1[0]-p2[0]) + Math.Abs(p1[1]-p2[1]) + Math.Abs(p1[2]-p2[2]);
        }

        static List<int[]> PerturbInSpace (int[] orig_coords)
        {
            List<int[]> perturbed = new List<int[]>();

            // I'm facing +x, +y is my left, +z is up
            perturbed.Add(orig_coords);
            // If -z is up, -y is on my left
            perturbed.Add(new int[] {orig_coords[0], -orig_coords[1], -orig_coords[2]});

            // if I now face +y, -x is my left, +z is up
            perturbed.Add(new int[] {orig_coords[1], -orig_coords[0], orig_coords[2]});
            // If -z is up, +x is on my left
            perturbed.Add(new int[] {orig_coords[1], orig_coords[0], -orig_coords[2]});

            // if I now face -x, -y is my left, +z is up
            perturbed.Add(new int[] {-orig_coords[0], -orig_coords[1], orig_coords[2]});
            // If -z is up, +y is on my left
            perturbed.Add(new int[] {-orig_coords[0], orig_coords[1], -orig_coords[2]});

            // if I now face -y, +x is my left, +z is up
            perturbed.Add(new int[] {-orig_coords[1], orig_coords[0], orig_coords[2]});
            // If -z is up, -x is on my left
            perturbed.Add(new int[] {-orig_coords[1], -orig_coords[0], -orig_coords[2]});

            // If I now face +z, +x is on my left, +y is up
            perturbed.Add(new int[] {orig_coords[2], orig_coords[0], orig_coords[1]});
            // If -y is up, -x is on my left
            perturbed.Add(new int[] {orig_coords[2], -orig_coords[0], -orig_coords[1]});
            
            // If I now face +x, -z is on my left, +y is up
            perturbed.Add(new int[] {orig_coords[0], -orig_coords[2], orig_coords[1]});
            // If -y is up, +z is on my left
            perturbed.Add(new int[] {orig_coords[0], orig_coords[2], -orig_coords[1]});

            // If I now face -z, -x is on my left, +y is up
            perturbed.Add(new int[] {-orig_coords[2], -orig_coords[0], orig_coords[1]});
            // If -y is up, +x is on my left
            perturbed.Add(new int[] {-orig_coords[2], orig_coords[0], -orig_coords[1]});

            // If I now face -x, +z is on my left, +y is up
            perturbed.Add(new int[] {-orig_coords[0], orig_coords[2], orig_coords[1]});
            // If -y is up, -z is on my left
            perturbed.Add(new int[] {-orig_coords[0], -orig_coords[2], -orig_coords[1]});

            // If I now face +z, -y is on my left, +x is up
            perturbed.Add(new int[] {orig_coords[2], -orig_coords[1], orig_coords[0]});
            // If -y is up, +y is on my left
            perturbed.Add(new int[] {orig_coords[2], orig_coords[1], -orig_coords[0]});

            // If I now face -y, -z is on my left, +x is up
            perturbed.Add(new int[] {-orig_coords[1], -orig_coords[2], orig_coords[0]});
            // If -y is up, +z is on my left
            perturbed.Add(new int[] {-orig_coords[1], orig_coords[2], -orig_coords[0]});

            // If I now face -z, +y is on my left, +x is up
            perturbed.Add(new int[] {-orig_coords[2], orig_coords[1], orig_coords[0]});
            // If -y is up, -y is on my left
            perturbed.Add(new int[] {-orig_coords[2], -orig_coords[1], -orig_coords[0]});

            // If I now face +y, +z is on my left, +x is up
            perturbed.Add(new int[] {orig_coords[1], orig_coords[2], orig_coords[0]});
            // If -y is up, -z is on my left
            perturbed.Add(new int[] {orig_coords[1], -orig_coords[2], -orig_coords[0]});

            return perturbed;
        }

        static bool CheckNumOverlap (List<int[]> scanner1, List<int[]> scanner2_in, int[] deltas)
        {
            // for each point in scanner 1, match with a point in scanner 2 and see if any of the rest line up
            foreach (int[] scanner1_point in scanner1)
            {
                foreach (int[] scanner2_point in scanner2_in)
                {
                    int delta_x = scanner2_point[0] - scanner1_point[0];
                    int delta_y = scanner2_point[1] - scanner1_point[1];
                    int delta_z = scanner2_point[2] - scanner1_point[2];
                    int num_matches = 0;

                    List<int[]> scanner2 = new List<int[]>(scanner2_in);
                    foreach (int[] scanner1_point_match in scanner1)
                    {
                        int[] target_point = new int[]{scanner1_point_match[0] + delta_x, scanner1_point_match[1] + delta_y, scanner1_point_match[2] + delta_z};
                        int pos_found = ContainsPoint(scanner2, target_point);
                        if (pos_found > -1)
                        {
                            scanner2.RemoveRange(pos_found, 1);
                            num_matches++;
                        }
                    }
                    if (num_matches >= 12)
                    {
                        deltas[0] = delta_x;
                        deltas[1] = delta_y;
                        deltas[2] = delta_z;

                        return true;
                    }
                }
            }
            return false;
        }

        public static int ContainsPoint (List<int[]> scanner, int[] target_point)
        {
            for (int beacon_id = 0; beacon_id < scanner.Count; beacon_id++)
            {
                if (target_point[0] == scanner[beacon_id][0] && target_point[1] == scanner[beacon_id][1] && target_point[2] == scanner[beacon_id][2])
                {
                    return beacon_id;
                }
            }
            return -1;
        }
    }
}
