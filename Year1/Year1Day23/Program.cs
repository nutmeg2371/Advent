﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

static class Constants
{   
    public const int HALLWAY_LENGTH = 11;
    public const int ROOM_COUNT = 4;
    public static readonly char[] ROOM_TO_LETTER_MAP = {'A', 'B', 'C', 'D'};
    public static readonly int[] ROOM_TO_TURN_MAP = {2,4,6,8};
}

static class NearConstants
{   
    public static int ROOM_DEPTH = 0;
    public static int BOARD_SIZE = 0;
    public static int SCORE_INDEX = 0;
}

namespace Year1Day23
{
    class Program
    {
        static void Main(string[] args)
        {

            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = "";

            List<char[]> lines = new List<char[]>();

            while ((curr_line = reader.ReadLine()) != null) 
            {
                curr_line = curr_line.Replace(".", "").Replace("#", "").Replace(" ", "");
                if (curr_line != "")
                {
                    lines.Add(curr_line.ToCharArray());   
                }
            }
            reader.Close();

            // build the first board and figure out the near constants
            NearConstants.ROOM_DEPTH = lines.Count();
            NearConstants.BOARD_SIZE = Constants.HALLWAY_LENGTH + Constants.ROOM_COUNT * NearConstants.ROOM_DEPTH;
            NearConstants.SCORE_INDEX = 2 * NearConstants.BOARD_SIZE;
            
            int[] initial_board = new int [2 * NearConstants.BOARD_SIZE + 1];
            for (int spot_i = 0; spot_i < NearConstants.BOARD_SIZE; spot_i++)
            {
               initial_board[spot_i] = '.'; 
            }

            int curr_line_num = 0;
            int curr_line_pos = 0;
            int init_pos = Constants.HALLWAY_LENGTH + curr_line_num * curr_line_pos;
            while (init_pos < NearConstants.BOARD_SIZE) 
            {
                initial_board[init_pos] = lines[curr_line_num][curr_line_pos];
                init_pos++;
                curr_line_num++;
                if (curr_line_num >= lines.Count)
                {
                    curr_line_pos++;
                    curr_line_num = 0;
                } 
            }

            // lock everyone who started out in a workable position
            for (int char_index = 0; char_index < Constants.ROOM_TO_LETTER_MAP.Length; char_index++)
            {
                int depth_in_room = NearConstants.ROOM_DEPTH;
                int curr_pos = Constants.HALLWAY_LENGTH + char_index * NearConstants.ROOM_DEPTH + depth_in_room - 1;
                while (depth_in_room > 0 && initial_board[curr_pos] == Constants.ROOM_TO_LETTER_MAP[char_index])
                {
                    initial_board[curr_pos + NearConstants.BOARD_SIZE] = 1;
                    curr_pos--;
                    depth_in_room--;
                }
            }

            List<int[]> curr_poss_states = new List<int[]>();
            curr_poss_states.Add(initial_board);
            int lowest_cost = int.MaxValue;

            while (curr_poss_states.Count > 0)
            {
                List<int[]> all_next_poss_states =new List<int[]>();

                foreach (int[] curr_board in curr_poss_states)
                {
                    if (curr_board[NearConstants.SCORE_INDEX] >= lowest_cost)
                    {
                        continue;
                    }
                    List<int[]> next_boards_this_board = GetPossibleNextBoards(curr_board);

                    foreach (int[] next_board in next_boards_this_board)
                    {
                        if (next_board[NearConstants.SCORE_INDEX] < lowest_cost)
                        {
                            if (IsFinished(next_board))
                            {
                                lowest_cost = next_board[NearConstants.SCORE_INDEX];
                            }
                            else
                            {
                                all_next_poss_states.Add(next_board);
                            }
                        }
                    }
                }
                curr_poss_states = BoardDedup(all_next_poss_states);
            }
            Console.WriteLine(lowest_cost);
        }

        static bool IsFinished(int[] curr_board)
        {
            for (int char_index = 0; char_index < Constants.ROOM_TO_LETTER_MAP.Length; char_index++)
            {
                int depth_in_room = NearConstants.ROOM_DEPTH;
                int curr_pos = Constants.HALLWAY_LENGTH + char_index * NearConstants.ROOM_DEPTH + depth_in_room - 1;
                while (depth_in_room > 0)
                {
                    if (curr_board[curr_pos] != Constants.ROOM_TO_LETTER_MAP[char_index])
                    {
                        return false;
                    }
                    curr_pos--;
                    depth_in_room--;
                }
            }
            return true;
        }

        static List<int[]> GetPossibleNextBoards(int[] curr_board)
        {
            List<int[]> next_poss_boards = new List<int[]>();

            for (int curr_pos = 0; curr_pos < NearConstants.BOARD_SIZE; curr_pos++)
            {
                // can't move anything in an empty slot or anything locked
                if (curr_board[curr_pos] == '.' || curr_board[curr_pos + NearConstants.BOARD_SIZE] == 1)
                {
                    continue;
                }

                // build the list of possible next boards
                List<int> next_positions = new List<int>();
                
                // add final targets for this letter
                for (int char_index = 0; char_index < Constants.ROOM_TO_LETTER_MAP.Length; char_index++)
                {
                    if (Constants.ROOM_TO_LETTER_MAP[char_index] == curr_board[curr_pos])
                    {        
                        int room_start_pos = Constants.HALLWAY_LENGTH + char_index * NearConstants.ROOM_DEPTH;
                        for (int depth = 0; depth < NearConstants.ROOM_DEPTH; depth++)
                        {
                            next_positions.Add(room_start_pos + depth);
                        }
                        break;
                    }
                }

                // if the letter is in a room, add the hallway as possible targts
                if (curr_pos >= Constants.HALLWAY_LENGTH)
                {
                    next_positions.AddRange(Enumerable.Range(0, Constants.HALLWAY_LENGTH));
                    foreach (int curr_turn in Constants.ROOM_TO_TURN_MAP)
                    {
                        next_positions.Remove(curr_turn);
                    }
                }
                foreach (int target_pos in next_positions)
                {
                    int poss_score = CheckPath(target_pos, curr_pos, curr_board);
                    if (poss_score > 0)
                    {
                        int[] next_board = new int[curr_board.Length];
                        Array.Copy(curr_board, next_board, curr_board.Length);
                        next_board[curr_pos] = '.';
                        next_board[target_pos] = curr_board[curr_pos];
                        next_board[NearConstants.SCORE_INDEX] += poss_score;
                        // if we finished a letter, lock it up
                        if (target_pos >= Constants.HALLWAY_LENGTH)
                        {
                           next_board[target_pos + NearConstants.BOARD_SIZE] = 1;
                        }
                        next_poss_boards.Add(next_board);
                    }
                }
            }
            return next_poss_boards;
        }

        static int CheckPath(int target_pos, int curr_pos, int[] curr_board)
        {
            int cost = -1;
            if (curr_board[target_pos] != '.')
            {
                return cost;
            }

            int curr_room = -1;
            int target_room = -1;
            int cost_out_of_room = 0;
            int cost_into_room = 0;

            // if we are moving into a room
            if (target_pos >= Constants.HALLWAY_LENGTH)
            {
                int level_in_room = (target_pos - Constants.HALLWAY_LENGTH) % NearConstants.ROOM_DEPTH;
                int delta_level = 0;

                // ensure we aren't blocking a spot below us (that is empty or that is someone else trying to get out)
                delta_level = 1;
                while (level_in_room + delta_level < NearConstants.ROOM_DEPTH)
                {
                    if (curr_board[curr_pos] != curr_board[target_pos + delta_level])
                    {
                        return cost;
                    }
                    delta_level++;
                }

                // ensure no one is above where we are trying to go, blocking us
                delta_level = -1;
                while (level_in_room + delta_level >= 0)
                {
                    if (curr_board[target_pos + delta_level] != '.')
                    {
                        return cost;
                    }
                    delta_level--;
                }
                target_room = (target_pos - Constants.HALLWAY_LENGTH) / NearConstants.ROOM_DEPTH;
                cost_into_room = level_in_room + 1;
            }

            // if we are moving out of a room
            if (curr_pos >= Constants.HALLWAY_LENGTH)
            {
                int level_in_room = (curr_pos - Constants.HALLWAY_LENGTH) % NearConstants.ROOM_DEPTH;
                int delta_level = 0;

                // ensure no one is above where we are trying to go, blocking us
                delta_level = -1;
                while (level_in_room + delta_level >= 0)
                {
                    if (curr_board[curr_pos + delta_level] != '.')
                    {
                        return cost;
                    }
                    delta_level--;
                }
                curr_room = (curr_pos - Constants.HALLWAY_LENGTH) / NearConstants.ROOM_DEPTH;
                cost_out_of_room = level_in_room + 1;
            }
            
            // get the source and target turning points.  if one of our ends isn't a room, make the turning point its location
            int curr_turning_point = (curr_room == -1) ? curr_pos :  Constants.ROOM_TO_TURN_MAP[curr_room]; 
            int target_turning_point = (target_room == -1) ? target_pos :  Constants.ROOM_TO_TURN_MAP[target_room]; 

            // ensure that the turning points are clear, and the paths between them
            for (int pos_i = Math.Min(curr_turning_point, target_turning_point) + 1; pos_i <= Math.Max(curr_turning_point, target_turning_point); pos_i++)
            {
                if (curr_board[pos_i] != '.' && pos_i != curr_pos)
                {
                    return cost;
                }
            }
            
            cost = Math.Abs(curr_turning_point - target_turning_point) + cost_into_room + cost_out_of_room;   
              
            switch (curr_board[curr_pos])
            {
                case 'B':
                {
                    cost *= 10;
                    break;
                }
                case 'C':
                {
                    cost *= 100;
                    break;
                }
                case 'D':
                {
                    cost *= 1000;
                    break;
                }
            }
            return cost;
        }
        static List<int[]> BoardDedup(List<int[]> curr_boards)
        {
            if (curr_boards.Count <= 0)
            {
                return curr_boards;
            }
            List<int[]> deduped_boards = new List<int[]>();

            foreach (int[] curr_board in curr_boards)
            {
                bool is_duplicate = false;

                foreach (int[] deduped_board in deduped_boards)
                {
                    if (CompareArrays(curr_board, deduped_board) == 0)
                    {
                        deduped_board[NearConstants.SCORE_INDEX] = Math.Min(curr_board[NearConstants.SCORE_INDEX], deduped_board[NearConstants.SCORE_INDEX]);
                        is_duplicate = true;
                        break;
                    }
                }
                if (!is_duplicate)
                {
                    deduped_boards.Add(curr_board);
                }
            }
            Console.WriteLine("  Deduped " + curr_boards.Count + " -> " + deduped_boards.Count + " (" + (Int64) deduped_boards.Count * 100 / curr_boards.Count + "%)");
            return deduped_boards;
        }

        static int CompareArrays(int[] board1, int[] board2)
        {
            int total_diff = 0;
            for (int i = 0; i < board1.Length; i++)
            {
                if (i == NearConstants.SCORE_INDEX)
                {
                    continue;
                }
                total_diff += Math.Abs(board1[i] - board2[i]);
            }
            return total_diff;
        }
    }
}
