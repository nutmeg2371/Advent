﻿using System;
using System.IO;
using System.Collections.Generic;


static class Constants
{   
    public const int GRID_SIZE = 100;
    public const int GRID_MULT = 5;
}

namespace Year1Day15
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            int [,] input_grid = new int[Constants.GRID_SIZE, Constants.GRID_SIZE];

            int line_num = 0;
            while ((curr_line = reader.ReadLine()) != null) 
            {                 
                for (int i = 0; i < curr_line.Length; i++)
                {
                    input_grid[line_num, i] = int.Parse(curr_line[i].ToString());
                }
                line_num++;
                
            } 
            reader.Close();

            Console.WriteLine(GetPathRisk(input_grid, Constants.GRID_SIZE));
            
            int [,] huge_grid = new int[Constants.GRID_SIZE * Constants.GRID_MULT, Constants.GRID_SIZE * Constants.GRID_MULT];

            for (int curr_row = 0; curr_row < Constants.GRID_SIZE * Constants.GRID_MULT; curr_row++)
            {
                for (int curr_col = 0; curr_col < Constants.GRID_SIZE * Constants.GRID_MULT; curr_col++)
                {
                    huge_grid[curr_row, curr_col] = input_grid[curr_row % Constants.GRID_SIZE, curr_col % Constants.GRID_SIZE] + curr_col / Constants.GRID_SIZE + curr_row / Constants.GRID_SIZE;
                    if (huge_grid[curr_row, curr_col] > 9)
                    {
                        huge_grid[curr_row, curr_col] -= 9;
                    }
                }
            }
            Console.WriteLine(GetPathRisk(huge_grid, Constants.GRID_SIZE * Constants.GRID_MULT));
        }

        static int GetPathRisk(int[,] grid, int grid_size)
        {

            int [,] path_risk = new int[grid_size, grid_size];
            int max_path_risk = grid_size * grid_size * 9;

            for (int curr_row = 0; curr_row < grid_size; curr_row++)
            {
                for (int curr_col = 0; curr_col < grid_size; curr_col++)
                {
                    path_risk[curr_row, curr_col] = max_path_risk;
                }
            }
            path_risk[grid_size - 1, grid_size - 1] = grid[grid_size - 1, grid_size - 1];

            bool change = true;
            while(change)
            {
                change = false;
                for (int this_row = grid_size - 1; this_row >= 0; this_row--)
                {
                    for (int this_col = grid_size - 1; this_col >= 0; this_col--)
                    {
                        if (this_row > 0 && path_risk[this_row - 1, this_col] + grid[this_row, this_col] < path_risk[this_row, this_col])
                        {
                            path_risk[this_row, this_col] = path_risk[this_row - 1, this_col] + grid[this_row, this_col];
                            change = true;
                        }
                        if (this_col > 0 && path_risk[this_row, this_col - 1] + grid[this_row, this_col] < path_risk[this_row, this_col])
                        {
                            path_risk[this_row, this_col] = path_risk[this_row, this_col - 1] + grid[this_row, this_col];
                            change = true;
                        }
                        if (this_row < grid_size -1 && path_risk[this_row + 1, this_col] + grid[this_row, this_col] < path_risk[this_row, this_col])
                        {
                            path_risk[this_row, this_col] = path_risk[this_row + 1, this_col] + grid[this_row, this_col];
                            change = true;
                        }
                        if (this_col < grid_size -1 && path_risk[this_row, this_col + 1] + grid[this_row, this_col] < path_risk[this_row, this_col])
                        {
                            path_risk[this_row, this_col] = path_risk[this_row, this_col + 1] + grid[this_row, this_col];
                            change = true;
                        }                        
                    }
                }
            }

            return path_risk[0, 0] - grid[0, 0];
        }
    }
}
