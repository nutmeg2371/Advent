﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;


static class Constants
{   
    public const int VERSION_LEN = 3;
    public const int TYPE_LEN = 3;
    public const int LITERAL_CHUNK_LEN = 5;
    public const int NIBBLE_LEN = 4;
    public const int SUBPACKETTYPE_LEN = 1;
    public const int SUBPACK_BIT_LEN = 15;
    public const int SUBPACK_NUM_PACKETS_LEN = 11;
    public const int ID_NUM_BITS = 0;
    public const int ID_NUM_PACKETS = 1;
    public const int OP_ADD = 0;
    public const int OP_MULT = 1;
    public const int OP_MIN = 2;
    public const int OP_MAX = 3;
    public const int OP_LITERAL = 4;
    public const int OP_GREATER = 5;
    public const int OP_LESS = 6;
    public const int OP_EQUAL = 7;

    public const int STATE_VERSION = 0;
    public const int STATE_TYPE = 1;
    public const int STATE_LITERAL = 2;
    public const int STATE_OPERATOR = 3;
    public const int STACK_OPERATOR = 0;
    public const int STACK_SUBPLACEHOLDER = 1;
    public const int STACK_LITERAL = 2;
}

namespace Year1Day16
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = reader.ReadLine();
            reader.Close();

            string packet_stream = "";
            foreach (char c in curr_line)
            {
                packet_stream += Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0');
            }

            int curr_state = Constants.STATE_VERSION;
            int packet_pos = 0;
            int total_versions = 0;
            // op, whether #bits/#packets if op, literal if literal, #bits/#packets, #bits this subtree
            TreeNode<Tuple<int, Int64, int, int>> sub_packet_tree = null;;
            TreeNode<Tuple<int, Int64, int, int>> curr_tree_node = sub_packet_tree;
            Stack<int> packet_starts = new Stack<int>();
            int min_packet_length = Constants.VERSION_LEN + Constants.TYPE_LEN + Constants.LITERAL_CHUNK_LEN;
            while (packet_pos < packet_stream.Length)
            {
                switch(curr_state)
                {
                    case Constants.STATE_VERSION:
                    {
                        if (packet_stream.Length - packet_pos < min_packet_length)
                        {
                            packet_pos = packet_stream.Length;
                            break;
                        }
                        total_versions += Convert.ToInt32(packet_stream.Substring(packet_pos, Constants.VERSION_LEN), 2);
                        packet_starts.Push(packet_pos);
                        packet_pos += Constants.VERSION_LEN;
                        curr_state = Constants.STATE_TYPE;
                        break;
                    }
                    case Constants.STATE_TYPE:
                    {
                        int packet_type = Convert.ToInt32(packet_stream.Substring(packet_pos, Constants.TYPE_LEN), 2);
                        packet_pos += Constants.TYPE_LEN;
                        if (packet_type == Constants.OP_LITERAL)
                        {
                            curr_state = Constants.STATE_LITERAL;
                        }
                        else
                        {
                            if (curr_tree_node == null)
                            {
                                curr_tree_node = new TreeNode<Tuple<int, Int64, int, int>>(new Tuple<int, Int64, int, int>(packet_type, 0, 0, 0));
                                sub_packet_tree = curr_tree_node;
                            }
                            else
                            {
                                curr_tree_node = curr_tree_node.AddChild(new Tuple<int, Int64, int, int>(packet_type, 0, 0, 0));
                            }
                            curr_state = Constants.STATE_OPERATOR;                            
                        }
                        break;
                    }
                    case Constants.STATE_LITERAL:
                    {
                        int prev_pos = packet_pos;
                        Tuple<int, Int64> parsed_literal = ParseLiteral(packet_stream, packet_pos);
                        packet_pos = parsed_literal.Item1;
                        int bits_this_packet = packet_pos - prev_pos + Constants.VERSION_LEN + Constants.TYPE_LEN;

                        if (curr_tree_node.Data.Item1 != Constants.OP_LITERAL)
                        {
                            curr_tree_node.AddChild(new Tuple<int, Int64, int, int>(Constants.OP_LITERAL, parsed_literal.Item2, 0, bits_this_packet)); 
                            
                            bool finished_node = true;
                            while (finished_node)
                            {
                                finished_node = false;
                                int childrens_bits = 0;
                                int header_bits = Constants.VERSION_LEN + Constants.TYPE_LEN + Constants.SUBPACKETTYPE_LEN;
                                List<Int64> curr_literals = new List<Int64>();
                                foreach (TreeNode<Tuple<int, Int64, int, int>> curr_child in curr_tree_node.Children)
                                {
                                    childrens_bits += curr_child.Data.Item4;
                                    curr_literals.Add(curr_child.Data.Item2);
                                }

                                if (curr_tree_node.Data.Item2 == Constants.ID_NUM_BITS)
                                {
                                    header_bits += Constants.SUBPACK_BIT_LEN;
                                }
                                else if (curr_tree_node.Data.Item2 == Constants.ID_NUM_PACKETS)
                                {
                                    header_bits += Constants.SUBPACK_NUM_PACKETS_LEN;
                                }

                                if ((curr_tree_node.Data.Item2 == Constants.ID_NUM_BITS && curr_tree_node.Data.Item3 == childrens_bits) ||
                                    (curr_tree_node.Data.Item2 == Constants.ID_NUM_PACKETS && curr_tree_node.Data.Item3 == curr_tree_node.Children.Count))
                                {
                                    Int64 answer = ApplyOperator(curr_literals, curr_tree_node.Data.Item1);
                                    curr_tree_node.Data = new Tuple<int, Int64, int, int>(Constants.OP_LITERAL, answer, 0, childrens_bits + header_bits);
                                    curr_tree_node.Children.Clear();
                                    curr_tree_node = curr_tree_node.Parent;
                                    finished_node = true;
                                }
                                if (curr_tree_node == null)
                                {
                                    break;
                                }
                            }
                        }

                        curr_state = Constants.STATE_VERSION;
                        break;
                    }
                    case Constants.STATE_OPERATOR:
                    {
                        int sub_type = Convert.ToInt32(packet_stream.Substring(packet_pos, Constants.SUBPACKETTYPE_LEN), 2);
                        packet_pos += Constants.SUBPACKETTYPE_LEN;

                        if (sub_type == Constants.ID_NUM_BITS)
                        {
                            int num_bits = Convert.ToInt32(packet_stream.Substring(packet_pos, Constants.SUBPACK_BIT_LEN), 2);
                            packet_pos += Constants.SUBPACK_BIT_LEN;
                            curr_state = Constants.STATE_VERSION;   
                            curr_tree_node.Data = new Tuple<int, Int64, int, int>(curr_tree_node.Data.Item1, Constants.ID_NUM_BITS, num_bits, 0);                    
                        }
                        else if (sub_type == Constants.ID_NUM_PACKETS)
                        {
                            int num_subpackets = Convert.ToInt32(packet_stream.Substring(packet_pos, Constants.SUBPACK_NUM_PACKETS_LEN), 2);
                            packet_pos += Constants.SUBPACK_NUM_PACKETS_LEN;
                            curr_state = Constants.STATE_VERSION;
                            curr_tree_node.Data = new Tuple<int, Int64, int, int>(curr_tree_node.Data.Item1, Constants.ID_NUM_PACKETS, num_subpackets, 0);  
                        }
                        break;
                    }
                }
            }
            Console.WriteLine(total_versions);
            Console.WriteLine(sub_packet_tree.Data.Item2);
          
        }
        static Tuple<int, Int64> ParseLiteral (string packet_stream, int start_packet_pos)
        {
            string bin_literal = "";
            while (true)
            {
                string curr_nibble = packet_stream.Substring(start_packet_pos, Constants.LITERAL_CHUNK_LEN);
                start_packet_pos += Constants.LITERAL_CHUNK_LEN;
                bin_literal += curr_nibble.Substring(1, Constants.NIBBLE_LEN);
                if (curr_nibble[0] == '0')
                {
                    break;
                }
            }
            return new Tuple<int, Int64>(start_packet_pos, Convert.ToInt64(bin_literal, 2));
        }
        static Int64 ApplyOperator (List<Int64> literals, int in_operator)
        {
            Int64 answer = 0;
            switch(in_operator)
            {
                case Constants.OP_ADD: //+
                {
                    answer = literals.Aggregate((Int64)0, (acc, x) => acc + x);
                    break;
                }
                case Constants.OP_MULT: //*
                {
                    answer = literals.Aggregate((Int64)1, (acc, x) => acc * x);
                    break;
                }
                
                case Constants.OP_MIN: //min
                {
                    List<Int64> sorted_literals = new List<Int64>(literals);
                    sorted_literals.Sort();
                    answer = sorted_literals[0];
                    break;
                }
                
                case Constants.OP_MAX: //max
                {
                    List<Int64> sorted_literals = new List<Int64>(literals);
                    sorted_literals.Sort();
                    answer = sorted_literals[sorted_literals.Count - 1];
                    break;
                }
                case Constants.OP_GREATER: //>
                {
                    if (literals[0] > literals[1])
                    {
                        answer = 1;
                    }
                    break;
                }
                case Constants.OP_LESS: //<
                {
                    if (literals[0] < literals[1])
                    {
                        answer = 1;
                    }
                    break;
                }
                case Constants.OP_EQUAL: //==
                {
                    if (literals[0] == literals[1])
                    {
                        answer = 1;
                    }
                    break;
                }
            }
            return answer;
        }
    }

    public class TreeNode<T>
    {
        public T Data { get; set; }
        public TreeNode<T> Parent { get; set; }
        public ICollection<TreeNode<T>> Children { get; set; }

        public TreeNode(T data)
        {
            this.Data = data;
            this.Children = new LinkedList<TreeNode<T>>();
        }

        public TreeNode<T> AddChild(T child)
        {
            TreeNode<T> childNode = new TreeNode<T>(child) { Parent = this };
            this.Children.Add(childNode);
            return childNode;
        }
    }
}
