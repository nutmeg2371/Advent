﻿using System;
using System.IO;
using System.Collections.Generic;


static class Constants
{
    public const int CARD_SIZE = 5;
}

namespace Advent
{
    class Year1Day4
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = reader.ReadLine();

            string[] called_nums_strs = curr_line.Split(",");
            int[] called_nums = Array.ConvertAll(called_nums_strs, s => int.Parse(s));
            
            int[,,] curr_card = new int[Constants.CARD_SIZE,Constants.CARD_SIZE,2];
            List<int[,,]> all_cards = new List<int[,,]>();
            int input_line_num = 0;
            while ((curr_line = reader.ReadLine()) != null) 
            {
                if (curr_line == "")
                {
                    continue;
                }
                string[] this_line = curr_line.Replace("  "," ").Trim().Split(" ");
                for (int col_num = 0; col_num < this_line.GetLength(0); col_num++)
                {
                    curr_card[input_line_num,col_num,0] = int.Parse(this_line[col_num].Trim());
                }
                if (++input_line_num == 5)
                {
                    input_line_num = 0;
                    all_cards.Add((int[,,])curr_card.Clone());
                }

            }
            reader.Close();

            List<int[,,]> remaining_cards = new List<int[,,]>();
            List<Tuple<int[,,],int>> winning_cards = new List<Tuple<int[,,], int>>();

            foreach (int curr_called_num in called_nums)
            {
                foreach (int[,,] this_card in all_cards)
                {
                    bool bingo = false;
                    int score_this_card = 0;  
                    for (int curr_row = 0; curr_row < Constants.CARD_SIZE; curr_row++)
                    {
                        for (int curr_col = 0; curr_col < Constants.CARD_SIZE; curr_col++)
                        {
                            if (this_card[curr_row,curr_col,0] == curr_called_num)
                            {
                                // mark the new spot
                                this_card[curr_row,curr_col,1] = 1;

                                // did this make bingo?  
                                if (!bingo)
                                {
                                    bingo = true;  
                                    for (int check_row = 0; check_row < Constants.CARD_SIZE; check_row++)
                                    {
                                        if (this_card[check_row,curr_col,1] == 0)
                                        {
                                            bingo = false;
                                            break;
                                        }
                                    }
                                }
                                if (!bingo)
                                {
                                    bingo = true;    
                                    for (int check_col = 0; check_col < Constants.CARD_SIZE; check_col++)
                                    {
                                        if (this_card[curr_row,check_col,1] == 0)
                                        {
                                            bingo = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else if (this_card[curr_row,curr_col,1] == 0)
                            {
                                score_this_card += this_card[curr_row,curr_col,0];
                            }
                        }
                    }
                    if (bingo)
                    {
                        winning_cards.Add(new Tuple<int[,,], int>(this_card, score_this_card * curr_called_num));
                    }
                    else
                    {
                        remaining_cards.Add(this_card);
                    }
                }
                all_cards = new List<int[,,]>(remaining_cards);
                remaining_cards.Clear();
            }
            
            Console.WriteLine (winning_cards[0].Item2);
            Console.WriteLine (winning_cards[winning_cards.Count - 1].Item2);
        }
    }
}