﻿using System;
using System.IO;
using System.Collections.Generic;


static class Constants
{
  public const int PART1_NUM_STEPS = 10;
  public const int PART2_NUM_STEPS = 40;
  public const int ARRAY_SIZE = 100;
}

namespace Year1Day14
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            string curr_chain = reader.ReadLine();
            reader.ReadLine();
            

            Int64[,] seen_matrix = new Int64[Constants.ARRAY_SIZE, Constants.ARRAY_SIZE];
            Int64[] counts_matrix = new Int64[Constants.ARRAY_SIZE];
            for (int curr_pos = 0; curr_pos < curr_chain.Length - 1; curr_pos++)
            {
                seen_matrix[curr_chain[curr_pos],curr_chain[curr_pos+1]] ++;
                counts_matrix[curr_chain[curr_pos]] ++;
            }
            counts_matrix[curr_chain[curr_chain.Length-1]] ++;

            Dictionary<string,string> rules = new Dictionary<string, string>();
            while ((curr_line = reader.ReadLine()) != null) 
            { 
                string[] rule_this_line = curr_line.Split(" -> ");
                rules.Add(rule_this_line[0],rule_this_line[1]);
            } 
            reader.Close();

            for (int step_num = 0; step_num < Constants.PART2_NUM_STEPS; step_num++)
            {
                Int64[,] next_seen_matrix = new Int64[Constants.ARRAY_SIZE, Constants.ARRAY_SIZE];
                foreach (KeyValuePair<string,string> curr_rule in rules)
                {
                    Int64 times_seen = seen_matrix[curr_rule.Key[0],curr_rule.Key[1]];
                    next_seen_matrix[curr_rule.Key[0],curr_rule.Value[0]] += times_seen;
                    next_seen_matrix[curr_rule.Value[0],curr_rule.Key[1]] += times_seen;
                    counts_matrix[curr_rule.Value[0]] += times_seen;
                }
                seen_matrix = (Int64[,])next_seen_matrix.Clone();

                if (step_num == Constants.PART1_NUM_STEPS - 1)
                {
                    Console.WriteLine(GetMaxMinDiff(counts_matrix));
                }
            }
            Console.WriteLine(GetMaxMinDiff(counts_matrix));
        }

        static Int64 GetMaxMinDiff(Int64[] counts_matrix)
        {
            Int64 min_char = Int64.MaxValue;
            Int64 max_char = 0;
            for (Int64 i = 0; i < counts_matrix.Length; i++)
            {
                if (counts_matrix[i] < min_char && counts_matrix[i] != 0)
                {
                    min_char = counts_matrix[i];
                }
                if (counts_matrix[i] > max_char)
                {
                    max_char = counts_matrix[i];
                }
            }
            return (max_char-min_char);
        }
    }
}
