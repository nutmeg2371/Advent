﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

static class Constants
{   
    public const int WINNING_SCORE_P1 = 1000;
    public const int WINNING_SCORE_P2 = 21;
    public const int NUM_POSITIONS = 10;
}

namespace Year1Day21
{
    class Program
    {
        static int DeterministicDieNumber = 0;
        static int DieRollCount = 0;

        static int[] die_roll_possibilities;

        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = "";

            List<int> player_starts = new List<int>();
            while ((curr_line = reader.ReadLine()) != null && curr_line != "") 
            {
                player_starts.Add(int.Parse(curr_line.Split("position: ")[1]));
            }
            reader.Close();           

            int[] player_positions = new int[player_starts.Count];
            int[] player_scores = new int[player_starts.Count];

            for (int curr_player = 0; curr_player < player_starts.Count; curr_player++)
            {
                player_positions[curr_player] = player_starts[curr_player];
            }

            int curr_player_turn = 0;
            while(true)
            {
                int this_turn_moves = RollDeterministicDie() + RollDeterministicDie() + RollDeterministicDie();
                player_positions[curr_player_turn] += this_turn_moves;
                while (player_positions[curr_player_turn] > Constants.NUM_POSITIONS)
                {
                    player_positions[curr_player_turn] -= Constants.NUM_POSITIONS;
                }
                player_scores[curr_player_turn] += player_positions[curr_player_turn];
                if (player_scores[curr_player_turn] >= Constants.WINNING_SCORE_P1)
                {
                    break;
                }
                curr_player_turn ++;
                curr_player_turn %= player_starts.Count;
            }
            curr_player_turn --;
            if (curr_player_turn < 0)
            {
                curr_player_turn = player_starts.Count - 1;
            }
            Console.WriteLine(player_scores[curr_player_turn] * DieRollCount);
            



            // for each player, keep track of:
            //    how many universes it is in each of 10 positions
            //    how many universes it has each of 22 scores 
            Int64[,,,] uvs_with_position_score = new Int64[Constants.NUM_POSITIONS + 1, Constants.WINNING_SCORE_P2 + 1, Constants.NUM_POSITIONS + 1, Constants.WINNING_SCORE_P2 + 1];
            Int64[,,,] next_uvs_this_half_turn = new Int64[Constants.NUM_POSITIONS + 1, Constants.WINNING_SCORE_P2 + 1, Constants.NUM_POSITIONS + 1, Constants.WINNING_SCORE_P2 + 1];
            uvs_with_position_score[player_starts[0], 0, player_starts[1], 0] = 1;
            Int64 p1_wins = 0;
            Int64 p2_wins = 0;
            SetupQuantumDie();
            
            bool unwon_games = true;
            while (unwon_games)
            {
                unwon_games = false;
                int new_position = 0;
                int new_score = 0;

                // for each previous position we could have been in, find each new position
                for (int p1_position = 1; p1_position <= Constants.NUM_POSITIONS; p1_position++)
                {
                    // for each possible score in this position (but don't advance games that are already won)
                    for (int p1_score = 0; p1_score <= Constants.WINNING_SCORE_P2 - 1; p1_score++)
                    {
                        // for every opponent possibility
                        for (int p2_position = 1; p2_position <= Constants.NUM_POSITIONS; p2_position++)
                        {
                             for (int p2_score = 0; p2_score <= Constants.WINNING_SCORE_P2 - 1; p2_score++)
                             {
                                // if no one to advance here, move on
                                if (uvs_with_position_score[p1_position, p1_score, p2_position, p2_score] == 0)
                                {
                                    continue;
                                }
                                // for each possible die roll this turn
                                foreach (int curr_die_roll in die_roll_possibilities)
                                {
                                    new_position = p1_position + curr_die_roll;
                                    while (new_position > Constants.NUM_POSITIONS)
                                    {
                                        new_position -= Constants.NUM_POSITIONS;
                                    }
                                    new_score = p1_score + new_position;
                                    if (new_score >= Constants.WINNING_SCORE_P2)
                                    {
                                        p1_wins += uvs_with_position_score[p1_position, p1_score, p2_position, p2_score];
                                    }
                                    else
                                    {
                                        next_uvs_this_half_turn[new_position, new_score, p2_position, p2_score] += uvs_with_position_score[p1_position, p1_score, p2_position, p2_score];
                                    }
                                }
                             }
                        }
                    }
                }
                uvs_with_position_score = (Int64[,,,]) next_uvs_this_half_turn.Clone();
                Array.Clear(next_uvs_this_half_turn, 0, next_uvs_this_half_turn.Length);

                // for each previous position we could have been in, find each new position
                for (int p2_position = 1; p2_position <= Constants.NUM_POSITIONS; p2_position++)
                {
                    // for each possible score in this position (but don't advance games that are already won)
                    for (int p2_score = 0; p2_score <= Constants.WINNING_SCORE_P2 - 1; p2_score++)
                    {
                        // for every opponent possibility
                        for (int p1_position = 1; p1_position <= Constants.NUM_POSITIONS; p1_position++)
                        {
                             for (int p1_score = 0; p1_score <= Constants.WINNING_SCORE_P2 - 1; p1_score++)
                             {
                                // if no one to advance here, move on
                                if (uvs_with_position_score[p1_position, p1_score, p2_position, p2_score] == 0)
                                {
                                    continue;
                                }
                                // for each possible die roll this turn
                                foreach (int curr_die_roll in die_roll_possibilities)
                                {
                                    new_position = p2_position + curr_die_roll;
                                    while (new_position > Constants.NUM_POSITIONS)
                                    {
                                        new_position -= Constants.NUM_POSITIONS;
                                    }
                                    new_score = p2_score + new_position;
                                    if (new_score >= Constants.WINNING_SCORE_P2)
                                    {
                                        new_score = Constants.WINNING_SCORE_P2;
                                        p2_wins += uvs_with_position_score[p1_position, p1_score, p2_position, p2_score];
                                    }
                                    else
                                    {
                                        next_uvs_this_half_turn[p1_position, p1_score, new_position, new_score] += uvs_with_position_score[p1_position, p1_score, p2_position, p2_score];
                                        if (next_uvs_this_half_turn[p1_position, p1_score, new_position, new_score] > 0)
                                        {
                                            unwon_games = true;
                                        }
                                    }
                                }
                             }
                        }
                    }
                }
                uvs_with_position_score = (Int64[,,,]) next_uvs_this_half_turn.Clone();
                Array.Clear(next_uvs_this_half_turn, 0, next_uvs_this_half_turn.Length);
            }
            Console.WriteLine(Math.Max(p1_wins, p2_wins));
        }


        static int RollDeterministicDie ()
        {
            DieRollCount++;
            DeterministicDieNumber++;
            if (DeterministicDieNumber > 100)
            {
                DeterministicDieNumber -= 100;
            }
            return DeterministicDieNumber;
        }

        static void SetupQuantumDie ()
        {
            die_roll_possibilities  = new int[3 * 3 *3];
            int curr_die_poss = 0;
            for (int i = 1; i <= 3; i++)
            {
                for (int j = 1; j <= 3; j++)
                {
                    for (int k = 1; k <= 3; k++)
                    {
                        die_roll_possibilities[curr_die_poss++] = i + j + k;
                    }
                }
            }
        }
    }       

    public class TreeNode<T>
    {
        public T Data { get; set; }
        public TreeNode<T> Parent { get; set; }
        public ICollection<TreeNode<T>> Children { get; set; }

        public TreeNode()
        {
            this.Children = new LinkedList<TreeNode<T>>();
        }
        public TreeNode(T data)
        {
            this.Data = data;
            this.Children = new LinkedList<TreeNode<T>>();
        }

        public TreeNode<T> AddChild(T child)
        {
            TreeNode<T> childNode = new TreeNode<T>(child) { Parent = this };
            this.Children.Add(childNode);
            return childNode;
        }
    }
}
