﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

static class Constants
{   
    public const int LOWEST_MONAD_DIGIT = 1;
    public const int NUM_DISTINCT_MONAD_DIGITS = 9;
    public const int HIGHEST_MONAD_DIGIT = LOWEST_MONAD_DIGIT + NUM_DISTINCT_MONAD_DIGITS - 1;
    public const int CHAR_TO_INT_DELTA = 119;
    public const int MAX_PATH_INDEX = 4;
    public const int MIN_PATH_INDEX = 5;
    public const int Z_VAR_INDEX = 3;
    public const int STATE_ARRAY_SIZE = MIN_PATH_INDEX + 1;
}

namespace Year1Day24
{
    class Program
    {
        static Dictionary<Tuple<Int64,Int64,Int64,Int64>,Tuple<Int64,Int64>> dedup_dict = new Dictionary<Tuple<Int64, Int64, Int64, Int64>, Tuple<Int64,Int64>>();

        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = "";

            
            List<string> commands_array = new List<string>();
            while ((curr_line = reader.ReadLine()) != null) 
            {
                commands_array.Add(curr_line);                             
            }
            reader.Close();

            StateRunALU(commands_array);
        }
        static void StateRunALU(List<string> alu_cmds)
        {
            Int64[][] curr_poss_states = new Int64[1][];
            curr_poss_states[0] = new Int64[]{0,0,0,0,0,0};
            int line_num = 0;

            foreach (string curr_cmd in alu_cmds)
            {
                line_num++;
                Console.WriteLine();
                Console.Write(line_num);

                string[] cmd_array = curr_cmd.Split(" ");
                int var_index = cmd_array[1].ToCharArray()[0] - Constants.CHAR_TO_INT_DELTA;
                
                if (cmd_array[0] == "inp")
                {                      
                    Int64[][] next_poss_states = FastArrayMultiply(curr_poss_states);
                    int next_state_index = 0;
                    for (int curr_val = Constants.LOWEST_MONAD_DIGIT; curr_val <= Constants.HIGHEST_MONAD_DIGIT; curr_val++)
                    {
                        for (var x = 0; x < curr_poss_states.Length; x++)
                        {
                            next_poss_states[next_state_index][var_index] = curr_val;
                            next_poss_states[next_state_index][Constants.MAX_PATH_INDEX] = next_poss_states[next_state_index][Constants.MAX_PATH_INDEX] * 10 + curr_val;
                            next_poss_states[next_state_index][Constants.MIN_PATH_INDEX] = next_poss_states[next_state_index][Constants.MIN_PATH_INDEX] * 10 + curr_val;
                            next_state_index++;
                        }
                    }
                    Console.Write(" Added an input: " + curr_poss_states.Count() + "->" + next_poss_states.Count());
                    curr_poss_states = next_poss_states;
                }
                else
                {
                    dedup_dict.Clear();
                    Int64 operand = 0;
                    bool literal = Int64.TryParse(cmd_array[2], out operand);
                    int operand_index = cmd_array[2].ToCharArray()[0] - Constants.CHAR_TO_INT_DELTA;
                                                                 
                    for (int state_index = 0; state_index < curr_poss_states.Length; state_index++)
                    {
                        if (!literal)
                        {
                            operand = curr_poss_states[state_index][operand_index];
                        }                  
                        
                        switch(cmd_array[0])
                        {
                            case "add":
                            {
                                curr_poss_states[state_index][var_index] += operand;
                                break;
                            }
                            case "mul":
                            {
                                curr_poss_states[state_index][var_index] *= operand;
                                break;
                            }
                            case "div":
                            {
                                curr_poss_states[state_index][var_index] /= operand;
                                break;
                            }
                            case "mod":
                            {
                                curr_poss_states[state_index][var_index] %= operand;
                                break;
                            }
                            case "eql":
                            {
                                curr_poss_states[state_index][var_index] = (curr_poss_states[state_index][var_index] == operand ? 1 : 0);
                                break;
                            }
                        }
                        Tuple<Int64, Int64, Int64, Int64> state_tuple = new Tuple<Int64, Int64, Int64, Int64>(curr_poss_states[state_index][0], curr_poss_states[state_index][1], curr_poss_states[state_index][2], curr_poss_states[state_index][3]);
                        Tuple<Int64, Int64> curr_path_tuple;
                        if (dedup_dict.TryGetValue(state_tuple, out curr_path_tuple))
                        {
                            dedup_dict[state_tuple] = new Tuple<Int64, Int64>(Math.Max(curr_poss_states[state_index][Constants.MAX_PATH_INDEX], curr_path_tuple.Item1),
                                                                            Math.Min(curr_poss_states[state_index][Constants.MIN_PATH_INDEX], curr_path_tuple.Item2));
                        }
                        else
                        {
                            dedup_dict.Add(state_tuple, new Tuple<Int64, Int64>(curr_poss_states[state_index][Constants.MAX_PATH_INDEX],curr_poss_states[state_index][Constants.MIN_PATH_INDEX]));
                        }
                        
                    }
                    if (dedup_dict.Count < curr_poss_states.Length)
                    {
                        Console.Write(" Need to dedup: ");
                        curr_poss_states = DeDupStates(curr_poss_states);
                    }
                    else
                    {
                        Console.Write(" " + curr_poss_states.Count());
                    }
                }                    
            }

            Int64 max_path = Int64.MinValue;
            Int64 min_path = Int64.MaxValue;
            foreach (Int64[] curr_state in curr_poss_states)
            {
                if (curr_state[Constants.Z_VAR_INDEX] == 0)
                {
                    if (curr_state[Constants.MAX_PATH_INDEX] > max_path)
                    {
                        max_path = curr_state[Constants.MAX_PATH_INDEX];
                    }
                    if (curr_state[Constants.MIN_PATH_INDEX] < min_path)
                    {
                        min_path = curr_state[Constants.MIN_PATH_INDEX];
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine("Max: " + max_path);
            Console.WriteLine("Min: " + min_path);
        }
        static Int64[][] FastArrayMultiply(Int64[][] curr_states_array)
        {
            Int64 num_states = curr_states_array.Length;
            Int64[][] new_states_array = new Int64[num_states * Constants.NUM_DISTINCT_MONAD_DIGITS][];

            for (int copy_instance = Constants.LOWEST_MONAD_DIGIT; copy_instance <= Constants.HIGHEST_MONAD_DIGIT; copy_instance++)
            {
                for (Int64 curr_state_ind = 0; curr_state_ind < num_states; curr_state_ind++)
                {
                    Int64[] curr_state = curr_states_array[curr_state_ind];
                    Int64[] new_state = new Int64[Constants.STATE_ARRAY_SIZE];
                    Array.Copy(curr_state, new_state, Constants.STATE_ARRAY_SIZE);
                    new_states_array[curr_state_ind + (copy_instance - 1) * num_states] = new_state;
                }
            }

            return new_states_array;
        }

        static Int64[][] DeDupStates (Int64[][] curr_states)
        {
            Int64[][] filtered_states = new Int64[dedup_dict.Count()][];
            Int64 curr_state_index = 0;
            foreach (KeyValuePair<Tuple<Int64,Int64,Int64,Int64>,Tuple<Int64,Int64>> curr_dict_entry in dedup_dict)
            {
                filtered_states[curr_state_index++] = new Int64[]{curr_dict_entry.Key.Item1, curr_dict_entry.Key.Item2, curr_dict_entry.Key.Item3, curr_dict_entry.Key.Item4, curr_dict_entry.Value.Item1, curr_dict_entry.Value.Item2 };
            }
            Console.Write(curr_states.Count() + "->" + filtered_states.Count() + " (reduced to " + (Int64)curr_state_index*100/curr_states.Length + "%)");
            return filtered_states;
        }
    }
}
