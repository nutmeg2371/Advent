﻿using System;
using System.IO;

namespace Advent
{
    class Year1Day1
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            int WINDOW_SIZE = 3;
            int[] sliding_window = new int[WINDOW_SIZE];
            for (int window_index = 0; window_index < WINDOW_SIZE; window_index++)
            {
                sliding_window[window_index] = int.MaxValue;
            }

            int num_increases_part1 = 0;
            int num_increases_part2 = 0;
            while ((curr_line = reader.ReadLine()) != null) 
            {
                if (int.Parse(curr_line) > sliding_window[WINDOW_SIZE - 1])
                {
                    num_increases_part1++;
                }                
                if (int.Parse(curr_line) > sliding_window[0])
                {
                    num_increases_part2++;
                }
                for (int window_index = 0; window_index < WINDOW_SIZE-1; window_index++)
                {
                    sliding_window[window_index] = sliding_window[window_index + 1];
                }
                sliding_window[WINDOW_SIZE - 1] = int.Parse(curr_line);
            }
            Console.WriteLine(num_increases_part1);
            Console.WriteLine(num_increases_part2);
            reader.Close();
        }
    }
}
