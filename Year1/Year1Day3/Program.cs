﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

static class Constants
{
    public const int NUM_DIGITS = 12;
}

namespace Advent
{
    class Year1Day3
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            List<char[]> input_nums = new List<char[]>();

            while ((curr_line = reader.ReadLine()) != null) 
            {
                input_nums.Add(curr_line.ToCharArray());
            }
            reader.Close();
            
            List<char[]> ox_numbers = new List<char[]>(input_nums);
            List<char[]> co_numbers = new List<char[]>(input_nums);

            string most_seen_str = "";
            string least_seen_str = "";
            for (int curr_digit = 0; curr_digit < Constants.NUM_DIGITS; curr_digit++)
            {
                if (FindMostCommon(input_nums, curr_digit) > 0)
                {
                    most_seen_str += "1";
                    least_seen_str += "0";
                }
                else
                {
                    most_seen_str += "0";
                    least_seen_str += "1";
                }
            }
            int gamma_rate = Convert.ToInt32(most_seen_str, 2);
            int epsilon_rate = Convert.ToInt32(least_seen_str, 2);
            Console.WriteLine(gamma_rate * epsilon_rate);

            int filtering_digit = 0;
            while (ox_numbers.Count > 1 && filtering_digit < Constants.NUM_DIGITS)
            {
                ox_numbers = FilterMostOrLeastCommon(ox_numbers, filtering_digit++, true);
            }
            filtering_digit = 0;
            while (co_numbers.Count > 1 && filtering_digit < Constants.NUM_DIGITS)
            {
                co_numbers = FilterMostOrLeastCommon(co_numbers, filtering_digit++, false);
            }
            int ox_no = Convert.ToInt32(new string(ox_numbers[0]), 2);
            int co_no = Convert.ToInt32(new string(co_numbers[0]), 2);
            Console.WriteLine(ox_no * co_no);
        }
        static int FindMostCommon(List<char[]> input_nums, int bit_pos)
        {
            int num_ones_seen = 0;
            foreach (char[] curr_input_num in input_nums)
            {
                if (curr_input_num[bit_pos] == '1')
                {
                    num_ones_seen++;
                }
                else
                {
                    num_ones_seen--;
                }
            }
            return num_ones_seen;
        }
        static List<char[]> FilterMostOrLeastCommon(List<char[]> input_nums, int bit_pos, bool most_common)
        {
            int num_ones_seen = FindMostCommon(input_nums, bit_pos);
            char filter_select_value = '0';
            if ((most_common && num_ones_seen > 0) || (!most_common && num_ones_seen < 0) || (most_common && num_ones_seen == 0))
            {
                filter_select_value = '1';
            }
            else if ((most_common && num_ones_seen < 0) || (!most_common && num_ones_seen > 0) || (!most_common && num_ones_seen == 0))
            {
                filter_select_value = '0';
            }
            return (input_nums.Where(curr_input_num => curr_input_num[bit_pos] == filter_select_value)).ToList<char[]>();
        }
    }
}