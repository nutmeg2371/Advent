﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Advent
{
    class Year1Day10
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            Stack<char> char_stack = new Stack<char>();
            int corrupt_line_points = 0;
            List<Int64> incomplete_line_scores = new List<Int64>();
            
            while ((curr_line = reader.ReadLine()) != null) 
            {
                char_stack.Clear();
                bool corrupt_line = false;
                foreach (char curr_char in curr_line)
                {
                    if (curr_char == '(' || curr_char == '{' || curr_char == '[' || curr_char == '<')
                    {
                        char_stack.Push(curr_char);
                        continue;
                    }
                    char match_char = char_stack.Pop();
                    if (curr_char == ')' && match_char != '(')
                    {
                        corrupt_line_points += 3;
                        corrupt_line = true;
                        break;
                    }   
                    else if (curr_char == ']' && match_char != '[')
                    {
                        corrupt_line_points += 57;
                        corrupt_line = true;
                        break;
                    }
                    else if (curr_char == '}' && match_char != '{')
                    {
                        corrupt_line_points += 1197;
                        corrupt_line = true;
                        break;
                    }   
                    else if (curr_char == '>' && match_char != '<')
                    {
                        corrupt_line_points += 25137;
                        corrupt_line = true;
                        break;
                    }
                }
                if (corrupt_line || char_stack.Count == 0) 
                {
                    continue;
                }  
                Int64 incomplete_line_points = 0;
                while (char_stack.Count > 0)
                {
                    char match_char = char_stack.Pop();
                    incomplete_line_points *= 5;
                    if (match_char == '(')
                    {
                        incomplete_line_points += 1;
                    }       
                    else if (match_char == '[')
                    {
                        incomplete_line_points += 2;
                    }    
                    else if (match_char == '{')
                    {
                        incomplete_line_points += 3;
                    }   
                    else if (match_char == '<')
                    {
                        incomplete_line_points += 4;
                    }      
                } 
                incomplete_line_scores.Add(incomplete_line_points);
            }
            reader.Close();
            incomplete_line_scores.Sort();
            Console.WriteLine (corrupt_line_points);
            Console.WriteLine (incomplete_line_scores[incomplete_line_scores.Count/2]);
        }
    }
}