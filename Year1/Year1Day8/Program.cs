﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Advent
{
    class Year1Day8
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_full_line = "";

            int total_sum = 0;
            int count_part1 = 0;
            while ((curr_full_line = reader.ReadLine()) != null) 
            {
                string[] curr_line = curr_full_line.Split(" | ");
                string[] curr_in_patterns = curr_line[0].Split(" ");
                string[] curr_out_patterns = curr_line[1].Split(" ");
                Array.Sort(curr_in_patterns, (x1, x2) => x1.Length.CompareTo(x2.Length));
                
                char[] segment_map = new char[7];

                // First element in array is 1, which gives two right bar entries (map at 2, 5).  They could be reversed though.
                segment_map[2] = curr_in_patterns[0][0];
                segment_map[5] = curr_in_patterns[0][1];
                
                // Second element in array is 7, which gives top bar (map at 0).
                for (int digit_index = 0; digit_index < 3; digit_index++)
                {
                    if (curr_in_patterns[1][digit_index] != segment_map[2] && curr_in_patterns[1][digit_index] != segment_map[5])
                    {
                        segment_map[0] = curr_in_patterns[1][digit_index];
                        break;
                    }
                }

                // 3 is in 4th, 5th, or 6th position, includes the whole right bar and top bar (map at 2, 5, 0), and includes the middle and bottom bars (map at 3, 6).  They could be reversed though.
                // This also lets us fill in the two left bar entries (map at 1, 4).  They could be reversed though. 
                for (int curr_len_5_pattern_index = 3; curr_len_5_pattern_index <= 5; curr_len_5_pattern_index++)
                {
                    if (curr_in_patterns[curr_len_5_pattern_index].Contains(segment_map[2]) && curr_in_patterns[curr_len_5_pattern_index].Contains(segment_map[5]))
                    {
                        string remaining_chars = curr_in_patterns[curr_len_5_pattern_index];
                        remaining_chars = remaining_chars.Replace(segment_map[2].ToString(),"");
                        remaining_chars = remaining_chars.Replace(segment_map[5].ToString(),"");
                        remaining_chars = remaining_chars.Replace(segment_map[0].ToString(),"");
                        segment_map[3] = remaining_chars[0];
                        segment_map[6] = remaining_chars[1];

                        remaining_chars = "";
                        for (char candidate_char = 'a'; candidate_char <= 'g'; candidate_char++)
                        {
                            if (!curr_in_patterns[curr_len_5_pattern_index].Contains(candidate_char))
                            {
                                remaining_chars += candidate_char;
                            }
                        }
                        segment_map[1] = remaining_chars[0];
                        segment_map[4] = remaining_chars[1];
                        break;
                    }
                }

                // If we walk through the length 6 items, we find 0, 6, and 9.  
                for (int curr_len_6_pattern_index = 6; curr_len_6_pattern_index <= 8; curr_len_6_pattern_index++)
                {
                    // 0 let's us disambiguate the possibly mixed middle and bottom bars (map at 3, 6).
                    if (!(curr_in_patterns[curr_len_6_pattern_index].Contains(segment_map[3]) && curr_in_patterns[curr_len_6_pattern_index].Contains(segment_map[6])))
                    {
                        if (curr_in_patterns[curr_len_6_pattern_index].Contains(segment_map[3]))
                        {
                            char temp = segment_map[3];
                            segment_map[3] = segment_map[6];
                            segment_map[6] = temp;
                        }
                    }
                    
                    // 6 let's us disambiguate the possibly mixed right bars (map at 2, 5).
                    if ((curr_in_patterns[curr_len_6_pattern_index].Contains(segment_map[2]) && !curr_in_patterns[curr_len_6_pattern_index].Contains(segment_map[5])) ||
                        (!curr_in_patterns[curr_len_6_pattern_index].Contains(segment_map[2]) && curr_in_patterns[curr_len_6_pattern_index].Contains(segment_map[5])))
                    {
                        if (curr_in_patterns[curr_len_6_pattern_index].Contains(segment_map[2]))
                        {
                            char temp = segment_map[2];
                            segment_map[2] = segment_map[5];
                            segment_map[5] = temp;
                        }
                    }                    
                    
                    // 9 let's us disambiguate the possibly mixed left bars (map at 1, 4).
                    if ((curr_in_patterns[curr_len_6_pattern_index].Contains(segment_map[1]) && !curr_in_patterns[curr_len_6_pattern_index].Contains(segment_map[4])) ||
                        (!curr_in_patterns[curr_len_6_pattern_index].Contains(segment_map[1]) && curr_in_patterns[curr_len_6_pattern_index].Contains(segment_map[4])))
                    {
                        if (curr_in_patterns[curr_len_6_pattern_index].Contains(segment_map[4]))
                        {
                            char temp = segment_map[4];
                            segment_map[4] = segment_map[1];
                            segment_map[1] = temp;
                        }
                    }
                }

                // segment map built, look at output
                string this_output = "";
                foreach (string curr_out_digit in curr_out_patterns)
                {
                    if (curr_out_digit.Length == 2)
                    {
                        this_output += "1";
                        count_part1++;
                    }
                    else if (curr_out_digit.Length == 3)
                    {
                        this_output += "7";
                        count_part1++;
                    }
                    else if (curr_out_digit.Length == 4)
                    {
                        this_output += "4";
                        count_part1++;
                    }
                    else if (curr_out_digit.Length == 7)
                    {
                        this_output += "8";
                        count_part1++;
                    }
                    else if (curr_out_digit.Length == 5 && curr_out_digit.Contains(segment_map[2]) && curr_out_digit.Contains(segment_map[5]))
                    {
                        this_output += "3";
                    }
                    else if (curr_out_digit.Length == 5 && curr_out_digit.Contains(segment_map[2]) && !curr_out_digit.Contains(segment_map[5]))
                    {
                        this_output += "2";
                    }
                    else if (curr_out_digit.Length == 5 && !curr_out_digit.Contains(segment_map[2]) && curr_out_digit.Contains(segment_map[5]))
                    {
                        this_output += "5";
                    }
                    else if (curr_out_digit.Length == 6 && !curr_out_digit.Contains(segment_map[3]))
                    {
                        this_output += "0";
                    }
                    else if (curr_out_digit.Length == 6 && !curr_out_digit.Contains(segment_map[2]))
                    {
                        this_output += "6";
                    }
                    else if (curr_out_digit.Length == 6 && !curr_out_digit.Contains(segment_map[4]))
                    {
                        this_output += "9";
                    }
                    else 
                    {
                        Console.Write("poop");
                    }
                }
                total_sum += int.Parse(this_output);
            }
            reader.Close();
            Console.WriteLine(count_part1);
            Console.WriteLine(total_sum);
        }
    }
}