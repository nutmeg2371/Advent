﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Advent
{
    class Year1Day7
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string[] curr_pos_strs = reader.ReadLine().Split(",");
            Int64[] curr_pos_ints = Array.ConvertAll(curr_pos_strs, curr_pos_str => Int64.Parse(curr_pos_str));
            Int64 max_pos = curr_pos_ints.Max();


            Int64 lowest_cost_part1 = Int64.MaxValue;
            Int64 lowest_cost_part2 = Int64.MaxValue;
            for (Int64 potential_pos = 0; potential_pos <= max_pos; potential_pos++)
            {
                Int64 this_pos_cost_part2 = 0;
                Int64 this_pos_cost_part1 = 0;
                foreach (Int64 this_pos in curr_pos_ints)
                {
                    Int64 n = Math.Abs(potential_pos - this_pos);
                    this_pos_cost_part1 += n;
                    this_pos_cost_part2 += n * (n+1) / 2;
                }
                if (this_pos_cost_part2 < lowest_cost_part2)
                {
                    lowest_cost_part2 = this_pos_cost_part2;
                }
                if (this_pos_cost_part1 < lowest_cost_part1)
                {
                    lowest_cost_part1 = this_pos_cost_part1;
                }

            }
            reader.Close();
            Console.WriteLine (lowest_cost_part1);
            Console.WriteLine (lowest_cost_part2);
        }
    }
}