﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Year1Day12
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";
            
            Dictionary<string, HashSet<string>> links = new Dictionary<string, HashSet<string>>();
            while ((curr_line = reader.ReadLine()) != null) 
            {
                string[] nodes_this_line = curr_line.Split("-");
                if (!links.ContainsKey(nodes_this_line[0]))
                {
                    links[nodes_this_line[0]] = new HashSet<string>();                    
                }
                if (!links.ContainsKey(nodes_this_line[1]))
                {
                    links[nodes_this_line[1]] = new HashSet<string>();                    
                }
                links[nodes_this_line[0]].Add(nodes_this_line[1]);
                links[nodes_this_line[1]].Add(nodes_this_line[0]);
            }
            reader.Close();

            HashSet<Tuple<List<string>,bool>> curr_paths = new HashSet<Tuple<List<string>, bool>>();

            HashSet<string> first_links = links["start"];
            foreach (string link_target in first_links)
            {
                List<string> initial_path = new List<string>();
                initial_path.Add("start");
                initial_path.Add(link_target);
                curr_paths.Add(new Tuple<List<string>,bool>(initial_path, false));
            }

            HashSet<Tuple<List<string>,bool>> working_paths = new HashSet<Tuple<List<string>, bool>>();
            HashSet<Tuple<List<string>,bool>> ending_paths = new HashSet<Tuple<List<string>, bool>>();
            int part1_ending_paths = 0;
            
            while (curr_paths.Count > 0)
            {
                foreach (Tuple<List<string>,bool> curr_path in curr_paths)
                {
                    string path_end = curr_path.Item1[curr_path.Item1.Count-1];
                    HashSet<string> added_links = links[path_end];
                    foreach (string link_target in added_links)
                    {
                        List<string> new_path = new List<string>(curr_path.Item1);
                        if (link_target == "end")
                        {
                            new_path.Add(link_target);
                            ending_paths.Add(new Tuple<List<string>,bool>(new_path,curr_path.Item2));
                            if (!curr_path.Item2)
                            {
                                part1_ending_paths++;
                            }
                        }
                        else if (Char.IsUpper(link_target[0]))
                        {
                            new_path.Add(link_target);
                            working_paths.Add(new Tuple<List<string>,bool>(new_path,curr_path.Item2));
                        }
                        else if (!new_path.Contains(link_target))
                        {
                            new_path.Add(link_target);
                            working_paths.Add(new Tuple<List<string>,bool>(new_path,curr_path.Item2));
                        }
                        else if (!curr_path.Item2 && link_target != "start")                        
                        {
                            new_path.Add(link_target);
                            working_paths.Add(new Tuple<List<string>,bool>(new_path,true));
                        }
                    }
                }
                curr_paths = new HashSet<Tuple<List<string>,bool>>(working_paths);
                working_paths.Clear();
            }
            Console.WriteLine(part1_ending_paths);
            Console.WriteLine(ending_paths.Count);
        }
    }
}
