﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Year1Day20
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");          
            string curr_line = "";

            string enhancement_str = "";
            while ((curr_line = reader.ReadLine()) != null && curr_line != "") 
            {
                enhancement_str += curr_line;
            }

            List<string> input_img_strs = new List<string>();
            while ((curr_line = reader.ReadLine()) != null)
            {
                input_img_strs.Add(curr_line);
            }
            reader.Close();

            int input_img_size = input_img_strs.Count;
            int[,] input_grid = new int[input_img_size, input_img_size];

            for (int row_i = 0; row_i < input_img_size; row_i++)
            {
                for (int col_i = 0; col_i < input_img_size; col_i++)
                {
                    if (input_img_strs[row_i][col_i] == '#')
                    {
                        input_grid[row_i, col_i] = 1;
                    }
                }
            }

            int num_enhances = 50;
            int[,] next_grid = new int[input_img_size + 4 * num_enhances, input_img_size + 4 * num_enhances];
            int[,] curr_grid = new int[input_img_size + 4 * num_enhances, input_img_size + 4 * num_enhances];

            // build final size grid
            for (int row_i = 0; row_i < input_img_size; row_i++)
            {
                for (int col_i = 0; col_i < input_img_size; col_i++)
                {
                    curr_grid[row_i + 2 * num_enhances, col_i + 2 * num_enhances] = input_grid[row_i, col_i];
                }
            }

            int curr_lit_pixels = 0;
            for (int enhance_num = 1; enhance_num <= num_enhances; enhance_num++)
            {
                curr_lit_pixels = 0;
                // go through and figure out new pixels
                for (int row_i = 1; row_i <= input_img_size + 4 * num_enhances -2; row_i++)
                {
                    for (int col_i = 1; col_i <= input_img_size + 4 * num_enhances - 2; col_i++)
                    {
                        string curr_enhance_index_str = "";
                        int curr_enhance_index = 0;
                        curr_enhance_index_str += curr_grid[row_i - 1, col_i - 1].ToString();
                        curr_enhance_index_str += curr_grid[row_i - 1, col_i].ToString();
                        curr_enhance_index_str += curr_grid[row_i - 1, col_i + 1].ToString();
                        curr_enhance_index_str += curr_grid[row_i, col_i - 1].ToString();
                        curr_enhance_index_str += curr_grid[row_i, col_i].ToString();
                        curr_enhance_index_str += curr_grid[row_i, col_i + 1].ToString();
                        curr_enhance_index_str += curr_grid[row_i + 1, col_i - 1].ToString();
                        curr_enhance_index_str += curr_grid[row_i + 1, col_i].ToString();
                        curr_enhance_index_str += curr_grid[row_i + 1, col_i + 1].ToString();
                        curr_enhance_index = Convert.ToInt32(curr_enhance_index_str, 2);
                        if (enhancement_str[curr_enhance_index] == '#')
                        {
                            next_grid[row_i, col_i] = 1;
                            curr_lit_pixels++;
                        }
                    }
                }

                // if the enhancement string starts with a #, for odd (even) enhancement numbers light up (turn off) the boundary
                if (enhancement_str[0] == '#')
                {
                    int boundary_value = (enhance_num % 2 == 0) ? 0 : 1;
                    for (int row_i = 0; row_i < curr_grid.GetLength(0); row_i++)
                    {
                        next_grid[row_i, 0] = boundary_value;
                        next_grid[row_i, curr_grid.GetLength(1) - 1] = boundary_value;
                    }
                    for (int col_i = 0; col_i < curr_grid.GetLength(1); col_i++)
                    {
                        next_grid[0, col_i] = boundary_value;
                        next_grid[curr_grid.GetLength(1) - 1, col_i] = boundary_value;
                    }
                    int delta_boundary = boundary_value * (4 * curr_grid.GetLength(0) - 4);
                    if (boundary_value == 0)
                    {
                        curr_lit_pixels -= delta_boundary;
                    }
                    else
                    {
                        curr_lit_pixels -= delta_boundary;
                    }         
                }
                
                // setup for next round
                curr_grid = next_grid;
                next_grid = new int[input_img_size + 4 * num_enhances, input_img_size + 4 * num_enhances];
            }        
            Console.WriteLine(curr_lit_pixels);
        }
    }
}
