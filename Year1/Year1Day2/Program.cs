﻿using System;
using System.IO;

namespace Advent
{
    class Year1Day2
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = "";

            int total_horizontal = 0;
            int total_depth_part1 = 0;
            int total_depth_part2 = 0;
            int curr_aim = 0;
            while ((curr_line = reader.ReadLine()) != null) 
            {
                string[] cmds = curr_line.Split(" ");
                int curr_cmd_val = int.Parse(cmds[1].ToString());
                switch(cmds[0].ToString())
                {
                    case "forward":
                    {
                        total_horizontal += curr_cmd_val;
                        total_depth_part2 += curr_aim * curr_cmd_val;
                        break;
                    }
                    case "down":
                    {
                        total_depth_part1 += curr_cmd_val;
                        curr_aim += curr_cmd_val;
                        break;
                    }
                    case "up":
                    {
                        total_depth_part1 -= curr_cmd_val;
                        curr_aim -= curr_cmd_val;
                        break;
                    }
                }
            }
            Console.WriteLine(total_horizontal * total_depth_part1);
            Console.WriteLine(total_horizontal * total_depth_part2);
        }
    }
}
