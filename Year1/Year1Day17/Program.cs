﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Year1Day17
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            string curr_line = reader.ReadLine();
            reader.Close();

            curr_line = curr_line.Replace("target area: x=","");
            curr_line = curr_line.Replace(", y=","..");
            int[] target_coords = Array.ConvertAll(curr_line.Split(".."), s => int.Parse(s.ToString()));

            // each possible x requires at least x many steps (and can be infinitely more)
            int min_initial_x = 0;
            int result_x = target_coords[0] - 1;
            while (result_x < target_coords[0])
            {
                min_initial_x++;
                result_x = min_initial_x * (min_initial_x + 1) / 2;
            }

            // maximum possible x is one that overshoots right away
            int max_initial_x = target_coords[1] + 1;

            int min_initial_y = target_coords[2];
            int max_initial_y = 1 - target_coords[2];

            // run it
            List<Tuple<int,int>> target_hits = new List<Tuple<int, int>>();
            for (int poss_x_vel = min_initial_x; poss_x_vel < max_initial_x; poss_x_vel++)
            {
                for (int poss_y_vel = min_initial_y; poss_y_vel < max_initial_y; poss_y_vel++)
                {
                    int curr_x_pos = 0;
                    int curr_y_pos = 0;
                    
                    int curr_x_vel = poss_x_vel;
                    int curr_y_vel = poss_y_vel;

                    bool overshot = false;
                    bool intarget = false;
                    while (!overshot)
                    {
                        curr_x_pos += curr_x_vel;
                        curr_y_pos += curr_y_vel;

                        overshot = curr_y_pos < target_coords[2];
                        intarget = curr_x_pos >= target_coords[0] && curr_x_pos <= target_coords[1] && curr_y_pos >= target_coords[2] && curr_y_pos <= target_coords[3];

                        if (intarget)
                        {
                            target_hits.Add(new Tuple<int, int>(poss_y_vel, poss_x_vel));
                            break;
                        }

                        curr_y_vel--;
                        if (curr_x_vel > 0)
                        {
                            curr_x_vel--;
                        }
                        else if (curr_x_vel < 0)
                        {
                            curr_x_vel++;
                        }
                    }
                    
                }
            }

            target_hits.Sort();
            target_hits.Reverse();
            int largest_y = target_hits[0].Item1;
            Console.WriteLine(largest_y * (largest_y + 1) /2);
            Console.WriteLine(target_hits.Count);
            

           
        }
    }
}
