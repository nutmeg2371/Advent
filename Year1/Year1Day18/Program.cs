﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Year1Day18
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader reader = File.OpenText("input.txt");
            
            List<List<string>> all_input_nums = new List<List<string>>();
            
            string curr_line = reader.ReadLine();
            List<string> addition_result = Listify(curr_line);
            all_input_nums.Add(addition_result);

            while ((curr_line = reader.ReadLine()) != null) 
            {
                List<string> curr_list = Listify(curr_line);
                all_input_nums.Add(curr_list);

                addition_result = AddTwo(addition_result, curr_list);
                Reduce(ref addition_result);
            }
            reader.Close();

            Console.WriteLine(Magnitude(addition_result));

            int max_magnitude = int.MinValue;
            for (int i = 0; i < all_input_nums.Count; i++)
            {
                for (int j = 0; j < all_input_nums.Count; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }
                    List<string> op_list = AddTwo(all_input_nums[i], all_input_nums[j]);
                    Reduce(ref op_list);
                    int curr_magnitude = Magnitude(op_list);
                    if (curr_magnitude > max_magnitude)
                    {
                        max_magnitude = curr_magnitude;
                    }
                }
            }
            Console.WriteLine(max_magnitude);

        }

        static void Reduce(ref List<string> operand)
        {
            bool exploded = true;
            bool split = true;
            while (exploded || split)
            {
                split = false;
                exploded = Explode(ref operand);
                if (!exploded)
                {
                    split = Split(ref operand);
                }
            }
        }

        static List<string> AddTwo(List<string> operand1, List<string> operand2)
        {
            List<string> combined_list = new List<string>();
            if (operand1.Count == 0)
            {
                return operand2;
            }
            if (operand2.Count == 0)
            {
                return operand1;
            }
            combined_list.Add("[");
            combined_list.AddRange(operand1);
            combined_list.Add(",");
            combined_list.AddRange(operand2);
            combined_list.Add("]");           
            return combined_list;
        }

        static bool Explode (ref List<string> operand)
        {
            int nesting_level = 0;
            int last_seen_num_pos = -1; 

            List<string> exploded = new List<string> ();
            for (int curr_pos = 0; curr_pos < operand.Count; curr_pos++)
            {
                if (operand[curr_pos] == "[")
                {
                    nesting_level++;
                }
                else if (operand[curr_pos] == "]")
                {
                    nesting_level--;
                }
                else if (operand[curr_pos] == ",")
                {
                    continue;
                }
                else if (nesting_level > 4 && operand.Count > 4 && operand[curr_pos] != "[" && operand[curr_pos] != "]" && operand[curr_pos] != "," && operand[curr_pos + 1] == "," && operand[curr_pos + 2] != "[" && operand[curr_pos + 2] != "]" && operand[curr_pos + 2] != "," )
                {
                    if (last_seen_num_pos > -1)
                    {
                        operand[last_seen_num_pos] = (int.Parse(operand[last_seen_num_pos]) + int.Parse(operand[curr_pos])).ToString();
                    }
                    int next_seen_num_pos = curr_pos + 3;
                    while (next_seen_num_pos < operand.Count && (operand[next_seen_num_pos] == "[" || operand[next_seen_num_pos] == "]" || operand[next_seen_num_pos] == ","))
                    {
                        next_seen_num_pos++;
                    }
                    if (next_seen_num_pos < operand.Count)
                    {
                        operand[next_seen_num_pos] = (int.Parse(operand[next_seen_num_pos]) + int.Parse(operand[curr_pos + 2])).ToString();
                    }
                    exploded.AddRange(operand.GetRange(0, curr_pos - 1));
                    exploded.Add("0");
                    exploded.AddRange(operand.GetRange(curr_pos + 4, operand.Count - curr_pos - 4));
                    operand = exploded;
                    return true;
                }                
                else
                {
                    last_seen_num_pos = curr_pos;
                }
            }
            return false;
        }

        static bool Split (ref List<string> operand)
        {
            List<string> splitted = new List<string> ();
            
            for (int curr_pos = 0; curr_pos < operand.Count; curr_pos++)
            {
                if (operand[curr_pos] == "[" || operand[curr_pos] == "]" || operand[curr_pos] == "," || int.Parse(operand[curr_pos]) < 10)
                {
                    splitted.Add(operand[curr_pos]);
                    continue;
                }
                int half = int.Parse(operand[curr_pos])/2;
                int other_half = half;
                if (2 * half != int.Parse(operand[curr_pos]))
                {
                    other_half++;
                }
                splitted.Add("[");
                splitted.Add(half.ToString());
                splitted.Add(",");
                splitted.Add(other_half.ToString());
                splitted.Add("]");
                splitted.AddRange(operand.GetRange(curr_pos + 1, operand.Count - curr_pos - 1));
                operand = splitted;
                return true;
            }
            return false;
        }

        

        static int Magnitude (List<string> operand)
        {
            List<string> intermediate = new List<string>(operand);

            bool change = true;
            while (change)
            {
                change = false;
                List<string> next_intermediate = new List<string>();
                for (int curr_pos = 0; curr_pos < intermediate.Count; curr_pos++)
                {
                    next_intermediate.Add(intermediate[curr_pos]);
                    if (intermediate.Count > 4 && intermediate[curr_pos] != "[" && intermediate[curr_pos] != "]" && intermediate[curr_pos] != "," && intermediate[curr_pos + 1] == "," && intermediate[curr_pos + 2] != "[" && intermediate[curr_pos + 2] != "]" && intermediate[curr_pos + 2] != "," )
                    {
                        next_intermediate.RemoveAt(next_intermediate.Count - 1);
                        next_intermediate.RemoveAt(next_intermediate.Count - 1);
                        next_intermediate.Add((3 * int.Parse(intermediate[curr_pos]) + 2 * int.Parse(intermediate[curr_pos + 2])).ToString());
                        curr_pos += 3;
                        change = true;
                    }
                }
                intermediate = next_intermediate;
            }
            
            return int.Parse(intermediate[0]);
        }

        static List<string> Listify (string operand)
        {
            List<string> listified = new List<string>();
            for (int curr_pos = 0; curr_pos < operand.Length; curr_pos++)
            {
                if (operand[curr_pos] == '[' || operand[curr_pos] == ']' || operand[curr_pos] == ',')
                {
                    listified.Add(operand[curr_pos].ToString());
                    continue;
                }
                string curr_num = "";
                while (operand[curr_pos] >= '0' && operand[curr_pos] <= '9')
                {
                    curr_num += operand[curr_pos];
                    curr_pos++;
                }
                curr_pos--;
                listified.Add(curr_num);
            }
            return listified;
        }
    }
}
